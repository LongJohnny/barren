# Barren
A sandbox exploration game about mastering an intricate physics driven movement system.

## Setup

First make sure you have [git LFS](https://git-lfs.github.com/) installed. 

Then, clone the repository using the following command on you-re preferred directory:

```
git clone https://gitlab.com/LongJohnny/barren.git
```

Now open Unity Hub and press "Open" and select the root folder where the repository was clone (i.e. barren).

Thats it!

## Authors and acknowledgment
- João Silva 
- Francisco Sousa 1
- Rafael Vaz
- Ana Rocha





