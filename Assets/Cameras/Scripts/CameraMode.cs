﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Cameras.Scripts
{
    public abstract class CameraMode : MonoBehaviour
    {
        [System.Serializable]
        public struct CameraModeEvents
        {
            /*
             * Raised when the transition to this camera starts
             * 
             * bool - indicates whether this transition is starting from a cancelled one
             */
            public UnityEvent<bool> onTransitionToThisStart;

            /*
             * Raised when the transition to this camera ends
             */
            public UnityEvent onTransitionToThisEnd;

            /*
             * Raised when the transition from this camera starts
             * 
             * bool - indicates whether this transition is starting from a cancelled one
             */
            public UnityEvent<bool> onTransitionFromThisStart;

            /*
             * Raised when the transition from this camera ends
             */
            public UnityEvent onTransitionFromThisEnd;
        }


        [Header("Camera mode transition options")]

        /*
         * Specified the type of function used in the transition
         * between cameras
         */
        public CameraTransitionFunction transitionFunction = CameraTransitionFunction.SmoothStep;

        /*
         * Specifies the transition speed to this camera mode
         */
        [Range(0, 1000)] public float transitionSpeed = 0.5f;

        /*
         * Contains all events raised in the transitions between modes
         */
        public CameraModeEvents cameraModeEvents;

        /*
         * Called when the transition from this camera starts
         * 
         * cancelled - indicates whether this transition is starting from a cancelled one
         */
        public virtual void TransitionFromThisStart(bool cancelled) {}

        /*
         * Called when the transition to this camera ends
         */
        public virtual void TransitionToThisEnd() {}

        /*
         * Called when the transition to this camera starts
         * 
         * cancelled - indicates whether this transition is starting from a cancelled one
         */
        public virtual void TransitionToThisStart(Vector3 previousCameraForward, bool previousIsPlayerCam, bool cancelled) { }

        /*
         * Called when the transition from this camera is over
         */
        public virtual void TransitionFromThisEnd() {}

        /*
         * Called when the transition to this camera has been cancelled
         */
        public virtual void TransitionToThisCancelled() { }

        /*
         * Called when the transition from this camera has been cancelled
         */
        public virtual void TransitionFromThisCancelled() { }

        /*
         * Returns whether or not this camera mode is over and the system should transition to the next camera mode
         */
        public virtual bool IsModeOver() => true;

        /*
         * Returns the transform of the camera
         */
        public abstract Transform GetCameraTransform();

        /*
         * Returns the current FOV of the camera
         */
        public abstract float GetFov();

        /*
         * Returns the camera associated with this camera mode
         */
        public abstract Camera GetCamera();

        /*
         * Sends camera mode to the camera manager to be used
         */
        public virtual void EnqueueCameraMode()
        {
            CameraManager.Instance.EnqueueCameraMode(this);
        }

        /*
         * Removes camera mode from camera manager to be cancelled or removed
         */
        public virtual void DequeueCameraMode()
        {
            CameraManager.Instance.DequeueCameraMode(this);
        }

        /*
         * Indicates whether this camera looks at the player
         */
        public virtual bool IsPlayerCam() => false;
    }
}