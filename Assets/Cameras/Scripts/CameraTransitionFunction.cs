﻿using System;
using System.Collections;
using Assets.Common.Scripts;
using UnityEngine;

namespace Assets.Cameras.Scripts
{
    public enum CameraTransitionFunction
    {
        Linear,
        Sin,
        Pow2,
        Pow3,
        SmoothStep,
    }

    public static class CameraTransitionFunctionExtensions
    {
        public static Func<float,float> GetTransitionFunction(this CameraTransitionFunction cameraTransitionFunction)
        {
            switch(cameraTransitionFunction)
            {
                case CameraTransitionFunction.Linear:
                    return (x) => x;
                case CameraTransitionFunction.Sin:
                    return (x) => Mathf.Sin(x*Mathf.PI*.5f);
                case CameraTransitionFunction.Pow2:
                    return (x) => x*x;
                case CameraTransitionFunction.Pow3:
                    return (x) => x*x*x;
                case CameraTransitionFunction.SmoothStep:
                    return (x) => MathAux.SmoothStep(0f, 1f, x);
                default:
                    return (x) => MathAux.SmoothStep(0f, 1f, x);
            }
        }
    }
}