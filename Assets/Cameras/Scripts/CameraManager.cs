﻿using System;
using System.Collections.Generic;
using CameraShake;
using UnityEngine;

namespace Assets.Cameras.Scripts
{
    public class CameraManager : MonoBehaviour
    {
        public static CameraManager Instance;

        private Camera _transitionCamera;

        private CameraMode _baseCameraMode;
        private LinkedList<CameraMode> _cameraModes = new LinkedList<CameraMode>();
        private CameraMode _currMode;
        private CameraMode _nextMode;
        private Func<float,float> _currTransitionFunction;

        private float _k = 0.0f;

#if UNITY_EDITOR
        public int CameraModes = 0;
        public string CurrentMode;
        public string NextMode;
#endif

        private void Awake()
        {
            if (Instance != null)
                throw new ArgumentException("Cannot have more than one camera manager!");
            Instance = this;
        }

        private void Start()
        {
            _transitionCamera = GetComponentInChildren<Camera>();
            _transitionCamera.enabled = false;
            _baseCameraMode = CameraController.Instance;
            _currMode = _baseCameraMode;
        }

        private void LateUpdate()
        {
#if UNITY_EDITOR
            CameraModes = _cameraModes.Count;
            CurrentMode = _currMode.name;
            NextMode = _nextMode != null ? _nextMode.name : "None";
#endif

            if (_cameraModes.Count == 0 || !_currMode.IsModeOver()) return;

            // Do we start a transition?
            if (!_transitionCamera.enabled)
            {
                //Debug.Log("Started transition to new camera!");


                _currMode.TransitionFromThisStart(false);
                _currMode.cameraModeEvents.onTransitionFromThisStart.Invoke(false);
                Component.Destroy(_currMode.GetCamera().GetComponent<AudioListener>());
                
                _nextMode = GetNextCameraMode(_cameraModes.First);
                _nextMode.TransitionToThisStart(_currMode.GetCameraTransform().forward,_currMode.IsPlayerCam(),false);
                _nextMode.cameraModeEvents.onTransitionToThisStart.Invoke(false);

                _currTransitionFunction = _nextMode.transitionFunction.GetTransitionFunction();
                _transitionCamera.enabled = true;
                _transitionCamera.gameObject.AddComponent<AudioListener>();

                CameraShaker.Instance.SetCameraTransform(_transitionCamera.transform);

                Transform curr = _currMode.GetCameraTransform();
                float currFOV = _currMode.GetFov();
                Transform next = _nextMode.GetCameraTransform();
                float nextFOV = _nextMode.GetFov();

                // If both camera modes are in the exact same position, rotation and FOV then there is no transition
                _k = (curr.position == next.position && 
                     curr.position == next.position && 
                     Math.Abs(currFOV - nextFOV) < float.Epsilon) ? 1f : 0f;
            }            

            // Perform transition
            _k += Time.deltaTime * _nextMode.transitionSpeed;
            _k = Mathf.Min(1f, _k);
            TransitionCamera(_currMode.GetCameraTransform(), _nextMode.GetCameraTransform(), _currMode.GetFov(), _nextMode.GetFov(), _currTransitionFunction);

            //Debug.Log(_k);

            // Is transition happening?
            if (_k < 1f) return;

            // Is the transition not from the base mode?
            if (_currMode != _baseCameraMode) _cameraModes.Remove(_currMode);

            _transitionCamera.enabled = false;
            _currMode.TransitionFromThisEnd();
            _currMode.cameraModeEvents.onTransitionFromThisEnd.Invoke();
            _currMode = _nextMode;
            Component.Destroy(_transitionCamera.GetComponent<AudioListener>());

            _nextMode.TransitionToThisEnd();
            _nextMode.cameraModeEvents.onTransitionToThisEnd.Invoke();
            CameraShaker.Instance.SetCameraTransform(_nextMode.GetCamera().transform);
            _nextMode.GetCamera().gameObject.AddComponent<AudioListener>();
            _nextMode = null;
        }
    
        
        private void TransitionCamera(Transform start, Transform end, float startFov, float endFov, Func<float, float> func)
        {
            float fk = func(_k);
            transform.position = Vector3.Slerp(start.position, end.position, fk);
            transform.rotation = Quaternion.Slerp(start.rotation, end.rotation, fk);
            _transitionCamera.fieldOfView = Mathf.Lerp(startFov, endFov, fk);

            if(start.position == end.position &&
               start.position == end.position &&
               Math.Abs(startFov - endFov) < float.Epsilon)
            {
                _k = 1f;
            }
        }

        /*
         * Change to a new camera mode
         */
        public void EnqueueCameraMode(CameraMode cameraMode)
        {
            if (!_cameraModes.Contains(cameraMode))
                _cameraModes.AddLast(cameraMode);  
        }
        
        /*
         * Cancel the current transition between camera modes if there is one happening
         * or remove it from the queue
         */
        public void DequeueCameraMode(CameraMode cameraMode)
        {
            if (_nextMode == cameraMode)
            {
                _k = 1f - _k;

                (_nextMode, _currMode) = (_currMode, _nextMode);

                _currMode.TransitionToThisCancelled();
                _currMode.TransitionFromThisStart(true);
                _currMode.cameraModeEvents.onTransitionToThisStart.Invoke(true);

                _nextMode.TransitionFromThisCancelled();
                _nextMode.TransitionToThisStart(_currMode.GetCameraTransform().forward, _currMode.IsPlayerCam(),true);
                _nextMode.cameraModeEvents.onTransitionToThisStart.Invoke(true);

                _currTransitionFunction = _nextMode.transitionFunction.GetTransitionFunction();

                CameraShaker.Instance.SetCameraTransform(_transitionCamera.transform);
            } 
            else if (cameraMode != _currMode)
            {
                _cameraModes.Remove(cameraMode);
            }
        }

        private CameraMode GetNextCameraMode(LinkedListNode<CameraMode> node)
        {
            if (_cameraModes.Count == 1)
            {
                if (_currMode != _baseCameraMode)
                {
                    return _baseCameraMode;
                } 
                else
                {
                    return _cameraModes.First.Value;
                }
            } 

            return node.Next.Value;
        }
   
    }
}