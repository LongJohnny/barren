﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Assets.Common;
using Assets.Common.Scripts;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Cameras.Scripts
{
    public class CameraControllable3D : CameraMode
    {

        [Header("Camera parameters")]
        [SerializeField] [Range(10.0f, 100.0f)] private float _maxSensitivity = 50f;
        [SerializeField] private float _sensitivityAcceleration = 200f;
        private float _currentSensitivity = 0f;

        [SerializeField] private float _joystickSensitivity = 2f;
        [SerializeField] private float _mouseSensitivity = .5f;

        [SerializeField]
        [Range(0.5f, 0.0000000001f)]
        [Tooltip("Controls the angle at which the camera stops")]
        private float _stopRangeUp = 0.001f;

        [SerializeField]
        [Range(0.5f, 0.0000000001f)]
        [Tooltip("Controls the angle at which the camera stops")]
        private float _stopRangeDown = 0.01f;

        [Tooltip("Center of the spherical surface where the camera will be moving")]
        [SerializeField] private Transform _rotateAroundTarget;
        [SerializeField] private Transform _lookAtTarget;

        private Vector3 _initialPosition;
        private Quaternion _initialRotation;
        
        private Camera _camera;

        private InputAction _lookMouse;
        private InputAction _lookJoystick;

        private bool _isOver = false;

        private void Awake()
        {
            Controls _controls = new Controls();
            _lookMouse = _controls.Camera.LookMouse;
            _lookMouse.Enable();
            _lookJoystick = _controls.Camera.LookJoystick;
            _lookJoystick.Enable();   
        }

        private void Start()
        {
            _followRadius = (transform.position - _rotateAroundTarget.position).magnitude;
            _initialPosition = transform.position;
            transform.LookAt(_lookAtTarget.position, Vector3.up);
            _initialRotation = transform.rotation;
            _camera = GetComponentInChildren<Camera>();
            this.enabled = false;
            _camera.enabled = false;

            if (_rotateAroundTarget == null)
            {
                throw new MissingReferenceException(this.name + " is missing a rotate around target! ");
            }
        }

        private void LateUpdate()
        {
            // Start by reading input and follow target movement
            Vector2 deltaLookMouse = _lookMouse.ReadValue<Vector2>();
            Vector2 deltaLookJoystick = _lookJoystick.ReadValue<Vector2>();

            Vector2 deltaLook;
            float rotationSensitivity;

            if (deltaLookMouse.sqrMagnitude > deltaLookJoystick.sqrMagnitude)
            {
                deltaLook = deltaLookMouse;
                rotationSensitivity = _mouseSensitivity;
            }
            else
            {
                deltaLook = deltaLookJoystick;
                rotationSensitivity = _joystickSensitivity;
            }

            if (deltaLook.sqrMagnitude > 0.2f)
            {
                deltaLook *= rotationSensitivity;
                UpdateSensitivity(deltaLook);
                Rotate(deltaLook);
            }

            transform.position -= _collisionZoom;
            _collisionZoom = CalculateCollisionZoom();
            transform.position += _collisionZoom;

        }

        public override void DequeueCameraMode()
        {
            base.DequeueCameraMode();
            _isOver = true;
        }

        private float _followRadius;
        private void Rotate(Vector2 rotation)
        {
            // No need to rotate if there is no rotation
            if (rotation.sqrMagnitude > 0f)
            {

                float dot = Vector3.Dot((transform.position - _rotateAroundTarget.position).normalized, Vector3.up);

                // Rotating the camera around the local Y axis
                transform.RotateAround(_rotateAroundTarget.position, Vector3.up, rotation.x * _currentSensitivity * Time.deltaTime);

                // Stopping the camera before it's aligned with the Global up axis
                if ((dot > 0.0f && (1.0f - dot > _stopRangeUp || rotation.y > 0.0f)) ||
                    (dot < 0.0f && (dot + 1.0f > _stopRangeDown || rotation.y < 0.0f)))
                {
                    // Rotating the camera around the local X axis
                    transform.RotateAround(_rotateAroundTarget.position, -transform.right, rotation.y * _currentSensitivity * Time.deltaTime);
                }

                // If the rotation causes the radius to change, then correct it
                Vector3 cameraToTarget = transform.position - _rotateAroundTarget.position;
                if (_collisionZoom.sqrMagnitude == 0f && Mathf.Abs(cameraToTarget.magnitude - _followRadius) > float.Epsilon)
                {
                    transform.position = _rotateAroundTarget.position + cameraToTarget.normalized * _followRadius;
                }
            }

            transform.LookAt(_lookAtTarget.position, Vector3.up);
        }

        private Vector3 _collisionZoom;
        /* Checks for camera collisions by casting the near plane from the player
         * Returns the change in position to stop colliding
         * Adapted from: https://www.gamedeveloper.com/programming/accurate-collision-zoom-for-cameras 
         */
        private Vector3 CalculateCollisionZoom()
        {
            Vector3[] frustumCorners = new Vector3[4];
            _camera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), _camera.nearClipPlane, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);

            Vector3 camPosition = transform.position;
            Vector3 centerOfNearPlane = camPosition + (_lookAtTarget.position - camPosition).normalized * _camera.nearClipPlane;
            Vector3 rayDirection = centerOfNearPlane - _lookAtTarget.position;

            RaycastHit hit;
            float minHitDistance = rayDirection.magnitude;

            for (int i = 0; i < 4; i++)
            {
                Vector3 camToCorner = _camera.transform.TransformVector(frustumCorners[i]);
                Vector3 worldSpaceCorner = camPosition + camToCorner * 5f; //scale near plane for better collision detection, does not go into ground

                Vector3 cornerOffset = worldSpaceCorner - centerOfNearPlane;

                Vector3 cornerAtLookAt = _lookAtTarget.position + cornerOffset;

                // Cast ray from lookAt corner towards near clip corner 
                if (Physics.Raycast(cornerAtLookAt, rayDirection.normalized, out hit, rayDirection.magnitude) && hit.collider.CompareTag("LargeObject"))
                {
                    minHitDistance = Mathf.Min(hit.distance, minHitDistance);
                }
            }

            float moveDistance = rayDirection.magnitude - minHitDistance;

            if (moveDistance > 0.0f)
            {
                return -rayDirection.normalized * moveDistance;
            }

            return Vector3.zero;
        }

        private void UpdateSensitivity(Vector3 deltaLook)
        {
            if (deltaLook.sqrMagnitude == 0f)
            {
                _currentSensitivity = 0f;
            }
            else
            {
                _currentSensitivity += Time.deltaTime * _sensitivityAcceleration;
                _currentSensitivity = Mathf.Min(_maxSensitivity, _currentSensitivity);
            }
        }

        

        public override void TransitionFromThisStart(bool cancelled)
        {
            _lookMouse.Disable();
            _lookJoystick.Disable();
            this.enabled = false;
            _camera.enabled = false;
        }

        public override void TransitionFromThisEnd()
        {
            transform.position = _initialPosition;
            transform.rotation = _initialRotation;
        }

        public override void TransitionToThisStart(Vector3 previousCameraForward, bool previousIsPlayerCam, bool cancelled)
        {
            this.enabled = true;
        }

        public override bool IsPlayerCam() => true;

        public override void TransitionToThisEnd()
        {
            _lookMouse.Enable();
            _lookJoystick.Enable();
            PlayerMainController.Instance.SwapPlayerMovementCamera(_camera);
            _camera.enabled = true;
            _isOver = false;
        }

        public override bool IsModeOver()
        {
            return _isOver;
        }

        public override Transform GetCameraTransform()
        {
            return _camera.transform;
        }

        public override float GetFov()
        {
            return _camera.fieldOfView;
        }

        public override Camera GetCamera()
        {
            return _camera;
        }
    }
}