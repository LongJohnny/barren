﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Assets.Common;
using Assets.Common.Scripts;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Cameras.Scripts
{
    public class CameraController : CameraMode
    {
        public static CameraController Instance;

        [Header("Camera parameters")]
        [SerializeField] [Range(10.0f, 100.0f)] private float _maxSensitivity = 50f;
        [SerializeField] private float _sensitivityAcceleration = 200f;
        private float _currentSensitivity = 0f;

        [SerializeField] private float _joystickSensitivity = 2f;
        [SerializeField] private float _mouseSensitivity = .5f;

        [SerializeField]
        [Range(0.5f, 0.0000000001f)]
        [Tooltip("Controls the angle at which the camera stops")]
        private float _stopRangeUp = 0.001f;

        [SerializeField]
        [Range(0.5f, 0.0000000001f)]
        [Tooltip("Controls the angle at which the camera stops")]
        private float _stopRangeDown = 0.01f;

        [SerializeField] [Range(0.0f, 1.0f)] private float _xMax = .1f;
        [SerializeField] [Range(0.0f, 1.0f)] private float _yMax = .2f;
        [SerializeField] [Range(-1.0f, 0.0f)] private float _yMin = -1f;

        [SerializeField] [Range(1.0f, 20.0f)] private float _automaticHorizontalRotationSpeed = 10.0f;
        [SerializeField] [Range(1.0f, 20.0f)] private float _automaticVerticalRotationSpeed = 1.7f;

        [Header("Camera follow transforms")]
        [SerializeField]
        [Tooltip("The follow target (player)")]
        private Transform _followTarget;
        private Vector3 _oldFollowTargetPosition;
        [SerializeField]
        [Tooltip("Center of the spherical surface where the camera will be moving")]
        private Transform _rotateAroundTarget;
        private Vector3 _lookAtTarget;

        [SerializeField]
        private Transform _ghostFollowTarget;
        [SerializeField]
        private Transform _ghostRotateAroundTarget;

        private float _defaultHeight;

        private Camera _camera;
        private FovUpdater _fovUpdater;

        private InputAction _lookMouse;
        private InputAction _lookJoystick;
        private InputAction _reCenter;
        private InputAction _changeCam;

        // Camera starts with recentering and automatic follow enabled
        private bool _recentering = true;
        private bool _automaticFollow = true;
        private bool _centerLookAt = true;
        private bool _isOver = false;

        private Queue<Vector2> _rotationQueue = new Queue<Vector2>();

        private float _followRadius;
        

        [SerializeField] [Tooltip("Choose if normal follow camera or far away follow cam")] 
        private bool _isFarCamera;
        [SerializeField] [Range(0f, 100f)] private float _distanceRadius = 22f;
        [SerializeField] [Range(0f, 100f)] private float _extraRadius = 20f;

        private void Awake()
        {
            Controls _controls = new Controls();
            _lookMouse = _controls.Camera.LookMouse;
            _lookMouse.Enable();
            _lookJoystick = _controls.Camera.LookJoystick;
            _lookJoystick.Enable();
            _reCenter = _controls.Camera.Recenter;
            _reCenter.Enable();
            _changeCam = _controls.Camera.CameraType;
            _changeCam.Enable();
            _camera = GetComponentInChildren<Camera>();
            if(!_isFarCamera) Instance = this;
            Cursor.visible = false;

            
        }

        private void Start()
        {
            _oldFollowTargetPosition = _followTarget.position;
            //_followRadius = (transform.position - _rotateAroundTarget.position).magnitude;
            _followRadius = _distanceRadius;
            _lookAtTarget = _followTarget.position + (_rotateAroundTarget.position - _followTarget.position) * 0.3f;
            _defaultHeight = transform.position.y - _followTarget.position.y;

            if (_isFarCamera)
            {
                _followRadius += _extraRadius;
            }


            _fovUpdater = GetComponentInChildren<FovUpdater>();
            //_ghostFollowTarget.position = _followTarget.position;
            //_ghostRotateAroundTarget.position = _rotateAroundTarget.position;
        }

        private void LateUpdate()
        {
            // Start by reading input and follow target movement
            Vector2 deltaLookMouse = _lookMouse.ReadValue<Vector2>();
            Vector2 deltaLookJoystick = _lookJoystick.ReadValue<Vector2>();

            Vector2 deltaLook;
            float rotationSensitivity;
            if (deltaLookMouse.sqrMagnitude > deltaLookJoystick.sqrMagnitude)
            {
                deltaLook = deltaLookMouse;
                rotationSensitivity = _mouseSensitivity;
            }
            else
            {
                deltaLook = deltaLookJoystick;
                rotationSensitivity = _joystickSensitivity;
            }

            _automaticFollow = _reCenter.triggered || _automaticFollow;
            if (_reCenter.triggered)
            {
                _recentering = true;
            }
            if (_changeCam.triggered)
            {
                _centerLookAt = !_centerLookAt;
            }
            Vector3 followTargetMovement = _followTarget.position - _oldFollowTargetPosition;
            _oldFollowTargetPosition = _followTarget.position;

            Move(followTargetMovement);

            if (_rotationQueue.Count > 0)
            {
                Vector2 rotation = Vector2.zero;
                while (_rotationQueue.Count > 0)
                {
                    rotation += _rotationQueue.Dequeue();
                }

                Rotate(rotation, true);
            }
            else
            {
                if (deltaLook.sqrMagnitude > 0.2f)
                {
                    deltaLook *= rotationSensitivity;
                    UpdateSensitivity(deltaLook);
                    Rotate(deltaLook);
                    _automaticFollow = false;
                }
                else if (_automaticFollow)
                {
                    deltaLook = CalculateAutomaticRotation(followTargetMovement);
                    UpdateSensitivity(deltaLook);
                    Rotate(deltaLook);
                }
            }

            transform.position -= _collisionZoom;
            _collisionZoom = CalculateCollisionZoom();
            transform.position += _collisionZoom;
        }

        private Vector3 desiredLookAtPosition = Vector3.zero;
        private void Move(Vector3 movement)
        {
            if (movement.sqrMagnitude > 0f)
            {
                _rotateAroundTarget.position += movement;
                Vector3 movementCam = Vector3.zero;

                Vector3 oldDesired = desiredLookAtPosition;
                desiredLookAtPosition = _followTarget.position + (_rotateAroundTarget.position - _followTarget.position) * 0.3f;
                //_lookAtTarget = Vector3.SmoothDamp(oldXLookAtPosition, desiredLookAtPosition, ref _velocity, smoothTime);

                Vector3 lookChange = desiredLookAtPosition - _lookAtTarget;
                Vector3 lookPan = Vector3.ProjectOnPlane(lookChange, transform.forward);
                lookPan = Vector3.ProjectOnPlane(lookPan, transform.up);     //movement of cam from one frame to other only in x of camera

                //_lookAtTarget = desiredLookAtPosition - lookPan / 1.1f;
                if (_centerLookAt)
                {
                    transform.position += movement;
                    _lookAtTarget = _followTarget.position + (_rotateAroundTarget.position - _followTarget.position) * 0.3f;  //0.3f is for lower third of screen
                }
                else
                {
                    if (WithinBounds(movement))
                    {
                        Vector3 moveDir = Vector3.Project(movement, transform.right);
                        //Debug.Log(moveDir);
                        _lookAtTarget = desiredLookAtPosition - lookPan + moveDir * .8f;
                        //Debug.Log(_lookAtTarget);
                        transform.position += Vector3.ProjectOnPlane(movement, _camera.transform.right);
                    }
                    else
                    {
                        transform.position += movement;
                        Vector3 lookChangeNew = desiredLookAtPosition - oldDesired;
                        Vector3 lookPanNew = Vector3.ProjectOnPlane(lookChangeNew, transform.forward);
                        lookPanNew = Vector3.ProjectOnPlane(lookPanNew, transform.up);
                        _lookAtTarget = desiredLookAtPosition - lookPan + lookPanNew;
                        //_lookAtTarget = Vector3.Lerp(_lookAtTarget, desiredLookAtPosition - lookPan + lookPanNew, 0.5f);
                    }
                }

            }
        }

        private bool WithinBounds(Vector3 movement)
        {
            // THIS IS WORKING
            movement = Vector3.ProjectOnPlane(movement, transform.forward);
            float moveDir = Vector3.Dot(movement, _camera.transform.right); //if positive, move is to the right, negative is to left
            Vector3 followTargetViewPort = _camera.WorldToViewportPoint(_followTarget.position) * 2f - new Vector3(1f, 1f, 0f);


            // If the player is within the movement bounds then don't follow in the local X 
            return (followTargetViewPort.x <= _xMax && followTargetViewPort.x >= -_xMax) ||
                   (followTargetViewPort.x <= -_xMax && moveDir > 0) ||
                   (followTargetViewPort.x >= _xMax && moveDir < 0);
        }

        private void Rotate(Vector2 rotation, bool forceMaxSensitivity = false)
        {
            // No need to rotate if there is no rotation
            if (rotation.sqrMagnitude > 0f)
            {
                float sensitivity = forceMaxSensitivity ? _maxSensitivity : _currentSensitivity;

                float dot = Vector3.Dot((transform.position - _rotateAroundTarget.position).normalized, Vector3.up);

                // Rotating the camera around the local Y axis
                transform.RotateAround(_rotateAroundTarget.position, Vector3.up, rotation.x * sensitivity * Time.deltaTime);

                // Stopping the camera before it's aligned with the Global up axis
                if ((dot > 0.0f && (1.0f - dot > _stopRangeUp || rotation.y > 0.0f)) ||
                    (dot < 0.0f && (dot + 1.0f > _stopRangeDown || rotation.y < 0.0f)))
                {
                    // Rotating the camera around the local X axis
                    transform.RotateAround(_rotateAroundTarget.position, -transform.right, rotation.y * sensitivity * Time.deltaTime);
                }

                // If the rotation causes the radius to change, then correct it
                Vector3 cameraToTarget = transform.position - _rotateAroundTarget.position;
                if (_collisionZoom.sqrMagnitude == 0f && Mathf.Abs(cameraToTarget.magnitude - _followRadius) > float.Epsilon)
                {
                    transform.position = _rotateAroundTarget.position + cameraToTarget.normalized * _followRadius;
                }
            }

            transform.LookAt(_lookAtTarget, Vector3.up);
        }

        public void EnqueueRotation(Vector2 rotation)
        {
            _rotationQueue.Enqueue(rotation);
        }

        private Vector3 _collisionZoom;
        /* Checks for camera collisions by casting the near plane from the player
         * Returns the change in position to stop colliding
         * Adapted from: https://www.gamedeveloper.com/programming/accurate-collision-zoom-for-cameras 
         */
        private Vector3 CalculateCollisionZoom()
        {
            Vector3[] frustumCorners = new Vector3[4];
            _camera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), _camera.nearClipPlane, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);

            Vector3 camPosition = transform.position;
            Vector3 centerOfNearPlane = camPosition + (_lookAtTarget - camPosition).normalized * _camera.nearClipPlane;
            Vector3 rayDirection = centerOfNearPlane - _lookAtTarget;

            RaycastHit hit;
            float minHitDistance = rayDirection.magnitude;

            for (int i = 0; i < 4; i++)
            {
                Vector3 camToCorner = _camera.transform.TransformVector(frustumCorners[i]);
                Vector3 worldSpaceCorner = camPosition + camToCorner * 5f; //scale near plane for better collision detection, does not go into ground

                Vector3 cornerOffset = worldSpaceCorner - centerOfNearPlane;

                Vector3 cornerAtLookAt = _lookAtTarget + cornerOffset;

                // Cast ray from lookAt corner towards near clip corner 
                if (Physics.Raycast(cornerAtLookAt, rayDirection.normalized, out hit, rayDirection.magnitude) && hit.collider.CompareTag("LargeObject"))
                {
                    minHitDistance = Mathf.Min(hit.distance, minHitDistance);
                }
            }

            float moveDistance = rayDirection.magnitude - minHitDistance;

            if (moveDistance > 0.0f)
            {
                return -rayDirection.normalized * moveDistance;
            }

            return Vector3.zero;
        }

        private void UpdateSensitivity(Vector3 deltaLook)
        {
            if (deltaLook.sqrMagnitude == 0f)
            {
                _currentSensitivity = 0f;
            }
            else
            {
                _currentSensitivity += Time.deltaTime * _sensitivityAcceleration;
                _currentSensitivity = Mathf.Min(_maxSensitivity, _currentSensitivity);
            }
        }

        private Vector2 CalculateAutomaticRotation(Vector3 movement)
        {
            Vector2 automaticDeltaLook = Vector2.zero;

            float heightDiff = _defaultHeight - (transform.position.y - _followTarget.position.y);

            if (Mathf.Abs(heightDiff) > 0.05f && _recentering)
            {
                automaticDeltaLook.y = -heightDiff * .1f;
            } 
            else if (movement.sqrMagnitude > 0.0001f)
            {
                _recentering = false;

                // The camera rotates less or more depending on the alignment of the movement with the
                // right and up direction. Also if the movement is very large then the dot product will result
                // in even larger rotations.
                automaticDeltaLook.x = Vector3.Dot(movement, transform.right) * _automaticHorizontalRotationSpeed;
                automaticDeltaLook.y = Vector3.Dot(movement, transform.up) * _automaticVerticalRotationSpeed;
                // Rotate more upwards then downwards
                automaticDeltaLook.y = automaticDeltaLook.y < 0f ? automaticDeltaLook.y : automaticDeltaLook.y * .4f;

                if (Mathf.Abs(heightDiff) > 0.05f)
                {
                    automaticDeltaLook.y += -heightDiff * .1f;
                }
            }

            return automaticDeltaLook;

        }

        public void ResetCameraOptions(Vector3 newLookAtDirection)
        {
            //_distanceRadius = 22f;
            _camera.fieldOfView = _fovUpdater.MinFOV;

            //for the tutorial!!!  Change where to look
            transform.position = _lookAtTarget - newLookAtDirection * _followRadius;

            Vector3 cameraToTarget = transform.position - _rotateAroundTarget.position;
            if (_collisionZoom.sqrMagnitude == 0f && Mathf.Abs(cameraToTarget.magnitude - _followRadius) > float.Epsilon)
            {
                transform.position = _rotateAroundTarget.position + cameraToTarget.normalized * _followRadius;
            }

            transform.LookAt(_lookAtTarget, Vector3.up);
        }

        public override void TransitionFromThisStart(bool cancelled)
        {
            _lookMouse.Disable();
            _lookJoystick.Disable();
            _camera.enabled = false;
        }

        public override void TransitionToThisEnd()
        {
            _lookMouse.Enable();
            _lookJoystick.Enable();
            PlayerMainController.Instance.SwapPlayerMovementCamera(_camera);
            _camera.enabled = true;
            _recentering = true;
            _isOver = false;
        }

        public override void TransitionToThisStart(Vector3 previousCameraForward, bool previousIsPlayerCam, bool cancelled)
        {
            if (previousIsPlayerCam)
            {
                transform.position = _lookAtTarget - previousCameraForward * _followRadius;

                // If the rotation causes the radius to change, then correct it
                Vector3 cameraToTarget = transform.position - _rotateAroundTarget.position;
                if (_collisionZoom.sqrMagnitude == 0f && Mathf.Abs(cameraToTarget.magnitude - _followRadius) > float.Epsilon)
                {
                    transform.position = _rotateAroundTarget.position + cameraToTarget.normalized * _followRadius;
                }

                transform.LookAt(_lookAtTarget, Vector3.up);
            }
        }

        public override bool IsPlayerCam() => true;

        public override Transform GetCameraTransform()
        {
            return _camera.transform;
        }

        public override float GetFov()
        {
            return _camera.fieldOfView;
        }

        public override Camera GetCamera()
        {
            return _camera;
        }

        public override bool IsModeOver()
        {
            if (_isFarCamera) return _isOver;

            return base.IsModeOver();
        }

        public override void DequeueCameraMode()
        {
            base.DequeueCameraMode();
            _isOver = true;
        }
    }
}