using System;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Cameras.Scripts
{
    public class CameraFollow2D : CameraMode
    {
        [Tooltip("The follow target (player)")]
        [SerializeField] private Transform _followTarget;
        [SerializeField] private float _heightOffset = 5f;
        [SerializeField] private float _distanceToPlayer = 50f;
        [SerializeField] [Range(10.0f, 100.0f)] private float _fieldOfView = 50f;

        private Camera _camera;
        private bool _isOver = false;


        private void Start()
        {
            _camera = GetComponentInChildren<Camera>();
            _camera.enabled = false;
            _camera.fieldOfView = _fieldOfView;

            if (_followTarget == null)
            {
                throw new MissingReferenceException(this.name + " is missing a follow target! ");
            }

            //_camera.transform.forward = (_followTarget.position - this.transform.position).normalized;
            transform.LookAt(_followTarget, Vector3.up);
        }

        public override void DequeueCameraMode()
        {
            base.DequeueCameraMode();
            _isOver = true;
        }

        private void LateUpdate()
        {
            Vector3 desiredLookAt = _followTarget.position;
            desiredLookAt.y += _heightOffset;
            Vector3 position = _followTarget.position - Vector3.right * _distanceToPlayer;
            position.y = _followTarget.position.y + _heightOffset;
            transform.position = position;
            transform.LookAt(desiredLookAt, Vector3.up);
            _camera.fieldOfView = _fieldOfView;
        }

        public override void TransitionToThisStart(Vector3 previousCameraForward, bool previousIsPlayerCam, bool cancelled)
        {
            this.enabled = true;
        }

        public override bool IsPlayerCam() => true;

        public override void TransitionToThisEnd()
        {
            _camera.enabled = true;
            PlayerMainController.Instance.SwapPlayerMovementCamera(_camera);
            _isOver = false;
        }

        public override void TransitionFromThisStart(bool cancelled)
        {
            _camera.enabled = false;
            this.enabled = false;
        }

        public override bool IsModeOver()
        {
            return _isOver;
        }

        public override Transform GetCameraTransform()
        {
            return transform;
        }

        public override float GetFov()
        {
            return _camera.fieldOfView;
        }

        public override Camera GetCamera()
        {
            return _camera;
        }
    }
}