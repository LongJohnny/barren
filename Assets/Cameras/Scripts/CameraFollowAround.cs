﻿using System.Collections;
using UnityEngine;

namespace Assets.Cameras.Scripts
{
    public class CameraFollowAround : CameraMode
    {
        [Tooltip("The follow target (player)")]
        [SerializeField] private Transform _followTarget;
        [SerializeField] private Transform _rotationAxis;
        [SerializeField][Range(0f, 60f)] private float _followDistance = 10f;
        [SerializeField][Range(-60f, 60f)] private float _verticalFollowOffset = 30f; // In degrees
        [SerializeField][Range(-60f, 60f)] private float _horizontalFollowOffset = 30f; // In degrees
        private Camera _camera;
        private bool _isOver = true;


        void Start()
        {
            _camera = GetComponentInChildren<Camera>();
            this.enabled = false;
            _camera.enabled = false;

            if (_followTarget == null)
            {
                throw new MissingReferenceException(this.name + " is missing a follow target! ");
            }

            if (_rotationAxis == null)
            {
                throw new MissingReferenceException(this.name + " is missing a rotation axis! ");
            }
        }

        void LateUpdate()
        {
            Vector3 p = _rotationAxis.position + Vector3.Project(_followTarget.position - _rotationAxis.position, _rotationAxis.up);
            Vector3 playerDir = _followTarget.position - p;
            
            transform.position = _followTarget.position + playerDir.normalized*_followDistance;
            transform.RotateAround(_followTarget.position, _rotationAxis.up, _horizontalFollowOffset);

            Vector3 verticalAxis = Vector3.Cross(_rotationAxis.up, -playerDir.normalized);
            transform.RotateAround(_followTarget.position, verticalAxis, _verticalFollowOffset);
            transform.LookAt(_followTarget);            
        }

        public override void DequeueCameraMode()
        {
            base.DequeueCameraMode();
            _isOver = true;
        }

        public override void TransitionToThisStart(Vector3 previousCameraForward, bool previousIsPlayerCam, bool cancelled)
        {
            this.enabled = true;
        }

        public override bool IsPlayerCam() => true;

        public override void TransitionToThisEnd()
        {
            _camera.enabled = true;
            PlayerMainController.Instance.SwapPlayerMovementCamera(_camera);
            _isOver = false;
        }

        public override void TransitionFromThisStart(bool cancelled)
        {
            _camera.enabled = false;
            this.enabled = false;
        }

        public override Transform GetCameraTransform()
        {
            return transform;
        }

        public override float GetFov()
        {
            return _camera.fieldOfView;
        }

        public override Camera GetCamera()
        {
            return _camera;
        }

        public override bool IsModeOver()
        {
            return _isOver;
        }
    }
}