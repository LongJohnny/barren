﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Cameras.Scripts
{
    public class CameraFollow : CameraMode
    {
        [Tooltip("The follow target (player)")]
        [SerializeField] private Transform _followTarget;

        private Camera _camera;
        private bool _isOver = true;

        private void Start()
        {
            _camera = GetComponentInChildren<Camera>();
            this.enabled = false;
            _camera.enabled = false;

            if (_followTarget == null)
            {
                throw new MissingReferenceException(this.name + " is missing a follow target! ");
            }
        }

        public override void DequeueCameraMode()
        {
            base.DequeueCameraMode();
            _isOver = true;
        }

        private void LateUpdate()
        {
            transform.position = Vector3.ProjectOnPlane(_followTarget.position, transform.forward); ;
        }

        public override void TransitionToThisStart(Vector3 previousCameraForward, bool previousIsPlayerCam, bool cancelled)
        {
            this.enabled = true;
        }

        public override bool IsPlayerCam() => true;

        public override void TransitionToThisEnd()
        {
            _camera.enabled = true;
            PlayerMainController.Instance.SwapPlayerMovementCamera(_camera);
            _isOver = false;
        }

        public override void TransitionFromThisStart(bool cancelled)
        {
            _camera.enabled = false;
            this.enabled = false;
        }

        public override bool IsModeOver()
        {
            return _isOver;
        }

        public override Transform GetCameraTransform()
        {
            return transform;
        }

        public override float GetFov()
        {
            return _camera.fieldOfView;
        }

        public override Camera GetCamera()
        {
            return _camera;
        }
    }
}