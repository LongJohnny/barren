﻿using System.Collections;
using CameraShake;
using UnityEngine;

namespace Assets.Cameras.Scripts
{
    public class CameraShake : MonoBehaviour
    {
        public void TrembleShake(float intensity)
        {
            CameraShaker.Instance.ShakePresets.ShortShake3D(0.12f * (1f - intensity), 50f, 1);
        }
    }
}