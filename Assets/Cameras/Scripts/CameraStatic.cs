﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Cameras.Scripts
{
    public class CameraStatic : CameraMode
    {
        private Camera _camera;

        [SerializeField] private bool _temporary = true;
        [SerializeField] private float _delay = 0f;
        private bool _isOver = true;

        private void Start()
        {
            _camera = GetComponentInChildren<Camera>();
            _camera.enabled = false;

            if (_delay > 0f && !_temporary)
            {
                throw new ArgumentException("Cannot have a permanent camera with delay!");
            }
        }

        public override void TransitionFromThisStart(bool cancelled)
        {
            _camera.enabled = false;
            _isOver = true;
        }

        public override void TransitionToThisEnd()
        {
            _camera.enabled = true;
            if (_delay > 0f)
            {
                _isOver = false;
                StartCoroutine(Wait(_delay));
            } 
            else if (!_temporary)
            {
                _isOver = false;
            }
        }

        public override void DequeueCameraMode()
        {
            base.DequeueCameraMode();
            if (!_temporary)
            {
                _isOver = true;
            }
        }

        public override Transform GetCameraTransform()
        {
            return transform;
        }

        public override bool IsModeOver()
        {
            return _isOver;
        }

        public override float GetFov()
        {
            return _camera.fieldOfView;
        }

        public override Camera GetCamera()
        {
            return _camera;
        }

        IEnumerator Wait(float delay)
        {
            yield return new WaitForSeconds(delay);
            _isOver = true;

        }
    }
}