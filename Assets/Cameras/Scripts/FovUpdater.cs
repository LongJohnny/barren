using Assets.Common.Scripts;
using UnityEngine;

namespace Assets.Cameras.Scripts
{
    [RequireComponent(typeof(Camera))]
    public class FovUpdater : MonoBehaviour
    {
        [SerializeField] private float _maxFov;
        [SerializeField] private float _minFov;
        [SerializeField] private float _fovMaxIncreseRate;
        [SerializeField] private float _fovDecreaseRate;

        private Vector3 _currentPlayerVelocity;
        private Vector3 _oldPlayerPosition;
        private Camera _camera;

        public float MinFOV { get => _minFov; }

        void Start() {
            _camera = GetComponent<Camera>();
            _camera.fieldOfView = _minFov;
            _oldPlayerPosition = PlayerMainController.Instance.PlayerPosition;
        }

        void LateUpdate()
        {

            //_currentPlayerVelocity = (PlayerMainController.Instance.PlayerPosition - _oldPlayerPosition) * 70f;

            //_oldPlayerPosition = PlayerMainController.Instance.PlayerPosition;
            _currentPlayerVelocity = PlayerMainController.Instance.PlayerVelocity;

            if (Vector3.Dot(transform.forward, _currentPlayerVelocity) > 0.2 && PlayerMainController.Instance.PlayerVelocity.magnitude > 30f) {

                // Calculating new fov based of the current speed
                float newFov = Mathf.Lerp(_minFov, _maxFov,
                    MathAux.SmoothStep(0.5f, 1f, _currentPlayerVelocity.magnitude / (PlayerMainController.Instance.TerminalVelocity)));

                // Checking if fov change is larger the threshold
                float fovChange = newFov - _camera.fieldOfView;
                if (Mathf.Abs(fovChange) > _fovMaxIncreseRate * Time.deltaTime) {
                    newFov = _camera.fieldOfView + _fovMaxIncreseRate * Time.deltaTime * Mathf.Sign(fovChange);
                }

                // Clamping fov, so that it does go above of below the defualt and max, respectively
                _camera.fieldOfView = Mathf.Clamp(newFov, _minFov, _maxFov);

            } else {
                float newFov = _camera.fieldOfView - _fovDecreaseRate * Time.deltaTime;
                // Clamping fov, so that it does go above of below the defualt and max, respectively
                _camera.fieldOfView = Mathf.Clamp(newFov, _minFov, _maxFov);
            }
        }


    }
}
