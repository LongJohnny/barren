﻿using System;
using System.Collections;
using System.Runtime.Serialization;
using Assets.Global;
using UnityEngine;
using UnityEngine.Events;
using Assets.Mechanics.CarryingEnergy.Scripts;

namespace Assets.Mechanics.ObjectRotation
{
    public class ObjectRotationMechanism : Activity
    {
        [SerializeField] private UnityEvent<float> _onRotateEvent;
        [SerializeField] private UnityEvent _onAttachEvent;
        [SerializeField] private UnityEvent _onDetachEvent;
        [SerializeField] private UnityEvent _onSnapMaxEvent;
        [SerializeField] private UnityEvent _onSnapMinEvent;
        [SerializeField] private Transform _mechanismToRotate;
        [SerializeField] private Transform _attachmentSlot;
        [SerializeField] private bool _rotatesLights;
        [SerializeField] private PillarsAnimation _pillars;
        [SerializeField] private float _rotationSpeed = 30f;
        [SerializeField] private float _attachmentSpeed = 5f;
        [SerializeField] private float _pauseDuration = 2f;
        [SerializeField] private float _minRotation = 0f;
        [SerializeField] private float _maxRotation = 0f;
        [SerializeField] private float _rotateAtTheStart = 0f;
        [SerializeField] private bool _deactivateOnSnapMax = false;
        [SerializeField] private bool _deactivateOnSnapMin = false;
        [SerializeField] private bool _rotateRightOnly = false;
        [SerializeField] private bool _rotateLeftOnly = false; 

        private float _currRotation = 0f;

        private float _horizontalInput;

        private Controls _controls;

        private bool _inSlot = false;
        private bool _detectPlayer = true;

        private void Start()
        {
            _controls = new Controls();
            _controls.Movement.MoveX.Enable();
            _controls.Movement.Jump.Enable();
            if (_rotateAtTheStart != 0f)
            {
                _mechanismToRotate.Rotate(0.0f, _rotateAtTheStart, 0.0f, Space.Self);

                if (Mathf.Abs(_rotateAtTheStart) > 0.0001f)
                {
                    _onRotateEvent.Invoke(_rotateAtTheStart/_rotationSpeed);
                }

                _currRotation += _rotateAtTheStart;
            }
            _rotatesLights = true;

            if (_pillars == null)
            {
                _rotatesLights = false;
                //throw new MissingComponentException("No pillars in object rotation mechanism");
            }
        }

        private Vector3 _startPlayerPosition = Vector3.zero;
        private float _k = 0f;
        private void Update()
        {
            if (  (_inSlot && !_rotatesLights) || (_inSlot && _pillars.Raised))
            {
                _horizontalInput = _controls.Movement.MoveX.ReadValue<float>();

                if (_rotateRightOnly && _horizontalInput < 0f)
                {
                    _horizontalInput = 0f;
                }

                if (_rotateLeftOnly && _horizontalInput > 0f)
                {
                    _horizontalInput = 0f;
                }

                float scaledInput = _horizontalInput * Time.deltaTime;

                float rotation = _rotationSpeed * scaledInput;

                bool snapped = false;
                if (Math.Abs(_minRotation - _maxRotation) > float.Epsilon)
                {
                    _currRotation += rotation;

                    if (_currRotation < _minRotation)
                    {
                        rotation -= (_currRotation - _minRotation);
                        _currRotation = Mathf.Clamp(_currRotation, _minRotation, _maxRotation);
                        _onSnapMinEvent.Invoke();
                        snapped = true;

                        if (_deactivateOnSnapMin)
                        {
                            this.enabled = false;
                            _detectPlayer = false;
                            _inSlot = false;
                            PlayerMainController.Instance.EnablePlayer();
                            _onDetachEvent.Invoke();
                        }
                    }
                    else if (_currRotation > _maxRotation)
                    {
                        rotation += (_maxRotation - _currRotation);
                        _currRotation = Mathf.Clamp(_currRotation, _minRotation, _maxRotation);
                        _onSnapMaxEvent.Invoke();
                        snapped = true;

                        if (_deactivateOnSnapMax)
                        {
                            this.enabled = false;
                            _detectPlayer = false;
                            _inSlot = false;
                            PlayerMainController.Instance.EnablePlayer();
                            _onDetachEvent.Invoke();
                        }
                    }
                }

                if (!snapped)
                {
                    PlayerMainController.Instance.transform.Rotate(0.0f, rotation, 0.0f, Space.Self);
                    _mechanismToRotate.Rotate(0.0f, rotation, 0.0f, Space.World);
                    //_mechanismToRotate.RotateAround(_attachmentSlot.transform.position, Vector3.up, rotation);

                    if (Mathf.Abs(rotation) > 0.0001f)
                    {
                        _onRotateEvent.Invoke(scaledInput);
                    }
                }

                if (_controls.Movement.Jump.triggered)
                {
                    _onDetachEvent.Invoke();
                    _inSlot = false;
                    PlayerMainController.Instance.EnablePlayer();
                    PlayerMainController.Instance.Jump();
                    StartCoroutine(Pause());
                    this.enabled = false;
                }
            }
            else if (_inSlot && _controls.Movement.Jump.triggered)
            {
                _onDetachEvent.Invoke();
                _inSlot = false;
                PlayerMainController.Instance.EnablePlayer();
                PlayerMainController.Instance.Jump();
                StartCoroutine(Pause());
                this.enabled = false;
            }

        }

        public void PullPlayerToAttachmentSlot()
        {
            if (_detectPlayer)
            {
                _detectPlayer = false;
                _startPlayerPosition = PlayerMainController.Instance.PlayerPosition;
                PlayerMainController.Instance.ClearPlayerPhysics();
                PlayerMainController.Instance.DisablePlayer();
                StartCoroutine(PullPlayerCoroutine());
            }
        }

        private IEnumerator PullPlayerCoroutine()
        {
            do
            {
                _k += Time.deltaTime * _attachmentSpeed;
                _k = Mathf.Min(1f, _k);
                PlayerMainController.Instance.transform.position = Vector3.Slerp(_startPlayerPosition, _attachmentSlot.position, _k);

                 yield return new WaitForEndOfFrame();
                
            } while (_k < 1f);

            _k = 0f;
            _onAttachEvent.Invoke();
            _inSlot = true;
            this.enabled = true;
        }


        private IEnumerator Pause()
        {
            yield return new WaitForSeconds(_pauseDuration);
            _detectPlayer = true;
        }

        public override bool IsCompleted()
        {
            return !this.enabled;
        }

        public override void CompleteActivity()
        {
            this.enabled = false;
            _detectPlayer = false;
        }

        public override ISerializable[] GetSaveInformation()
        {
            return new ISerializable[] { new SFloat(_currRotation) };
        }

        public override void LoadFromSaveInformation(ISerializable[] saveInformation)
        {
            _currRotation = saveInformation[0] as SFloat;
        }
    }
}