﻿using System.Collections;
using System.Runtime.Serialization;
using Assets.Global;
using UnityEngine;

namespace Assets.Mechanics.ObjectRotation
{
    public class Rotates : Activity
    {
        [SerializeField] private Transform _rotateAround;
        [SerializeField] private Vector3 _rotationAxis = Vector3.up;
        [SerializeField] private float _rotationSpeed = 1f;

        public void Rotate(float rotation)
        {
            rotation *= _rotationSpeed;
            if (_rotateAround == null)
            {
                transform.Rotate(_rotationAxis, rotation);
            }
            else
            {
                transform.RotateAround(_rotateAround.position, _rotationAxis, rotation);
            }
        }

        public override ISerializable[] GetSaveInformation()
        {
            SVector3 position = transform.position;
            SVector3 rotation = transform.rotation.eulerAngles;
            return new ISerializable[] { position, rotation };
        }

        public override void LoadFromSaveInformation(ISerializable[] saveInformation)
        {
            transform.position = saveInformation[0] as SVector3;
            transform.rotation = Quaternion.Euler(saveInformation[1] as SVector3);
        }
    }
}