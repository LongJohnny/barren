﻿using System.Runtime.Serialization;
using Assets.Global;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Mechanics.ObjectRotation
{
    public class RotateAnimation : Activity
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private string _animationStateName = "New Animation";
        [SerializeField] private float _playSpeed = .01f;
        [SerializeField] private UnityEvent _onAnimationEnd;
        [SerializeField] private bool _reverseAnimation = true;

        private float _currentTime = 0f;
        private bool _animationComplete = false;

        private void Start()
        {
            if (!_animationComplete)
            {
                _animator.speed = 0f;

                if (_reverseAnimation)
                    _currentTime = 1f;

                _animator.Play(_animationStateName, 0, _currentTime);
            }

        }

        public void Rotate(float rotation)
        {
            if (!_animationComplete)
            {
                _currentTime += rotation * _playSpeed;
                _currentTime = Mathf.Clamp(_currentTime, 0f, 1f);

                if (_reverseAnimation && _currentTime <= 0f)
                {
                    _animationComplete = true;
                    _onAnimationEnd.Invoke();
                }
                else if (!_reverseAnimation && _currentTime >= 1f)
                {
                    _animationComplete = true;
                    _onAnimationEnd.Invoke();
                }

                _animator.speed = 0f;
                _animator.Play(_animationStateName, 0, _currentTime);
            }
        }

        public override void CompleteActivity()
        {
            _animationComplete = true;
        }

        public override bool IsCompleted()
        {
            return _animationComplete;
        }

        public override ISerializable[] GetSaveInformation()
        {
            return new ISerializable[] {new SFloat(_currentTime)};
        }

        public override void LoadFromSaveInformation(ISerializable[] saveInformation)
        {
            _currentTime = saveInformation[0] as SFloat;
            _animator.speed = 0f;
            _animator.Play(_animationStateName, 0, _currentTime);
        }
    }
}