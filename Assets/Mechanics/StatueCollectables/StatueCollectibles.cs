﻿using System.Collections;
using System.Runtime.Remoting.Channels;
using Assets.Common.Scripts;
using Assets.Mechanics.ObjectRotation;
using Assets.Player.Scripts;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Mechanics.StatueCollectables
{
    public class StatueCollectibles : Activity
    {
        [SerializeField] private float _moveToPositionDuration = 4f;
        private OrbitCollectiblesController _orbitCollectiblesController;
        private ObjectRotationMechanism _objectRotationMechanism;
        [SerializeField] private DelayedTrigger _pullPlayerArea;
        [SerializeField] private DelayedTrigger _completeStatueTrigger;
        [SerializeField] private GameObject[] _statueCollectibles;
        [SerializeField] private GameObject _completeStatue;
        [SerializeField] private UnityEvent _onCompleteStatueEvent;

        private Coroutine _movePiecesCoroutine;
        private bool _statueComplete = false;

        private void Start()
        {
            _orbitCollectiblesController = FindObjectOfType<OrbitCollectiblesController>();
            _objectRotationMechanism = GetComponent<ObjectRotationMechanism>();

            _pullPlayerArea.gameObject.SetActive(false);
            _objectRotationMechanism.enabled = false;
            _pullPlayerArea.gameObject.SetActive(false);
        }

        public void TryCompleteStatue()
        {
            if (_movePiecesCoroutine == null && !_statueComplete && _orbitCollectiblesController.PlayerHasAllCollectibles())
            {
                _movePiecesCoroutine = StartCoroutine(MoveCollectiblesToStatue());
                _completeStatueTrigger.gameObject.SetActive(false);
                _statueComplete = true;
                // TODO Success particles
            }
            else
            {
                // TODO Fail particles
            }
        }

        IEnumerator MoveCollectiblesToStatue()
        {

            Vector3[] startPositions = new Vector3[_statueCollectibles.Length];
            Quaternion[] startRotations = new Quaternion[_statueCollectibles.Length];
            Vector3[] startScales = new Vector3[_statueCollectibles.Length];

            Vector3[] endPositions = new Vector3[_statueCollectibles.Length]; 
            Quaternion[] endRotations = new Quaternion[_statueCollectibles.Length]; 
            Vector3[] endScales = new Vector3[_statueCollectibles.Length];


            OrbitCollectible[] orbitCollectibles = _orbitCollectiblesController.GetOrbitCollectibles();

            OrbitCollectible orbitCollectible;
            GameObject statueCollectible;
            for (int i = 0; i < _statueCollectibles.Length; i++)
            {
                orbitCollectible = orbitCollectibles[i];
                statueCollectible = _statueCollectibles[i];

                startPositions[i] = orbitCollectible.transform.position;
                startRotations[i] = orbitCollectible.transform.rotation;
                startScales[i] = orbitCollectible.transform.localScale;

                endPositions[i] = statueCollectible.transform.position;
                endRotations[i] = statueCollectible.transform.rotation;
                endScales[i] = statueCollectible.transform.localScale;

                statueCollectible.SetActive(true);
                orbitCollectible.RemoveFromOrbit();
            }

            
            float currTime = 0f;
            float normalizedTime = 0f;

            do
            {
                currTime += Time.deltaTime;
                normalizedTime = currTime / _moveToPositionDuration;
                normalizedTime = MathAux.SmoothStep(0f, 1f,normalizedTime);
                normalizedTime = Mathf.Min(1f, normalizedTime);

                for (int i = 0; i < _statueCollectibles.Length; i++)
                {
                    statueCollectible = _statueCollectibles[i];

                    statueCollectible.transform.position = Vector3.Slerp(startPositions[i], endPositions[i], normalizedTime);
                    statueCollectible.transform.rotation = Quaternion.Slerp(startRotations[i], endRotations[i], normalizedTime);
                    statueCollectible.transform.localScale = Vector3.Lerp(startScales[i], endScales[i], normalizedTime);

                }

                yield return new WaitForEndOfFrame();

            } while (normalizedTime < 1f);

            foreach (GameObject stCollectible in _statueCollectibles)
            {
                stCollectible.SetActive(false);
            }

            ActivateStatue();
        }

        private void ActivateStatue()
        {
            _completeStatue.SetActive(true);
            _objectRotationMechanism.enabled = true;
            _pullPlayerArea.gameObject.SetActive(true);
            _statueComplete = true;
        }

        public override void CompleteActivity()
        {
            ActivateStatue();
        }

        public override bool IsCompleted()
        {
            return _statueComplete;
        }
    }
}