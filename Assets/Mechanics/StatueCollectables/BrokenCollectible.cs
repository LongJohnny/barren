﻿using Assets.Common.Scripts;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Mechanics.StatueCollectables
{
    public class BrokenCollectible : Activity
    {
        [SerializeField][Range(0, 4)] private int _collectableNumber;
        [SerializeField] private float _slowDownTimeScale = 0.05f;
        [SerializeField] private float _slowDownDuration = 5f;
        [SerializeField] private Material _collectableMaterial;
        [SerializeField] private OrbitCollectible _targetOrbit;
        [SerializeField] private UnityEvent _onCollect;

        private bool _isCollected = false;
        private float _defaultFixedTime;

        private void Start()
        {
            _defaultFixedTime = Time.fixedDeltaTime;
            //_collectableMaterial.SetColor("_EmissionColor", Color.black);
        }

        public void Collect()
        {
            if (!_isCollected)
            {
                _isCollected = true;
                Time.timeScale *= _slowDownTimeScale;
                // We have to scale fixedDeltaTime as described in this video: https://www.youtube.com/watch?v=F0kezWUqytM&ab_channel=Unity
                // otherwise there is jitter
                Time.fixedDeltaTime = _defaultFixedTime * _slowDownTimeScale;
                Invoke("ReturnTimeToNormal", _slowDownDuration * _slowDownTimeScale);
                StartCoroutine(Shrink());
            }
        }

        private void ReturnTimeToNormal()
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = _defaultFixedTime;
        }

        private IEnumerator Shrink() 
        {
            Vector3 initialScale = transform.localScale;
            Vector3 initialposition = transform.position;
            float k = 0f;
            float materialEmission = 0f;
            float time_passed = 0f;

            //_collectableMaterial.SetColor("_EmissionColor", new Color(100f, 0f, 0f));

            do
            {
                time_passed += Time.deltaTime;
                k = Mathf.Max(0f, time_passed/(_slowDownDuration * _slowDownTimeScale));
                transform.localScale = Vector3.Lerp(initialScale, _targetOrbit.transform.localScale, k);
                transform.position = Vector3.Slerp(initialposition, _targetOrbit.transform.position, k);

                yield return new WaitForEndOfFrame();

            } while (k < 1f);

            PlayerMainController.Instance.CollectCollectable(_collectableNumber);

            gameObject.SetActive(false);

            _onCollect.Invoke();
        }


        public override bool IsCompleted()
        {
            return _isCollected;
        }

        public override void CompleteActivity()
        {
            _isCollected = true;
            enabled = false;
            gameObject.SetActive(false);
        }


    }
}