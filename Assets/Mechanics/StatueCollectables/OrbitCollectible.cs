﻿using System;
using System.Collections;
using Assets.Common.Scripts;
using UnityEngine;

namespace Assets.Mechanics.StatueCollectables
{
    public class OrbitCollectible : MonoBehaviour
    {
        [SerializeField] private float _rotationSpeed = 100f;
        [SerializeField] private Vector3 _rotationAxis = Vector3.up;
        [SerializeField] private Transform _orbitAroundTarget;

        private void Update()
        {
            transform.RotateAround(_orbitAroundTarget.position, _rotationAxis, Time.deltaTime * _rotationSpeed * 5f);
        }

        public void RemoveFromOrbit()
        {
            this.enabled = false;
            this.gameObject.SetActive(false);
            // TODO hide trail gradually
        }

        public bool IsInOrbit() =>  this.isActiveAndEnabled;
        
    }
}