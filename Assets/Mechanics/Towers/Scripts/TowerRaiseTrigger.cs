using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Assets.Global;
using Assets.Mechanics;
using UnityEngine;
using UnityEngine.Events;

public class TowerRaiseTrigger : Activity
{

    private bool _lightsConnected = false;
    private bool _mechanismSnapped = false;

    [SerializeField] private UnityEvent _onLightsAndSnapEvent;

    private bool _triggered = false;

    public void LightsConnected()
    {
        _lightsConnected = true;

        if (_mechanismSnapped && !_triggered)
        {
            _onLightsAndSnapEvent.Invoke();
            _triggered = true;
        }
    }

    public void OnSnapMechanism()
    {
        _mechanismSnapped = true;

        if (_lightsConnected && !_triggered)
        {
            _onLightsAndSnapEvent.Invoke();
            _triggered = true;
        }
    }

    public void OnUnSnapMechanism()
    {
        _mechanismSnapped = false;
    }

    public override bool IsCompleted()
    {
        return _triggered;
    }

    public override void CompleteActivity()
    {
        _triggered = true;
    }

    public override ISerializable[] GetSaveInformation()
    {
        return new ISerializable[] {new SBool(_lightsConnected) };
    }

    public override void LoadFromSaveInformation(ISerializable[] saveInformation)
    {
        _lightsConnected = saveInformation[0] as SBool;
    }
}
