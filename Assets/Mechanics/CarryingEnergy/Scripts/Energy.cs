﻿using System;
using UnityEngine;

namespace Assets.Mechanics.CarryingEnergy.Scripts
{
    [RequireComponent(typeof(ParticleSystem))]
    public class Energy : EnergySource
    {
        [SerializeField] private EnergyConfigurations _energyConfigurations;
        [SerializeField] private Transform _spawnPoint;
        [SerializeField] private AudioSource _energyGrabSound;
        [SerializeField] private AudioSource _energySound;

        private float _currentEnergy = 0.0f;

        private PlayerMovementController m_playerMovementController;
        private ParticleSystem _energyParticles;

        private bool _isHeld = false;
        private bool _consumed = false;
        private Vector3 _consumePosition;

        void Start()
        {
            m_playerMovementController = FindObjectOfType<PlayerMovementController>();
            if (m_playerMovementController == null) 
                throw new MissingReferenceException("There is no player movement controller in the scene!");

            _energyParticles = GetComponent<ParticleSystem>();
            _currentEnergy = _energyConfigurations.maxEnergy;

            _energySound.Play();
        }

        void Update()
        {
            if (_consumed)
            {
                transform.position = Vector3.Lerp(transform.position, _consumePosition, Time.deltaTime * _energyConfigurations.energyPlaceGrabSpeed);

                if ((transform.position - _consumePosition).magnitude < 0.01f)
                {
                    Consume();
                }
            } 
            else if (_isHeld)
            {
                transform.position = Vector3.Lerp(transform.position, m_playerMovementController.transform.position, Time.deltaTime * (m_playerMovementController.Speed * 0.5f + _energyConfigurations.energyPlaceGrabSpeed));

                if (m_playerMovementController.OnGround)
                {
                    _currentEnergy -= _energyConfigurations.energyDecreaseRate * Time.deltaTime;
                    _currentEnergy = Mathf.Max(0f, _currentEnergy);
                }

                var emission = _energyParticles.sizeOverLifetime;

                if (_currentEnergy == 0f)
                {
                    _isHeld = false;

                    if (_spawnPoint != null)
                    {
                        transform.position = _spawnPoint.position;
                        _currentEnergy = _energyConfigurations.maxEnergy;
                        _energySound.volume = 1f;
                    }
                    else
                    {
                        this.Consume();
                    }
                } 

                emission.sizeMultiplier = _currentEnergy/_energyConfigurations.maxEnergy;
                _energySound.volume = _currentEnergy / _energyConfigurations.maxEnergy;
            }
        }


        public bool RanOut() => !_isHeld;

        public void Consume(Vector3 position)
        {
            _consumed = true;
            _consumePosition = position;
            _energyGrabSound.Play();
        }

        private void Consume()
        {
            _energyParticles.Stop();
            _energySound.Stop();
            var size = _energyParticles.sizeOverLifetime;
            size.sizeMultiplier = 0f;
            this.enabled = false;
            GameObject.Destroy(this.gameObject);
        }

        public void ResetToMax()
        {
            _currentEnergy = _energyConfigurations.maxEnergy;
            _energySound.volume = 1f;
        }

        public bool IsFullOfEnergy() => Math.Abs(_currentEnergy - _energyConfigurations.maxEnergy) < float.Epsilon;

        public override Energy GrabEnergy()
        {
            if (this != null && !_consumed)
            {
                _isHeld = true;
                return this;
            }

            return null;
        }

        public bool IsConsumed()
        {
            return _consumed;
        }

        public override bool IsCompleted()
        {
            return _consumed;
        }

        public override void CompleteActivity()
        {
            this.Consume();
        }
    }
}