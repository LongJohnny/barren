﻿using System.Net.Configuration;
using System.Runtime.Serialization;
using Assets.Common.Scripts;
using Assets.Global;
using CameraShake;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Mechanics.CarryingEnergy.Scripts
{
    public class EnergyDestination : EnergySource
    {
        [SerializeField] private UnityEvent _onEnergyPlacement;

        [SerializeField] private AudioSource _energyPlacementSound;
        [SerializeField] private AudioSource _energySound;
        [SerializeField] private Material _energizedMaterial;
        [SerializeField] private Energy _energyPrefab;
        [SerializeField] private ParticlesMoveTowards _particlesMoveTowards;
        [SerializeField] private MeshRenderer _renderer;

        [SerializeField] private bool _hasEnergy = false;
        [SerializeField] private bool _canReceiveEnergy = true;
        [SerializeField] private bool _canSendEnergy = true;

        public bool CanReceiveEnergy
        {
            get => _canReceiveEnergy;
            set => _canReceiveEnergy = value;
        }

        public bool HasEnergy
        {
            get => _hasEnergy;
            set => _hasEnergy = value;
        }

        // Linked list of energy destinations
        [SerializeField] private EnergyDestination _nextDestination;
        [SerializeField] private EnergyDestination _prevDestination;
        public EnergyDestination NextDestination
        {
            get => _nextDestination;
            set => _nextDestination = value;
        }
        public EnergyDestination PrevDestination
        {
            get => _prevDestination;
            set => _prevDestination = value;
        }

        private void Start()
        {
            if (_renderer.material != _energizedMaterial)
            {
                _particlesMoveTowards.enabled = false;
                _particlesMoveTowards.gameObject.SetActive(false);

                if (_hasEnergy)
                {
                    PlaceEnergy(null);
                }
            }

            _energySound.playOnAwake = false;
        }

        public bool PlaceEnergy(Energy energy)
        {
            if (!_canReceiveEnergy) return false;

            if (_hasEnergy & energy != null)
                return false;

            if (energy != null)
            {
                energy.Consume(transform.position);
                _energyPlacementSound.Play();
            }

            _hasEnergy = true;
            _renderer.material = _energizedMaterial;
            _onEnergyPlacement.Invoke();
            _particlesMoveTowards.gameObject.SetActive(true);

            if (_prevDestination != null && _prevDestination._hasEnergy)
            {
                _prevDestination._particlesMoveTowards.enabled = true;
                _prevDestination._particlesMoveTowards.target = transform;
            }

            if (_nextDestination != null && _nextDestination._hasEnergy)
            {
                _particlesMoveTowards.enabled = true;
                _particlesMoveTowards.target = _nextDestination.transform;
            }

            _energySound.loop = true;
            _energySound.Play();

            return true;
        }

        public override Energy GrabEnergy()
        {
            if (!this.enabled || !_canSendEnergy) return null;

            if (_hasEnergy)
            {
                Energy energy = Instantiate(_energyPrefab);
                energy.transform.position = transform.position;
                energy.GrabEnergy();
                return energy;
            }
            return null;
        }


        public override ISerializable[] GetSaveInformation()
        {
            return new ISerializable[]
            {
                new SBool(_hasEnergy),
                new SBool(_canReceiveEnergy),
                new SBool(_canSendEnergy),
            };
        }

        public override void LoadFromSaveInformation(ISerializable[] saveInformation)
        {
            _hasEnergy = saveInformation[0] as SBool;
            _canReceiveEnergy = saveInformation[1] as SBool;
            _canSendEnergy = saveInformation[2] as SBool;
        }



        public override bool IsCompleted()
        {
            return _hasEnergy;
        }

        public override void CompleteActivity()
        {
            //_onEnergyPlacement.Invoke();
            _hasEnergy = true;
            _renderer.material = _energizedMaterial;
            _particlesMoveTowards.gameObject.SetActive(true);
        }

        public override void PostComplete()
        {
            if (_nextDestination != null && _nextDestination._hasEnergy)
            {
                _particlesMoveTowards.enabled = true;
                _particlesMoveTowards.target = _nextDestination.transform;
            }
        }

        

        /**
         * Given a position in between the this and the next destination
         * it returns the next position given the step in the energy path trajectory
         */
        public Vector3 GetNextPositionInTrajectory(Vector3 currPosition, float step, out bool reachedNext)
        {
            if (_nextDestination == null)
            {
                reachedNext = true;
                return currPosition;
            }

            return _particlesMoveTowards.GetNextPositionInTrajectory(currPosition, step, out reachedNext);
        }

        public Vector3 GetPositionInTrajectory(float k)
        {
            return _particlesMoveTowards.GetPositionInTrajectory(k);
        }

        public void AjustTrajectory()
        {
            if (_particlesMoveTowards != null && _particlesMoveTowards.isActiveAndEnabled)
            {
                _particlesMoveTowards.AjustTrajectory();
            }
        }
    }
}