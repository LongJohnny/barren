﻿using System.Collections;
using UnityEngine;

namespace Assets.Mechanics.CarryingEnergy.Scripts
{
    public class EnergyDestinationGroupActivation : MonoBehaviour
    {

        [SerializeField] private EnergyDestination _startDestination;
        [SerializeField] private EnergyDestination _endDestination;

        public void ActivateGroup()
        {
            if (_startDestination == null || _endDestination == null) return;

            EnergyDestination currDestination = _startDestination.NextDestination;
            _startDestination.PlaceEnergy(null);
            do
            {
                currDestination.PlaceEnergy(null);

                currDestination = currDestination.NextDestination;


            } while (currDestination != _endDestination);

            _endDestination.PlaceEnergy(null);

        }
    }
}