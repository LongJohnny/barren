﻿using UnityEngine;

namespace Assets.Mechanics.CarryingEnergy.Scripts
{
    [CreateAssetMenu(menuName = "EnergyConfigurations")]
    public class EnergyConfigurations : ScriptableObject
    {
        public float maxEnergy = 100.0f;
        public float energyDecreaseRate = 10.0f;
        public float energyGrabRadius = 2.0f;
        public float energyPillarGrabRadius = 2.0f;
        public float energyPlacementRadius = 3.0f;
        public float energyGrindRadius = 5.0f;
        public float energyPlaceGrabSpeed = 1f;
    }
}