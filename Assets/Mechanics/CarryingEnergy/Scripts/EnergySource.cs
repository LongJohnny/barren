﻿namespace Assets.Mechanics.CarryingEnergy.Scripts
{
    public abstract class EnergySource : Activity
    {
        public abstract Energy GrabEnergy();
    }
}