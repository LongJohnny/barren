﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Mechanics.CarryingEnergy.Scripts
{
    public class ConnectedEnergyDestinationsTrigger : Activity
    {
        [SerializeField] private EnergyDestination _startDestination;
        [SerializeField] private EnergyDestination _endDestination;

        [SerializeField] private UnityEvent _onEnergyDestinationsConnected;
        private bool _hasRaisedTrigger = false;

        private void Start()
        {
            if (_startDestination == null || _endDestination == null)
            {
                throw new MissingReferenceException(this.name + "cannot work without start and end destinations!");
            }

            EnergyDestination curr = _startDestination.NextDestination;

            while (curr != _endDestination)
            {
                if (curr.NextDestination == null)
                    throw new ArgumentException("Start and End destinations are not connected!");

                curr = curr.NextDestination;

            }
            
        }

        private void Update()
        {
            if (!_hasRaisedTrigger && _startDestination.HasEnergy && _endDestination.HasEnergy)
            {
                EnergyDestination curr = _startDestination.NextDestination;

                while (curr != _endDestination)
                {
                    if (!curr.HasEnergy) return;

                    curr = curr.NextDestination;
                } 

                _onEnergyDestinationsConnected.Invoke();

                _hasRaisedTrigger = true;

                enabled = false;
            }
        }

        public override bool IsCompleted()
        {
            return _hasRaisedTrigger;
        }

        public override void CompleteActivity()
        {
            _hasRaisedTrigger = true;
            this.enabled = false;
        }
    }
}