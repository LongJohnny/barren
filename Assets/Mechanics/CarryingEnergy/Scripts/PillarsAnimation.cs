﻿using System.Collections;
using UnityEngine;

namespace Assets.Mechanics.CarryingEnergy.Scripts
{
    public class PillarsAnimation : MonoBehaviour
    {
        [SerializeField] private float _additionalHeight = 100f;
        private PillarMovement[] _childPillars;
        [SerializeField] private float _raiseDelay = 0.2f;
        private bool _raised = false;
        public bool Raised { get => _raised; }

        private float _maxHeight;

        // Use this for initialization
        void Start()
        {
            _childPillars = GetComponentsInChildren<PillarMovement>();
            _maxHeight = FindMaxHeight();

        }

        public void PlayRaiseAnimation()
        {
            //_maxHeight = FindMaxHeight();

            float delay = 0f;
            for (int i = 0; i < _childPillars.Length; i++)
            {
                _childPillars[i].Raise(_maxHeight, delay);// + _additionalHeight
                delay += _raiseDelay;
            }

            StartCoroutine(waitForAnimation(delay, true));
        }

        public void PlayLowerAnimation()
        {
            float delay = 0f;
            for (int i = _childPillars.Length - 1; i >= 0; i--)
            {
                _childPillars[i].Lower(delay);
                delay += _raiseDelay;
            }
            StartCoroutine(waitForAnimation(delay, false));

        }

        private float FindMaxHeight()
        {
            float maxHeight = -1;
            for (int i = 0; i < _childPillars.Length; i++)
            {
                if (_childPillars[i].transform.position.y > maxHeight)
                {
                    maxHeight = _childPillars[i].transform.position.y;
                }
            }


            if(maxHeight < PlayerMainController.Instance.PlayerPosition.y - 20)
                maxHeight = PlayerMainController.Instance.PlayerPosition.y - 20;
            return maxHeight;// + 60;
        }

        

        IEnumerator waitForAnimation(float delay, bool raising)
        {
            if(!raising)
                _raised = !_raised;
            yield return new WaitForSeconds(delay+0.5f);

            if(raising)
                _raised = !_raised;
            //Debug.Log("Changed after " + delay);
            //Debug.Log("Raised is " + _raised);
            if (!_raised)
                _maxHeight = FindMaxHeight();

        }
    }
}