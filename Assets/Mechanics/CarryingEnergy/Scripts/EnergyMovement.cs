using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Cameras.Scripts;
using Assets.Common.Scripts;
using Assets.Mechanics.CarryingEnergy.Scripts;
using Assets.Player.Scripts;
using UnityEngine;
using UnityEngine.Events;


public class EnergyMovement : MonoBehaviour
{
    [SerializeField] private EnergyConfigurations _energyConfigurations;
    private EnergyDestination[] _energyDestinations;

    [SerializeField] private DashOutPath _dashOutPath;

    [SerializeField] private Camera _grindCamera;

    [SerializeField] private AudioSource _energyTravelSound;

    private Controls _controls;
    private EnergyDestination _currentEnergy;

    private float _kPull = 0f;
    private float _kGrind = 0f;
    private bool _goGrind = false; 
    private bool _loadJump = false; 
    private bool _inGrind = false;
    private bool _grinding = false;
    private bool _grindDirection = true; // false = prevDestination, true = nextDestination

    private Vector3 _initPosition;

    private float _horizontalInput, _verticalInput;


    public UnityEvent onEnterTrigger;
    public UnityEvent onStayTrigger;
    public UnityEvent onLeaveTrigger;

    void Awake()
    {
        _controls = new Controls();
        _controls.Interaction.EnergyGrind.Enable();
        _controls.Movement.Jump.Enable();
        _controls.Movement.MoveX.Enable();
        _controls.Movement.MoveY.Enable();
    }

    void Start()
    {
        _energyDestinations = FindObjectsOfType<EnergyDestination>();
        /*if (_dashOutPath == null)
        {
            throw new MissingReferenceException("Energy movement requires the dash out path to work!");
        }*/
    }
  

    void Update()
    {

        _horizontalInput = _controls.Movement.MoveX.ReadValue<float>();
        _verticalInput = _controls.Movement.MoveY.ReadValue<float>();

        if (!_goGrind && !_inGrind && _controls.Interaction.EnergyGrind.triggered)
        {
            foreach (EnergyDestination energyDestination in _energyDestinations)
            {
                if ((energyDestination.transform.position - transform.position).magnitude <
                    _energyConfigurations.energyGrindRadius && energyDestination.HasEnergy
                   )
                {
                    _currentEnergy = energyDestination;
                    _goGrind = true;
                    _inGrind = false;
                    _loadJump = false;
                    _kPull = 0f;
                    _initPosition = transform.position;

                    _controls.Interaction.EnergyGrind.Disable();
                    PlayerMainController.Instance.ClearPlayerPhysics();
                    PlayerMainController.Instance.DisablePlayer();


                    EnterCam();
                    
                    break;
                }

            }
            
        }

        CheckAndGrind();
    }

    private void CheckAndGrind()
    {
        if (_goGrind)
        {
            if (tryJump())
            {
                PlayerMainController.Instance.EnablePlayer();
                _controls.Interaction.EnergyGrind.Enable();
                PlayerMainController.Instance.Jump();

                _goGrind = false;
                _inGrind = false;


                ExitCam();
            }
            else if (PullPlayer(_currentEnergy.transform.position))
            {
                _goGrind = false;
                _inGrind = true;
               //_dashOutPath.gameObject.SetActive(true);
                _energyTravelSound.pitch = 1f;
            }
        }
        else if (_inGrind)
        {

            if (!_grinding)
            {

                //_dashOutPath.UpdatePath(Vector3.ProjectOnPlane(_grindCamera.transform.forward, Vector3.up));

                if ( _loadJump || tryJump())
                {
                    PlayerMainController.Instance.EnablePlayer();
                    _controls.Interaction.EnergyGrind.Enable();
                    PlayerMainController.Instance.Dash();

                    _goGrind = false;
                    _inGrind = false;
                    _loadJump = false;

                    //this.enabled = false;
                    //StartCoroutine(DisableForTwoFixedUpdates());
                    //_dashOutPath.gameObject.SetActive(false);


                    ExitCam();
                }
                else
                {
                    if (Mathf.Abs(_horizontalInput) >= 0.6f ||Mathf.Abs( _verticalInput) >= 0.6f)  
                    {
                        bool grindDirection = GetGrindDirection(out bool hasDirection);

                        if (hasDirection)
                        {
                            _kGrind = 0f;
                            _grindDirection = grindDirection;
                            _grinding = true;
                            //_dashOutPath.gameObject.SetActive(false);
                        }
                    }
                }
            }
            else
            {
                tryJump();//LOAD JUMP

                if (_grindDirection) // Grind to next
                {
                    if (Grind(_currentEnergy.NextDestination.transform.position))
                    {
                        _kGrind = 0f;
                        _grinding = false;
                        _currentEnergy = _currentEnergy.NextDestination;
                        //_dashOutPath.gameObject.SetActive(true);
                        _energyTravelSound.pitch += 0.05f;
                    }
                }
                else // Grind to prev
                {
                    if (Grind(_currentEnergy.PrevDestination.transform.position)) {
                        _kGrind = 0f;
                        _grinding = false;
                        _currentEnergy = _currentEnergy.PrevDestination;
                        //_dashOutPath.gameObject.SetActive(true);

                        _energyTravelSound.pitch += 0.05f;
                    }
                }

                _energyTravelSound.pitch = Mathf.Min(2.5f, _energyTravelSound.pitch);
            }
        }
    }

    private bool GetGrindDirection(out bool hasDirection)
    {
        Vector3 cameraForward = _grindCamera.transform.forward;
        Vector3 cameraRight = _grindCamera.transform.right;

        Vector3 inputDir = (cameraForward * _verticalInput + cameraRight * _horizontalInput).normalized;

        var nextDestination = _currentEnergy.NextDestination;
        var prevDestination = _currentEnergy.PrevDestination;

        float largestDot = float.MinValue;
        bool moveNext = false;
        
        hasDirection = false;

        if (nextDestination != null && nextDestination.HasEnergy)
        {
            Vector3 dirToNext = (nextDestination.transform.position - _currentEnergy.transform.position).normalized;
            float dot = Vector3.Dot(dirToNext, inputDir);

            if (dot > 0f && dot > largestDot)
            {
                largestDot = dot;
                moveNext = true;
                hasDirection = true;
            }
        } 

        if (prevDestination != null && prevDestination.HasEnergy)
        {
            Vector3 dirToPrev = (prevDestination.transform.position - _currentEnergy.transform.position).normalized;
            float dot = Vector3.Dot(dirToPrev, inputDir);

            if (dot > 0f && dot > largestDot)
            {
                largestDot = dot;
                moveNext = false;
                hasDirection = true;
            } 
        }

        return moveNext;
    }

    private bool Grind(Vector3 destination)
    {
        // TODO Ajust height from energy path

        if (_kGrind == 0f)
        {

            _energyTravelSound.Play();
        }

        _kGrind += Time.deltaTime * 3f;
        _kGrind = Mathf.Min(1f, _kGrind);
        Vector3 position = Vector3.Lerp(_currentEnergy.transform.position, destination,
            _kGrind);
        position.y += 5f;
        transform.position = position;
        
        return _kGrind >= 1f;
    }

    private bool PullPlayer(Vector3 pullDirection)
    {
        _kPull += Time.deltaTime * 3f;
        _kPull = Mathf.Min(_kPull, 1f);
        Vector3 position = Vector3.Lerp(_initPosition, pullDirection, Mathf.SmoothStep(0f, 1f, _kPull));
        position.y += 5f;

        transform.position = position;

        return _kPull >= 1f;
    }

    IEnumerator DisableForTwoFixedUpdates()
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        this.enabled = true;
    }

    private void EnterCam()
    {
        onEnterTrigger.Invoke();
    }

    private void ExitCam()
    {
        onLeaveTrigger.Invoke();
    }

    private bool tryJump()
    {
        if (_controls.Movement.Jump.triggered && !_loadJump)
        {
            if (_grinding)
            {
                _loadJump = true;
            }

            return true;
        }
        return false;
    }
}

