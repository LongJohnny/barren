﻿using System.Collections;
using UnityEngine;

namespace Assets.Mechanics.CarryingEnergy.Scripts
{
    public class PillarMovement : MonoBehaviour
    {
        [SerializeField] public AudioSource _raiseSound;
        [SerializeField] public AudioSource _lowerSound;
        private EnergyDestination _energyDestination;
        private bool _raising;
        private bool _lowering;

        private void Start()
        {
            _energyDestination = GetComponentInChildren<EnergyDestination>();
        }


        public void Raise(float maxHeight, float delay)
        {
            if (_raising) return;
            _raising = true;
            _lowering = false;

            StartCoroutine(RaiseAnimation(maxHeight, delay));
        }

        public void Lower(float delay)
        {
            if (_lowering) return;
            _lowering = true;

            if (_raising) {
                _raising = false;
                StartCoroutine(LowerAnimation(delay));
            }

            _raising = false;

        }

        IEnumerator RaiseAnimation(float maxHeight, float delay)
        {
            yield return new WaitForSeconds(delay);

            Vector3 initialPosition = transform.position;
            Vector3 endPosition = initialPosition;
            endPosition.y = maxHeight;

            float k = 0f;

            while(_raising)
            {
                k += Time.deltaTime * 2f;
                k = Mathf.Min(1f, k);

                transform.position = Vector3.Lerp(initialPosition, endPosition, Mathf.Pow(k, 5f));

                if (k >= 1f) yield break;
                
                //_raiseSound.Play();
                yield return new WaitForEndOfFrame();
            }


        }

        IEnumerator LowerAnimation(float delay)
        {
            yield return new WaitForSeconds(delay);

            Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit);

            Vector3 initialPosition = transform.position;
            Vector3 endPosition = initialPosition;
            // Ádding a random offset to avoid having all pillars be placed at the exact same height
            endPosition.y = hit.point.y + Random.Range(-3f, 3f); 
            float k = 0f;

            while (_lowering)
            {
                k += Time.deltaTime * 2f;
                k = Mathf.Min(1f, k);

                transform.position = Vector3.Lerp(initialPosition, endPosition, Mathf.Pow(k, 5f));

                if (k >= 1f)
                {
                    _energyDestination.AjustTrajectory();
                    _lowering = false;
                    yield break;
                }

                yield return new WaitForEndOfFrame();
            }

            _lowerSound.Play();
        }
    }
}