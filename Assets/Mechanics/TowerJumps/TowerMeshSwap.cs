﻿using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Mechanics.TowerJumps
{
    public class TowerMeshSwap : MonoBehaviour
    {
        [SerializeField] private GameObject _towerCut;
        [SerializeField] private GameObject _tower;

        public void AlignTowerToPlayer(bool cancelled)
        {
            if (!cancelled)
            {
                Vector3 rotation = transform.rotation.eulerAngles;

                Vector3 towerLocalUp = _tower.transform.up;

                Vector3 tp = (PlayerMainController.Instance.PlayerPosition - _tower.transform.position).normalized;
                tp = Vector3.ProjectOnPlane(tp, towerLocalUp);

                float largestDot = float.MinValue;
                float dot;

                if ((dot = Vector3.Dot(tp, _tower.transform.right)) > largestDot)
                {
                    largestDot = dot;
                    _towerCut.transform.rotation = Quaternion.LookRotation(_tower.transform.right, towerLocalUp);
                }

                if ((dot = Vector3.Dot(tp, -_tower.transform.right)) > largestDot)
                {
                    largestDot = dot;
                    _towerCut.transform.rotation = Quaternion.LookRotation(-_tower.transform.right, towerLocalUp);
                }

                if ((dot = Vector3.Dot(tp, _tower.transform.forward)) > largestDot)
                {
                    largestDot = dot;
                    _towerCut.transform.rotation = Quaternion.LookRotation(_tower.transform.forward, towerLocalUp);
                }

                if (Vector3.Dot(tp, -_tower.transform.forward) > largestDot)
                {
                    _towerCut.transform.rotation = Quaternion.LookRotation(-_tower.transform.forward, towerLocalUp);
                }
            }
        }

        public void OpenTower()
        {
            _tower.SetActive(false);
        }

        public void CloseTower()
        {
            _tower.SetActive(true);
        }

    }
}