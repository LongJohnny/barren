﻿using System.Collections;
using UnityEngine;
using Assets.Cameras.Scripts;

namespace Assets.Mechanics.Boosts
{
    public class SpeedBoost : MonoBehaviour
    {

        [SerializeField] private bool _velocityDir;

        [SerializeField] private bool _cameraDir;
            
        private void OnTriggerEnter(Collider other)
        {
            if (_velocityDir)
            {
                PlayerMainController.Instance.ApplyForce(PlayerMainController.Instance.PlayerVelocity.normalized * 5000, ForceMode.Impulse);
            }

            if (_cameraDir)
            {
                PlayerMainController.Instance.ApplyForce(CameraController.Instance.GetCamera().transform.forward * 5000, ForceMode.Impulse);
            }
        }
    }
}   