using UnityEngine;
using UnityEngine.Events;

namespace Assets.Mechanics.Breakables
{
    [RequireComponent(typeof(Rigidbody))]
    public class Breakable : Activity
    {
        [SerializeField] private GameObject _broken;
        [SerializeField][Range(0.1f, 1000.0f)] private float _mass = 100;
        [SerializeField] private UnityEvent _onBreakEvent;

        void Start()
        {
            _broken.SetActive(false);
            GetComponent<Rigidbody>().mass = _mass;
            float numberOfBrokenPieces = _broken.transform.childCount;
            foreach (Rigidbody child in _broken.GetComponentsInChildren<Rigidbody>())
            {
                child.mass = _mass / numberOfBrokenPieces;
            }
        }

        public void BreakObject()
        {
            _broken.transform.rotation = gameObject.transform.rotation;
            _broken.transform.position = gameObject.transform.position;
            _broken.SetActive(true);
            gameObject.SetActive(false);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.collider.CompareTag("Player"))
            {
                _onBreakEvent.Invoke();
                BreakObject();
            }
        }

        public override bool IsCompleted()
        {
            return isActiveAndEnabled;
        }

        public override void CompleteActivity()
        {
            _broken.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
