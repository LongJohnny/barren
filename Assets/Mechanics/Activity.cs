﻿using System;
using System.Runtime.Serialization;
using UnityEngine;

namespace Assets.Mechanics
{
    [System.Serializable]
    public abstract class Activity : MonoBehaviour
    {
        public string GUID
        {
            get { return _GUID; }
            set { _GUID = value;  }
        }

        [SerializeField]
        private string _GUID;

        public virtual bool IsCompleted() { return false; }

        public virtual void CompleteActivity() {}

        public virtual ISerializable[] GetSaveInformation()
        {
            return Array.Empty<ISerializable>(); 
        }

        public virtual void LoadFromSaveInformation(ISerializable[] saveInformation) {}

        public virtual void PostComplete() {}
    }
}