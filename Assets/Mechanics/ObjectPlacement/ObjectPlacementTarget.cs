﻿using Assets.Mechanics.CarryingEnergy;
using Assets.Player.Scripts.Inventory;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Mechanics.ObjectPlacement
{
    public class ObjectPlacementTarget : Activity
    {
        [SerializeField] private GameObject _object;
        [SerializeField] private Transform _targetPlace;
        [SerializeField] private float _placementRadius = 1.5f;
        [SerializeField] private float _timeToPosition = 5.0f;
        [SerializeField] private UnityEvent _onObjectPlaced;
        private bool _hasObject = false;
        private bool _withinRadius = false;

        private Rigidbody _rigidbody;

        private void Start()
        {
            _rigidbody = _object.GetComponent<Rigidbody>();
        }

        void Update()
        {
            if (!_hasObject)
            {
                float _distanceToTarget = (_object.transform.position - _targetPlace.position).magnitude;

                if (!_withinRadius && _distanceToTarget <= _placementRadius)
                {
                    _withinRadius = true;
                    if (_rigidbody != null)
                        _rigidbody.isKinematic = true;
                    _startPosition = _object.transform.position;
                    _startRotation = _object.transform.rotation;
                }

                if (_withinRadius)
                {
                    MoveObjectToTargetPlace();
                }
            }
        }

        private float _currTime = 0.0f;
        private Vector3 _startPosition;
        private Quaternion _startRotation;
        private void MoveObjectToTargetPlace()
        {
            _currTime += Time.deltaTime;
            _currTime = Mathf.Min(_timeToPosition, _currTime);

            float lerpFactor = Mathf.Pow(_currTime / _timeToPosition, 4.0f);
            _object.transform.position = Vector3.Lerp(_startPosition, _targetPlace.position, lerpFactor);
            _object.transform.rotation = Quaternion.Slerp(_startRotation, _targetPlace.rotation, lerpFactor);

            if (!_hasObject && lerpFactor >= 1.0f)
            {
                _hasObject = true;
                _onObjectPlaced.Invoke();
            }
        }

        public override bool IsCompleted()
        {
            return _hasObject;
        }

        public override void CompleteActivity()
        {
            _object.transform.position = _targetPlace.position;
            _object.transform.rotation = _targetPlace.rotation;
        }
    }
}