﻿using UnityEngine;

namespace Assets.Mechanics.Eldrich.Scripts
{
    [RequireComponent(typeof(Collider))]
    public class EldrichCheckpoint : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _eldrichOne;
        [SerializeField] private MeshRenderer _eldrichTwo;
        public bool PassedCheckpoint => _passedCheckpoint;
        private bool _passedCheckpoint = false;

        private Color _color = new Color(0.07058824f, 0.3019608f, 0.7490196f);
        private Color _color2 = new Color(0.7490196f, 0.0f, 0.509804f);
        private float _maxBrightness = 100f;
        private float _minBrightness = 0.5f;
        private float _currBrightness;


        private void Start()
        {
            if (!_passedCheckpoint)
            {
                UpdateBrightness(_minBrightness, _color2);
            }
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _passedCheckpoint = true;
            }
        }

        /**
         * Completes the checkpoint by changing its visual and disabling the checkpoint collider, used
         * when the circuit is complete.
         */
        public void CompleteCheckpoint()
        {
            GetComponent<Collider>().enabled = false;
            UpdateBrightness(_maxBrightness, _color);
        }

        /**
         * Visually updates the checkpoint colors to reflect
         * the time waiting for the player
         */
        public void VisuallyUpdateCheckpoint(float waitNormalizedTime)
        {
            float intensity = Mathf.Lerp(_maxBrightness, _minBrightness, waitNormalizedTime);
            Color color = Color.Lerp(_color, _color2, waitNormalizedTime);
            UpdateBrightness(intensity, color);
        }

        public void ResetCheckpoint()
        {
            _passedCheckpoint = false;
            UpdateBrightness(_minBrightness, _color2);
        }

        private void UpdateBrightness(float brightness, Color color)
        {
            _currBrightness = brightness;

            foreach (Material material in _eldrichOne.materials)
            {
                material.SetColor("_EmissionColor", color * brightness);
            }

            foreach (Material material in _eldrichTwo.materials)
            {
                material.SetColor("_EmissionColor", color * brightness);
            }
        }
    }
}