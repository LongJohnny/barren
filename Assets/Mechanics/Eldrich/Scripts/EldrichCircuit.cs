﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Mechanics.Eldrich.Scripts
{
    public class EldrichCircuit : Activity
    {
        [SerializeField] private float _checkPointDuration;
        [SerializeField] private float _ActivationDuration = 3f;
        [SerializeField] private AudioSource _enterCheckpointSound;
        [SerializeField] private AudioSource _completeCircuitSound;
        [SerializeField] private AudioSource _failedCircuitSound;
        [SerializeField] private EldrichCheckpoint[] _checkpoints;
        [SerializeField] private UnityEvent _onCompleteCircuitEvent;
        [SerializeField] private UnityEvent _onActivateCircuitEvent;

        private LinkedList<EldrichCheckpoint> _checkpointsLinkedList = new LinkedList<EldrichCheckpoint>();
        private LinkedListNode<EldrichCheckpoint> _currNext; // The current checkpoint when starting from the first
        private LinkedListNode<EldrichCheckpoint> _currPrev; // The current checkpoint when starting from the last
        private float _timer = 0f;


        private bool _attemptingCircuit = false;
        private bool _circuitComplete = false;
        private bool _circuitActive = false;

        private void Start()
        {
            if (_checkpoints.Length < 3)
            {
                throw new MissingReferenceException("Eldrich Circuit requires at least 3 checkpoints to work!");
            }

            if (!_circuitComplete)
            {
                foreach (EldrichCheckpoint checkpoint in _checkpoints)
                {
                    _checkpointsLinkedList.AddLast(checkpoint);
                }

                _currNext = _checkpointsLinkedList.First.Next;
                _currPrev = _checkpointsLinkedList.Last.Previous;

                this.enabled = false; // Circuit starts disabled
            }
        }

        private void Update()
        {
            
            if (!_attemptingCircuit )
            {
                if (_checkpointsLinkedList.First.Value.PassedCheckpoint)
                {
                    ResetCircuit(1);
                    _attemptingCircuit = true;
                    _enterCheckpointSound.Play();
                }
                else if (_checkpointsLinkedList.Last.Value.PassedCheckpoint)
                {
                    ResetCircuit(-1);
                    _attemptingCircuit = true;
                    _enterCheckpointSound.Play();
                }
            }
            else
            {
                if (_checkpointsLinkedList.First.Value.PassedCheckpoint) // Starting circuit from the first
                {
                    if (_currNext.Value.PassedCheckpoint)
                    {
                        _timer = 0f;

                        if (_currNext == _checkpointsLinkedList.Last) // Has player completed circuit?
                        {
                            _completeCircuitSound.Play();
                            CompleteCircuit();
                            _onCompleteCircuitEvent.Invoke();
                        }
                        else
                        {
                            _enterCheckpointSound.Play();
                            _currNext = _currNext.Next; // Advance to next checkpoint
                        }
                    }
                    else
                    {
                        _timer += Time.deltaTime;

                        if (_timer >= _checkPointDuration)
                        {
                            _timer = 0f;
                            _failedCircuitSound.Play();
                            ResetCircuit();
                            _attemptingCircuit = false;
                        } 
                        else
                        {
                            _currNext.Value.VisuallyUpdateCheckpoint(_timer / _checkPointDuration);
                        }
                    }
                }
                else if (_checkpointsLinkedList.Last.Value.PassedCheckpoint) // Starting circuit from the last
                {
                    if (_currPrev.Value.PassedCheckpoint)
                    {
                        _timer = 0f;


                        if (_currPrev == _checkpointsLinkedList.First) // Has player completed circuit?
                        {
                            _completeCircuitSound.Play();
                            CompleteCircuit();
                            _onCompleteCircuitEvent.Invoke();
                        }
                        else
                        {
                            _enterCheckpointSound.Play();
                            _currPrev = _currPrev.Previous; // Advance to next checkpoint
                        }
                    }
                    else
                    {
                        _timer += Time.deltaTime;

                        if (_timer >= _checkPointDuration)
                        {
                            _timer = 0f;
                            _failedCircuitSound.Play();
                            ResetCircuit();
                            _attemptingCircuit = false;
                        } 
                        else
                        {
                            _currPrev.Value.VisuallyUpdateCheckpoint(_timer / _checkPointDuration);
                        }
                    }
                }
            }
        }

        public void ActivateCircuit()
        {
            if (!_circuitComplete && !_circuitActive)
            {
                StartCoroutine(ActivationCoroutine());
                this.enabled = true;
                _circuitActive = true;
                _onActivateCircuitEvent.Invoke();
            }
        }

        private IEnumerator ActivationCoroutine()
        {
            float timer = 0;
            float normalizedTimer;

            do
            {
                timer += Time.deltaTime;
                normalizedTimer = Mathf.Min(1f, (timer / _ActivationDuration));

                _checkpointsLinkedList.First.Value.VisuallyUpdateCheckpoint(1f - normalizedTimer);
                _checkpointsLinkedList.Last.Value.VisuallyUpdateCheckpoint(1f - normalizedTimer);

                yield return new WaitForEndOfFrame();

            } while (normalizedTimer < 1f);

            
        }

        private void ResetCircuit(short excludeElement = 0)
        {
            if (excludeElement == 0)
                Debug.Log("Failed ciruit");

            foreach (EldrichCheckpoint checkpoint in _checkpointsLinkedList)
            {
                if (excludeElement != 0) { // Dont reset start or end checkpoints if exclude is not zero
                    if (excludeElement > 0 && checkpoint == _checkpointsLinkedList.First.Value)
                    {
                        continue;
                    }

                    if (excludeElement < 0 && checkpoint == _checkpointsLinkedList.Last.Value)
                    {
                        continue;
                    }

                }
                checkpoint.ResetCheckpoint();
            }

            if (excludeElement == 0)
            {
                _checkpointsLinkedList.First.Value.VisuallyUpdateCheckpoint(0f);
                _checkpointsLinkedList.Last.Value.VisuallyUpdateCheckpoint(0f);
            }

            _currNext = _checkpointsLinkedList.First.Next;
            _currPrev = _checkpointsLinkedList.Last.Previous;
        }

        private void CompleteCircuit()
        {
            Debug.Log("Completed ciruit");

            foreach (EldrichCheckpoint checkpoint in _checkpointsLinkedList)
            {
                checkpoint.CompleteCheckpoint();
            }

            _circuitComplete = true;
            this.enabled = false;
        }

        public override bool IsCompleted() { return _circuitComplete; }

        public override void CompleteActivity()
        {
            CompleteCircuit();
        }
    }
}