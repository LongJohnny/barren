﻿using UnityEngine;

namespace Assets.Mechanics.Timer
{
    public class TimerMoveObject : MonoBehaviour
    {
        [Tooltip("Time to move object to final position in seconds")]
        [SerializeField] private float _timeToMoveObject = 30.0f;
        [SerializeField] private float _currentTime = 0.0f;
        private float _startTime = 0.0f;
        private bool _timerStarted = false;

        [SerializeField] private Transform _object;
        private Vector3 _startPosition;
        [SerializeField] private Transform _finalPosition;

        public bool StartCount = false;

        void Start()
        {
            _startPosition = _object.position;
        }

        void Update()
        {
            if (StartCount)
            {
                StartCount = false;
                StartTimer();
            }

            if (_timerStarted)
            {
                if (ObjectReachedFinalPosition())
                {
                    Debug.Log("Nice!");
                    StopTimer();
                }

                CountTimer();

                if (_currentTime >= _timeToMoveObject)
                {
                    StopTimer();

                    if (!ObjectReachedFinalPosition())
                    {
                        _object.position = _startPosition;
                    }
                }
            }
        }

        private void StartTimer()
        {
            _timerStarted = true;
            _currentTime = 0.0f;
            _startTime = Time.time;
        }

        private void CountTimer()
        {
            _currentTime = Time.time - _startTime;
        }

        private void StopTimer()
        {
            _timerStarted = false;
            _currentTime = 0.0f;
            _startTime = 0.0f;
        }

        private bool ObjectReachedFinalPosition()
        {
            return (_object.position - _finalPosition.position).magnitude <= 1.5f;
        }


    }
}