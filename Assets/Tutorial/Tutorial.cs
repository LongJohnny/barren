using Assets.Cameras.Scripts;
using System.Collections;
using System.Collections.Generic;
using Assets.Global;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;


public class Tutorial : MonoBehaviour
{
    [SerializeField] Transform[] RespawnPositions;
    private Vector3 latestRespawn;
    private bool _inDriftTutorial = false;
    private Vector3 _newLookDirPostion;
    private PlayerMainController _player;
    [SerializeField] private Image _image;



    // Start is called before the first frame update
    void Start()
    {
        if(RespawnPositions == null)
        {
            throw new UnassignedReferenceException("Respawn Positions missing in Tutorial Manager!");
        }
        latestRespawn = RespawnPositions[0].position;
        Vector3 constraints = new Vector3(1, 0, 0);

        //change Player state for tutorial
        _player = PlayerMainController.Instance;
        _player.EnableMovementConstraints(constraints);

        _player.ChangeTerminalVelocity(80f);
    }


    public void Respawn()
    {
        _player.ClearPlayerPhysics();
        _player.PlayerPosition = latestRespawn;

        if (_inDriftTutorial)
        {
            Vector3 lookAtDirection = _newLookDirPostion - latestRespawn;


            CameraController.Instance.ResetCameraOptions(lookAtDirection);
        }

    }

    public void UpdateNewRespawn(Transform respawnPoint)
    {
        latestRespawn = respawnPoint.position;
    }

    public void ChangeToDriftTutorial(Transform newLookDirection)
    {
        _inDriftTutorial = true;
        _newLookDirPostion = newLookDirection.position;

        Debug.Log("Change To Drift Tutorial!");
        _player.ClearMovementConstraints();

        Vector3 lookAtDirection = _newLookDirPostion - latestRespawn;
        CameraController.Instance.ResetCameraOptions(lookAtDirection);
    }

    public void EndTutorial()
    {
        //pauseMenuController.enabled = false;
        StartCoroutine(FadeToWhite());

    }
    IEnumerator FadeToWhite()
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("MainScene", LoadSceneMode.Single);
        asyncOperation.allowSceneActivation = false;

        float k = 0f;
        do
        {
            k += Time.deltaTime * 3f;
            _image.color = new Color(1f, 1f, 1f, k);

            yield return new WaitForEndOfFrame();

        } while (k < 1f);

        GameManager.CreateOrOverwriteSaveFile();
        asyncOperation.allowSceneActivation = true;
        
    }
}
