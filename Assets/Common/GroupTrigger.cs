﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Common
{
    public class GroupTrigger : MonoBehaviour
    {
        [SerializeField] private uint _numberOfActivations = 4;
        private uint _currentActivations= 0;

        [SerializeField] private UnityEvent _onGroupActivationEvent;

        public void ActivateTrigger()
        {
            _currentActivations += 1;

            if (_currentActivations >= _numberOfActivations)
            {
                _onGroupActivationEvent.Invoke();
            }
        }
    }
}