using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UI;

public class PortalLandmark : MonoBehaviour {

    public string destinationSceneName;


    [Tooltip("Disable player when within the disable range")]
    public bool disablePlayerOnTeleport = false;

    [Tooltip("Range at which the player is disabled")]
    public float disableRange = 2000.0f;
    public PlayerMainController player;

    public GameObject loadingScreen;

    public PauseMenuController pauseMenuController;

    public AudioSource audioSource;

    private void Update() {
        if (disablePlayerOnTeleport && player != null) {
            if ((transform.position - player.transform.position).magnitude < disableRange) {
                player.DisablePlayer();
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {

            pauseMenuController.enabled = false;

            if (!disablePlayerOnTeleport) {
                audioSource.Play();
                player.TeleportPlayer(() => {
                    loadingScreen.SetActive(true);
                    // Ensuring the loadingscreen appears immediatly 
                    Invoke("LoadNewLevelAsync",0.1f);
                });
            } else {
                LoadNewLevel();
            }
            //Invoke("LoadNewLevel", 3.0f);
            /*if (GetComponent<AudioSource>()) {
                GetComponent<AudioSource>().Play();
                Invoke("LoadNewLevel", GetComponent<AudioSource>().clip.length);
            } else {
                LoadNewLevel();
            }*/
        }
    }

    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Player") {
            if (GetComponent<AudioSource>()) {
                GetComponent<AudioSource>().Play();
                Invoke("LoadNewLevel", GetComponent<AudioSource>().clip.length);
            } else {
                LoadNewLevel();
            }
        }
    }

    private void LoadNewLevelAsync() {
        SceneManager.LoadSceneAsync(destinationSceneName);
    }

    private void LoadNewLevel() {
        SceneManager.LoadScene(destinationSceneName);
    }





}
