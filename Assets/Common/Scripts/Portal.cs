using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Portal : MonoBehaviour {

    public string destinationSceneName;
    [Range(1,4)]
    public int scriptureId = 1;

    [Tooltip ("Disable player when within the disable range")]
    public bool disablePlayerOnTeleport = false;

    [Tooltip ("Range at which the player is disabled")]
    public float disableRange = 2000.0f;
    public PlayerMainController player;

    private void Update () {
        if (disablePlayerOnTeleport && player != null) {
            //Debug.Log ((transform.position - player.transform.position).magnitude);
            if ((transform.position - player.transform.position).magnitude < disableRange) {
                player.DisablePlayer ();
            }
        }
    }

    private void OnTriggerEnter (Collider other) {
        if (other.tag == "Player") {
            if (player != null) {
                player.DisablePlayer ();
                //player.PlayTeleportAnimation ();
            }
            LoadNewLevel();//LoadNewLevel", 3.0f);
            /*if (GetComponent<AudioSource>()) {
                GetComponent<AudioSource>().Play();
                Invoke("LoadNewLevel", GetComponent<AudioSource>().clip.length);
            } else {
                LoadNewLevel();
            }*/
        }
    }

    private void OnCollisionEnter (Collision other) {
        if (other.gameObject.tag == "Player") {
            if (GetComponent<AudioSource> ()) {
                GetComponent<AudioSource> ().Play ();
                Invoke ("LoadNewLevel", GetComponent<AudioSource> ().clip.length);
            } else {
                LoadNewLevel ();
            }
        }
    }

    private void LoadNewLevel () {
        GameObject scripturesMenu = Resources.Load<GameObject> ("UI/ScripturesMenu");
        Texture newScripture = ScriptureSingleton.Instance.UnlockScripture (scriptureId);
        scripturesMenu = Instantiate (scripturesMenu, Vector3.zero, Quaternion.identity, null);
        Cursor.visible = true;
        Time.timeScale = 0;
        scripturesMenu.transform.Find ("Background/Scripture").GetComponent<RawImage>().texture = newScripture;
        return;
    }



}