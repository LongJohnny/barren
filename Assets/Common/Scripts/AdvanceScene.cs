﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Common.Scripts
{
    public class AdvanceScene : MonoBehaviour
    {
        public int sceneId; 
        public void Advance()
        {
            SceneManager.LoadScene(sceneId);
        }
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}