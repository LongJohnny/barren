﻿using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

namespace Assets.Common.Scripts
{

    /** Adapted from the following asset:
     * https://assetstore.unity.com/packages/vfx/particles/particle-attractor-86896
     */
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticlesMoveTowards : MonoBehaviour
    {

        [SerializeField] private float _speed = 2.5f;
        private ParticleSystem _particleSystem;
        private ParticleSystem.Particle[] _particles;
        private int _particlesAlive;
        [SerializeField] private Transform _target;
        private float _maxTrajectoryHeight = 40f;
        private float _maxTrajectorySlope = 4f;

        private float _trajectorySlope = 2f;
        private float _trajectoryHeight = 0f;

        private float _trajectoryAdjustment = 0f;
        private float _trajectoryMaxAmplitude = 9f;

        public Transform target
        {
            get => _target;
            set => _target = value;
        }

        private void Start()
        {
            _particleSystem = GetComponent<ParticleSystem>();
            AjustTrajectory();
        }

        public void AjustTrajectory()
        {
            Vector3 rayDir = _target.position - transform.position;
            if (Physics.Raycast(transform.position, rayDir, out RaycastHit hit, rayDir.magnitude))
            {
                if (hit.transform != _target)
                {
                    // Ajust trajectory using a combination of the alignment of the hit normal with the up direction and the
                    // distance to the hit surface
                    float slopeTerrain = Mathf.Max(0f, Vector3.Dot(hit.normal.normalized, Vector3.up));
                    float distance = 1f - hit.distance/rayDir.magnitude;

                    /*_trajectoryHeight = slopeTerrain * _maxTrajectoryHeight;
                    _trajectorySlope =  distance * _maxTrajectorySlope;*/


                    _trajectoryAdjustment = (.5f * slopeTerrain + .5f * distance) * _trajectoryMaxAmplitude;
                }
            }
        }

        private float TrajectoryChange(float k)
        {
            return (1f - Mathf.Pow(2f * k - 1f, 2f)) * Mathf.Pow((1f - k), 2f);
        }

        private float TrajectoryChange(float x, float height, float slope)
        {
            // View graph here: https://www.desmos.com/calculator/ihbmocmwrm

            float h = Mathf.Clamp(height, 0.01f, _maxTrajectoryHeight);
            float s = Mathf.Clamp(slope, 2f, _maxTrajectorySlope);

            return (1f - Mathf.Pow(Mathf.Abs(2f * x - 1f), s))*h;
        }

        /**
         * Given a position in between the destinations
         * it returns the next position given the step in the energy path trajectory
         */
        public Vector3 GetNextPositionInTrajectory(Vector3 currPosition, float step, out bool reachedNext)
        {
            Vector3 position = Vector3.Lerp(currPosition, _target.position, step);
            float x = MathAux.InverseLerp(transform.position, target.position, currPosition);
            position.y += TrajectoryChange(x) * _trajectoryAdjustment;

            reachedNext = x >= 0.99f;

            return position;
        }

        public Vector3 GetPositionInTrajectory(float k)
        {
            Vector3 position = Vector3.Lerp(transform.position, _target.position, k);
            position.y += TrajectoryChange(k) * _trajectoryAdjustment * 5f;

            return position;
        }

        private void Update()
        {
            Debug.DrawRay(transform.position, _target.position - transform.position, Color.blue);

            _particles = new ParticleSystem.Particle[_particleSystem.main.maxParticles];
            _particlesAlive = _particleSystem.GetParticles(_particles);

            float step = _speed * Time.deltaTime;
            for (int i = 0; i < _particlesAlive; i++)
            {
                _particles[i].position = GetNextPositionInTrajectory(_particles[i].position, step, out _);
            }
            
            _particleSystem.SetParticles(_particles, _particlesAlive);
        }
	}
}