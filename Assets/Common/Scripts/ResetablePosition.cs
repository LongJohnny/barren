using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetablePosition : MonoBehaviour {
    private Vector3 startPosition;

    private void Start() {
        startPosition = transform.position;
    }

    public void ResetPosition() {
        transform.position = startPosition;
        if (GetComponent<Rigidbody>()) GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
