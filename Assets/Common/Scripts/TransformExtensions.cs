﻿using System.Collections;
using UnityEngine;

namespace Assets.Common.Scripts
{
    public static class TransformExtensions
    {
        public static void SmoothLookAt(this Transform transform, Transform target, Vector3 worldUp, float t)
        {
            Quaternion lookRotation = Quaternion.LookRotation(target.position - transform.position, worldUp);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, t);
        }

        public static void SmoothLookAt(this Transform transform, Vector3 worldPosition, Vector3 worldUp, float t)
        {
            Quaternion lookRotation = Quaternion.LookRotation(worldPosition - transform.position, worldUp);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, t);
        }
    }
}