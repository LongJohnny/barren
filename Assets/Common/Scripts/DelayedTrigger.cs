﻿using System.Collections;
using Assets.Mechanics;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Common.Scripts
{
    [RequireComponent(typeof(Collider))]
    public class DelayedTrigger : Activity
    {
        [SerializeField] private float _timeToTrigger = 0.1f;
        [SerializeField] private bool _triggerOnce = false;

        public UnityEvent onEnterTrigger;
        public UnityEvent onStayTrigger;
        public UnityEvent onLeaveTrigger;

        private Coroutine _enteredTriggerCoroutine;
        private bool _enteredTrigger = false;

        private bool _hasBeenTriggered = false;

        private void Start()
        {
            if (!GetComponent<Collider>().isTrigger)
            {
                throw new MissingReferenceException("Collider is not a trigger!");
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (this.enabled && other.CompareTag("Player") && !_hasBeenTriggered)
            { 
                _enteredTriggerCoroutine = StartCoroutine(RaiseTrigger(_timeToTrigger));
            }
                
        }

        private void OnTriggerStay(Collider other)
        {
            if (this.enabled && other.CompareTag("Player") && _enteredTrigger)
            {
                onStayTrigger.Invoke();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (this.enabled && other.CompareTag("Player"))
            {
                if (_enteredTrigger)
                {
                    onLeaveTrigger.Invoke();
                }
                else
                {
                    StopCoroutine(_enteredTriggerCoroutine);
                    _enteredTriggerCoroutine = null;
                }

                _enteredTrigger = false;

                if (_hasBeenTriggered)
                {
                    this.enabled = false;
                }
            }
        }


        IEnumerator RaiseTrigger(float delay)
        {
            yield return new WaitForSeconds(delay);

            _enteredTrigger = true;

            _enteredTriggerCoroutine = null;

            if (_triggerOnce)
            {
                _hasBeenTriggered = true;
            }

            onEnterTrigger.Invoke();
        }

        public override bool IsCompleted()
        {
            return _hasBeenTriggered;
        }

        public override void CompleteActivity()
        {
            if (_triggerOnce)
            {
                _hasBeenTriggered = true;
                this.enabled = false;
            }
        }
    }
}