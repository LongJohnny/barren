﻿using System.Collections;
using UnityEngine;

namespace Assets.Common.Scripts
{
    public class SlowMotion : MonoBehaviour
    {
        private float _defaultFixedTime;

        private void Start()
        {
            _defaultFixedTime = Time.fixedDeltaTime;
        }

        public void StartSlowMotion(float slowmoScale)
        {
            Time.timeScale *= slowmoScale;
            // We have to scale fixedDeltaTime as described in this video: https://www.youtube.com/watch?v=F0kezWUqytM&ab_channel=Unity
            // otherwise there is jitter
            Time.fixedDeltaTime = _defaultFixedTime * slowmoScale;
        }

        public void StopSlowMotion()
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = _defaultFixedTime;
        }


    }
}