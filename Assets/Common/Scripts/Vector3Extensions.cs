﻿using System.Collections;
using UnityEngine;

namespace Assets.Common
{
    public static class Vector3Extensions 
    {
        public static Vector3 Pow(this Vector3 v, float p)
        {
            return new Vector3(Mathf.Pow(v.x, p), Mathf.Pow(v.y, p), Mathf.Pow(v.z, p));
        }
    }
}