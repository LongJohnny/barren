﻿using System.Collections;
using UnityEngine;

namespace Assets.Common.Scripts
{
    public class FollowPosition : MonoBehaviour
    {

        [SerializeField] private Transform _transform;
        void Update()
        {
            transform.position = _transform.position;
        }
    }
}