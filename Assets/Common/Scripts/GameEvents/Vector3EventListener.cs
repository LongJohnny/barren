﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityVector3Event : UnityEvent<Vector3> { }


public class Vector3EventListener : MonoBehaviour
{
    public Vector3Event Event;
    public UnityVector3Event Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Vector3 arg)
    {
        Response.Invoke(arg);
    }
}
