﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class Vector3Event : ScriptableObject
{
	private readonly List<Vector3EventListener> _listeners = new List<Vector3EventListener>();

	public void Raise(Vector3 arg)
	{
		for (int i = _listeners.Count - 1; i >= 0; i--)
			_listeners[i].OnEventRaised(arg);
	}

	public void RegisterListener(Vector3EventListener listener)
	{ _listeners.Add(listener); }

	public void UnregisterListener(Vector3EventListener listener)
	{ _listeners.Remove(listener); }
    
    
}
