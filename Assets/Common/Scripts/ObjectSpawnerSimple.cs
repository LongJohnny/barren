﻿using Assets.Mechanics.CarryingEnergy.Scripts;
using Assets.Overworld.Scripts;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

using UnityEngine;

namespace Assets.Common.Scripts
{
    public class ObjectSpawnerSimple : MonoBehaviour
    {
        public Transform parent;
        public GameObject prefabToSpawn;
        public Transform startPosition;
        public Transform endPosition;

        public float spacing;
        public float maxSideOffset;
        public float maxUpOffset;
        //public TerrainGenerator terrainGenerator;

        public void SpawnObjects()
        {
            if (parent == null || prefabToSpawn == null || startPosition == null ||
                endPosition == null || spacing == 0)
            {
                throw new MissingReferenceException("Cannot spawn objects there are parameters missing!");
            }

            Vector3 direction = endPosition.position - startPosition.position;
            float numberOfObjects = direction.magnitude / spacing;

            //TerrainData[] terrains = terrainGenerator.GetTerrains();
            //int size = (int)Mathf.Sqrt(terrains.Length);

            Vector3 offsetDirection = Vector3.Cross(direction, Vector3.up).normalized;

            EnergyDestination prevEnergyDestination = null;

            for (int i = 0; i < numberOfObjects; i++)
            {
                Vector3 spawnPosition = startPosition.position + direction.normalized * i * spacing;
                spawnPosition += offsetDirection * Random.Range(0f, maxSideOffset);

                //bool foundTerrain = false;

                //for (int yt = 1; yt <= size; yt++)
                //{
                //    for (int xt = 1; xt <= size; xt++)
                //    {
                //        float x = xt * 10000f;
                //        float z = yt * 10000f;

                //if (spawnPosition.x <= x && spawnPosition.z <= z)
                //{
                //foundTerrain = true;

                //int terrainIdx = (xt - 1) + (yt - 1) * size;

                //float sx = (spawnPosition.x - (xt - 1f) * 10000f) / 10000f;
                //float sy = (spawnPosition.z - (yt - 1f) * 10000f) / 10000f;

                //spawnPosition.y = terrains[terrainIdx].GetInterpolatedHeight(sx, sy);
                //spawnPosition += Vector3.up * Random.Range(-maxUpOffset, 0f);

                //Vector3 normal = terrains[terrainIdx].GetInterpolatedNormal(sx, sy);
                //Vector3 axis = Vector3.Cross(Vector3.up, normal);
                //float angle = Mathf.Asin(axis.magnitude);

                GameObject spawned = Instantiate(prefabToSpawn, spawnPosition, Quaternion.identity, parent);
                //spawned.transform.rotation *= Quaternion.AngleAxis(Mathf.Rad2Deg * angle * Random.Range(0.2f, 0.7f), axis);

                if (prefabToSpawn.GetComponentInChildren<EnergyDestination>() != null)
                {
                    if (prevEnergyDestination == null)
                    {
                        prevEnergyDestination = spawned.GetComponentInChildren<EnergyDestination>();
                    }
                    else
                    {
                        EnergyDestination currEnergyDestination = spawned.GetComponentInChildren<EnergyDestination>();

                        currEnergyDestination.PrevDestination = prevEnergyDestination;

                        prevEnergyDestination.NextDestination = currEnergyDestination;

                        prevEnergyDestination = currEnergyDestination;
                    }
                }
                //break;
                //}
                //}
                //if (foundTerrain)
                //{
                //    break;
                //}
                //}
            }

        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ObjectSpawnerSimple))]
    public class ObjectSpawnerSimpleEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            ObjectSpawnerSimple objectSpawnerSimple = target as ObjectSpawnerSimple;
            if (objectSpawnerSimple != null && GUILayout.Button("Spawn objects"))
            {
                objectSpawnerSimple.SpawnObjects();

                if (GUI.changed)
                {
                    EditorUtility.SetDirty(objectSpawnerSimple);
                    EditorSceneManager.MarkSceneDirty(objectSpawnerSimple.gameObject.scene);
                }
            }

        }
    }
#endif
}