﻿using System;
using System.Collections;
using Assets.Mechanics;
using CameraShake;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.AI;

namespace Assets.Common.Scripts
{
    public class RaiseObject : Activity
    {
        [SerializeField] private float _raiseDuration = 1f;
        [SerializeField] private float _baseHeight = 0f;
        [SerializeField] private float _raisedHeight = 1f;
        [SerializeField] private bool _startLowered = true;

        [SerializeField] private bool _shakeOnRaise = true;
        [SerializeField] private BounceShake.Params _shakeParams;

        [SerializeField] private UnityEvent _onStartRaise;
        [SerializeField] private UnityEvent _onEndRaise;

        private bool _isRaised = false;

        
        private void Start()
        {
            if (_raiseDuration <= 0f)
            {
                throw new ArgumentException("Raise duration has to be above 0!");
            }

            if (_raisedHeight < _baseHeight)
            {
                throw new ArgumentException("Raise height has to be larger than base height!");
            }

            if (!_isRaised)
            {
                if (!_startLowered)
                {
                    _isRaised = true;
                }
                else
                {
                    _isRaised = false;
                    Vector3 finalPosition = transform.position;
                    finalPosition.y = _baseHeight;
                    transform.position = finalPosition;
                }
            }
        }

        public void PlayRaiseAnimation()
        {
            if (!_isRaised)
            {
                _isRaised = true;
                _onStartRaise.Invoke();
                StartCoroutine(RaiseCoroutine());
            }
        }

        public void RaiseInstantly()
        {
            if (!_isRaised)
            {
                _isRaised = true;
                Vector3 finalPosition = transform.position;
                finalPosition.y = _raisedHeight;
                transform.position = finalPosition;
            }
        }

        private IEnumerator RaiseCoroutine()
        {
            float timer = 0f;

            Vector3 initialPosition = transform.position;
            Vector3 finalPosition = transform.position;
            finalPosition.y = _raisedHeight;

            do
            {
                timer += Time.deltaTime;
                timer = Mathf.Min(_raiseDuration, timer);

                float normalizedTimer = timer / _raiseDuration;

                transform.position = Vector3.Lerp(initialPosition, finalPosition,
                    MathAux.SmoothStep(0.0f, 1f, normalizedTimer));

                if (_shakeOnRaise)
                {
                    BounceShake.Params shakeParams = _shakeParams;
                    shakeParams.rotationStrength *= (1f - normalizedTimer);
                    shakeParams.attenuation = null;
                    //CameraShaker.Instance.RegisterShake(new BounceShake(shakeParams));
                    CameraShaker.Instance.ShakePresets.ShortShake3D(0.1f * (1f - normalizedTimer), 40f, 1);
                }

                /*if (_shakeOnRaise && timer >= _raiseDuration)
                    CameraShaker.Instance.ShakePresets.ShortShake3D(0.3f, 30f, 10);*/

                yield return new WaitForEndOfFrame();
            } while (timer < _raiseDuration);

            _onEndRaise.Invoke();
        }

        public override bool IsCompleted()
        {
            return _isRaised;
        }

        public override void CompleteActivity()
        {
            RaiseInstantly();
        }
    }
}