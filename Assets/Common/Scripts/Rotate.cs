﻿using System.Collections;
using UnityEngine;

namespace Assets.Mechanics.StatueCollectables
{
    public class Rotate : MonoBehaviour
    {
        [SerializeField] private Space _space = Space.World;
        [SerializeField] private Vector3 _axis = Vector3.up;
        [SerializeField] private float _speed = 50f;

        void Update()
        {
            transform.Rotate(_axis, Time.deltaTime * _speed, _space);
        }
    }
}