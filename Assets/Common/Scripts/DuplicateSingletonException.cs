﻿using System;
using System.Runtime.Serialization;

namespace Assets.Common.Scripts
{
    public class DuplicateSingletonException : Exception
    {
        public DuplicateSingletonException()
        {
        }

        public DuplicateSingletonException(string message) : base(message)
        {
        }

        public DuplicateSingletonException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DuplicateSingletonException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}