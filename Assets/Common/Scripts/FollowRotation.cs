﻿using System.Collections;
using UnityEngine;

namespace Assets.Common.Scripts
{
    public class FollowRotation : MonoBehaviour
    {

        [SerializeField] private Transform _followTarget;
        
        void Update()
        {
            transform.rotation = _followTarget.rotation;
        }
    }
}