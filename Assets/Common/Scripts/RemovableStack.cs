﻿using System.Collections;
using System.Collections.Generic;

namespace Assets.Common.Scripts
{
    public class RemovableStack<T>
    {
        private List<T> _items = new List<T>();

        public void Push(T item)
        {
            _items.Add(item);
        }

        public T Pop()
        {
            if (_items.Count > 0)
            {
                T temp = _items[_items.Count - 1];
                _items.RemoveAt(_items.Count - 1);
                return temp;
            }
            else
                return default(T);
        }

        public T Peek() => _items.Count > 0 ? _items[_items.Count - 1] : default(T);

        public void Remove(T item)
        {
            _items.Remove(item);
        }

        public bool Contains(T item) => _items.Contains(item);

        public int Count => _items.Count;
    }
}