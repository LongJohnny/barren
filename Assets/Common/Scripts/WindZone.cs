using System.Runtime.Serialization;
using Assets.Global;
using Assets.Mechanics;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class WindZone : Activity
{
   [SerializeField] private float _strength = 5000f;
   [SerializeField] private Vector3 _direction = Vector3.up;
   [SerializeField] private bool _useLocalForwardAsDir = false;

   private bool _pushOthers = false;

   private void OnTriggerStay(Collider other)
   {
        if (_useLocalForwardAsDir)
        {
            _direction = transform.forward;
        }

        if (other.tag == "Player")
       {
           PlayerMainController.Instance.ApplyForce(_direction * _strength, ForceMode.Force);
       }
       else if (_pushOthers)
       {
           other.attachedRigidbody.AddForce(_direction * _strength * 0.005f, ForceMode.Force);
       }
   }

   public void PushOthers()
   {
       _pushOthers = true;
   }

   public override bool IsCompleted()
   {
       return isActiveAndEnabled;
   }

   public override void CompleteActivity()
   {
       gameObject.SetActive(true);
   }

   public override ISerializable[] GetSaveInformation()
   {
       return new ISerializable[] { new SBool(_pushOthers) };
   }

   public override void LoadFromSaveInformation(ISerializable[] saveInformation)
   {
       _pushOthers = saveInformation[0] as SBool;
   }
}
