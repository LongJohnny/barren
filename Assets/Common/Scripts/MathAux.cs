﻿using UnityEditor;
using UnityEngine;
using static UnityEngine.Mathf;
using static UnityEngine.Vector2;

namespace Assets.Common.Scripts
{
    public static class MathAux 
    {
        public static float Fract(float x) => x - Floor(x);
        public static Vector2 Floor2(Vector2 p) => new Vector2(Floor(p.x), Floor(p.y));
        public static Vector2 Fract2(Vector2 p) => new Vector2(Fract(p.x), Fract(p.y));

        public static Vector2 Cos2(Vector2 p) => new Vector2(Cos(p.x), Cos(p.y));
        public static Vector2 Sin2(Vector2 p) => new Vector2(Sin(p.x), Sin(p.y));
        
        public static float Bilerp(float a, float b, float c, float d, float k)
        {
            return Lerp(Lerp(a, b, k), Lerp(c, d, k), k);
        }

        public static Vector3 RandomVector3()
        {
            return new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
        }

        public static Vector3 QuadraticBezier(Vector3 a, Vector3 b, Vector3 c, float k)
        {
            float t1 = 1f - k;
            float t2 = Pow(t1, 2f);
            float t3 = Pow(k, 2f);

            return t2 * a + 2*t1 * b + t3 * c;
        }

        public static float Noise(Vector2 p) => PerlinNoise(p.x, p.y);
        public static float Ridge(Vector2 p) => 1f - Abs(PerlinNoise(p.x, p.y));

        public static float Smin(float a, float b, float k)
        {
            float x = Exp(-k * a);
            float y = Exp(-k * b);
            return (a * x + b * y) / (x + y);
        }

        public static float Smax2(float a, float b, float k)
        {
            return Smin(a, b, -k);
        }

        public static float Smax(float a, float b, float k)
        {
            float h = Clamp(0.5f + 0.5f * (b - a) / k, 0.0f, 1.0f);
            return Lerp(b, a, h) - k * h * (1.0f - h);
        }

        public static float Rand(Vector2 p)
        {
            return Fract(Sin(Dot(p, new Vector2(12.9898f, 78.233f)) * 43758.5453123f));
        }

        public static float Rand(float x)
        {
            return Fract(Sin(x) * 43758.5453123f);
        }

        public static Vector2 Rand2(Vector2 p)
        {
            Vector2 a = new Vector2(Dot(p, new Vector2(127.1f, 311.7f)), Dot(p, new Vector2(269.5f, 183.3f)));
            return new Vector2(Rand(a.x), Rand(a.y));
        }

        public static float SmoothStep(float edgeA, float edgeB, float x)
        {
            float p = Clamp((x - edgeA) / (edgeB - edgeA), 0.0f, 1.0f);
            float v = p * p * (3.0f - 2.0f * p);
            return v;
        }
        
        public static float Voronoi(Vector2 p)
        {
            Vector2 i = Floor2(p);
            Vector2 f = Fract2(p);

            Vector2 nb, rp, b;
            float min = 0f;
            float sm = 0f;
            float dist = 1f;
            for (int _i = -1; _i <= 1; ++_i)
            {
                for (int j = -1; j <= 1; ++j)
                {
                    nb.x = _i;
                    nb.y = j;

                    rp = Rand2(Floor2(i + nb));

                    dist = Min(dist, (f - (nb + rp)).magnitude);

                    //min = (f - (nb + rp)).magnitude; 

                    /*if (min < 0.1f)
                    {
                        dist = Min(dist, 0.1f);
                    } 
                    else if (min < 0.2f)
                    {
                        float k = (min - 0.1f) / 0.1f;

                        dist = Min(dist, Lerp(0.1f, min, k));
                    } 
                    else
                    {
                        dist = Min(dist, min);
                    }*/

                }
            }

            return dist;
        }

        public static float DomainWarping(Vector2 p) { 

            Vector2 a = new Vector2(
                Noise(p), 
                Noise(p + new Vector2(2f, 2f)));

            Vector2 b = new Vector2(
                Noise(Cos2(a * .2f) + new Vector2(1.7f, 9.2f)), 
                Noise(a * 2f  + new Vector2(8.3f, 2.8f)) * 2f);

            // .5f to ensure this is between 0 and 1
            return Noise(p + b * 4f);
        }

        public static float DomainWarping2(Vector2 p)
        {
            float val = 0f;
            for (int i = 0; i < 4; ++i)
            {
                val = Noise(p + new Vector2(val, val));
            }
            return val;
        }

        public static float WavesCircle(Vector2 p, float f)
        {

            float d = (p - new Vector2(.5f, .5f)).magnitude;

            return 1f-Abs(-Cos(d * f));
        }

        public static float InverseLerp(Vector3 a, Vector3 b, Vector3 v)
        {
            Vector3 x = b - a;
            Vector3 y = v - a;

            return Clamp(Vector3.Dot(y, y) / Vector3.Dot(x, y),0f, 1f);
        }

    }
}