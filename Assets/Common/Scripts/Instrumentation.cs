using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Instrumentation : MonoBehaviour {

    public static Instrumentation Instance;

    public Dictionary<string, string> metrics;

    private GameObject playerObj;

    private void Awake () {
        if (Instance == null) {
            Instance = this;
        } else DestroyImmediate (this);
    }


    private void Start () {
        metrics = new Dictionary<string, string> ();
        foreach (PlayerMovementController mc in FindObjectsOfType<PlayerMovementController> ()) {
            if (mc.gameObject.name == "Player") {
                playerObj = mc.gameObject;
            }
        }

        if (playerObj == null) Debug.LogWarning ("No player found for metrics.");
    }



    private void Update () {
        float velocity = playerObj.GetComponent<Rigidbody> ().velocity.magnitude;
        UpdateMaxFloat ("MaxVelocity", velocity);

        float altitude = playerObj.transform.position.y;
        UpdateMaxFloat ("MaxAltitude", altitude);

        //Not the best, because the time always increases, but it's the most reliable
        UpdateMaxFloat ("Time", Time.time);

    }


    private void OnDestroy () {
        WriteMetrics ();
    }

    private void UpdateMaxFloat (string key, float value) {
        if (!metrics.ContainsKey (key)) {
            metrics.Add (key, value.ToString ());
        } else {
            if (value > float.Parse (metrics[key])) {
                metrics[key] = value.ToString ();
            }
        }
    }

    private void WriteMetrics () {
        int fileId = (int) Random.Range (0, 1000000000);
        string path = Application.persistentDataPath + "/Metrics";
        if (!Directory.Exists (path)) Directory.CreateDirectory (path);
        StreamWriter writer = new StreamWriter (path + "/" + fileId.ToString () + ".metr", true);
        Debug.Log ("Created metrics file at " + Application.persistentDataPath);
        writer.WriteLine (SceneManager.GetActiveScene ().name);
        foreach (string s in metrics.Keys) {
            writer.WriteLine (s + ":" + metrics[s]);
        }
        writer.Close ();
    }
}