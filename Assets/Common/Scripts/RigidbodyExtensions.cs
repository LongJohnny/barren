using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RigidbodyExtensions {
    
    /*
    Used to apply a force to reach a certain velocity
    Works great on the x and z axis, but does not consider gravity only drag
    */
    public static void AddForceToReachVelocity(this Rigidbody rb, Vector3 desiredVelocity, float timeToVelocity = 0) {

        // Current velocity on x and z axis
        Vector3 currentVelocity = rb.velocity;
        currentVelocity.y = 0.0f;

        // Here we slightly increase the acceleration to consider the drag
        // the added value is multiplied by 0.2f because when drag is 1, and velocity
        // is 10 then the speed only reaches 9.8, so we add acceleration so that it
        // reaches the desired speed
        desiredVelocity = desiredVelocity + desiredVelocity.normalized * 0.2f * rb.drag;

        // If the desired velocity is within a 90 degree radius of the current velocity
        // and the magnitude of the current velocity is larger than the desired velocity
        // then we maintain the magnitude so that we don't lose speed
        if(Vector3.Dot(desiredVelocity , currentVelocity) > 0){
            // Instead of changing the direction drastically we calculate a halfway vector
            // that is used instead of the desired velocity
            desiredVelocity = ((desiredVelocity + currentVelocity)/ 2.0f);

            if (currentVelocity.sqrMagnitude > desiredVelocity.sqrMagnitude) 
                desiredVelocity = desiredVelocity.normalized * currentVelocity.magnitude;
        }

        // Average linear acceleration formula
        Vector3 deltaVelocity = desiredVelocity - currentVelocity;

        // When timeToVelocity is 0 then delta time will be the time since the last physics update
        // which should give a large enough acceleration that will cause the rigidbody to reach
        // its desired velocity instantaneously
        float deltaTime = Mathf.Clamp(Time.fixedDeltaTime + timeToVelocity, 0.0f, float.MaxValue);
        Vector3 acceleration = deltaVelocity / deltaTime;
        
        // Newton's second law of motion
        Vector3 force = rb.mass * acceleration;

        // Finally we add a continuous force that considers the mass of the object
        rb.AddForce(force, ForceMode.Force); 
    }
}