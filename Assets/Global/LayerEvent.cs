﻿using System;
using Assets.Mechanics;
using UnityEngine;

namespace Assets.Global
{
    public class LayerEvent : Activity
    {
        
        [SerializeField] private Transform _landmarkTarget;
        private LayerItem _layerItem;
        private bool _isComplete = false;

        private void Start()
        {
            if (_landmarkTarget == null)
            {
                _landmarkTarget = transform;
            }
        }


        public void Activate()
        {
            if (!_isComplete)
                LayerManager.Instance.ActivateLayer(_layerItem);
        }

        public void Complete()
        {
            if (!_isComplete)
            {
                Debug.Log("Layer item has been completed!");
                _isComplete = true;
                LayerManager.Instance.CompleteLayer(_layerItem);
            }
        }

        public bool IsComplete => _isComplete;

        public Transform LandmarkTarget => _landmarkTarget;

        public void AttributeLayerItem(LayerItem layerItem)
        {
            _layerItem = layerItem;
        }

        public override void CompleteActivity()
        {
            _isComplete = true;
        }

        public override bool IsCompleted()
        {
            return _isComplete;
        }
    }
}