﻿using System;
using UnityEngine;

namespace Assets.Global
{
    [Serializable]
    public class LayerItem
    {
        public const uint MAX_LAYERS = 10;

        [Range(0, MAX_LAYERS)] public uint layerNumber;
        public LayerEvent layerEvent;
        
        [NonSerialized] public LayerItem next = null;
        public bool HasNext => next != null;

        public void Init()
        {
            layerEvent.AttributeLayerItem(this);
        }
    }

    [Serializable]
    public class LayerItemList
    {
        public String name;
        public bool startActive;
        public LayerItem[] layerItems;

        public void Init()
        {
            foreach (var layer in layerItems)
            {
                layer.Init();
            }

            LayerItem curr, prev;
            for (int i = 1; i < layerItems.Length; ++i)
            {
                curr = layerItems[i];
                prev = layerItems[i - 1];

                if (curr.layerNumber < prev.layerNumber) throw new ArgumentException("cannot have a LayerID lower than the previous LayerID in the group!");

                if ((curr.layerNumber - prev.layerNumber) > 1) throw new ArgumentException("cannot have a LayerID more than one value larger than the previous LayerID in the group!");

                prev.next = curr;

            }
        }

        public LayerItem First => layerItems[0];
    }


}