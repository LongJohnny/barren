﻿using Assets.Mechanics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.AzureSky;

namespace Assets.Global
{
    public class GameManager: MonoBehaviour
    {
        private const string SaveFileName = "barrenSaveFile";

        private static bool _loadGame = false;

        [SerializeField] private bool _saveGame = true;
        [SerializeField] private float _saveIntervalMinutes = 3f;
        [SerializeField] private Transform _playerStart;
        [SerializeField] private Transform _player;

        public static bool IsThereSaveFile()
        {
            return File.Exists(GetSaveFilePath());
        }

        public static void CreateOrOverwriteSaveFile()
        {
            string saveFilePath = GetSaveFilePath();
            if (File.Exists(saveFilePath))
            {
                File.Delete(saveFilePath);
                Debug.Log("Successfully deleted save file!");
            }

            File.Create(saveFilePath);
        }

        private static string GetSaveFilePath()
        {
            return Application.persistentDataPath + "/" + SaveFileName + ".bs";
        }
        public static void SetLoadGame()
        {
            _loadGame = true;
        }

        private AzureTimeController _azureTimeController;
        private Activity[] _activities;
        private PlayerMovementController m_playerMovementController;

        private void Start()
        {
            _azureTimeController = FindObjectOfType<AzureTimeController>();
            if (_azureTimeController == null)
            {
                throw new MissingReferenceException("Missing AzureTimeController from the current scene!");
            }

            _activities = Resources.FindObjectsOfTypeAll<Activity>();
            if (_activities == null || _activities.Length == 0)
            {
                throw new MissingReferenceException("Missing Activities from the current scene!");
            }

            foreach (var activity in _activities)
            {
                if (String.IsNullOrEmpty(activity.GUID))
                {
                    Debug.Log("Found empty GUID in object " + activity.name);
                    Debug.Log("Parent " + activity.transform.root.name);
                }
            }

            m_playerMovementController = FindObjectOfType<PlayerMovementController>();
            if (m_playerMovementController == null)
            {
                throw new MissingReferenceException("Missing PlayerMovementController from the current scene!");
            }

            if (_loadGame)
            {
                LoadGame();
            }

            StartCoroutine(SaveCoroutine());
        }

        public void LoadGame()
        {
            string saveFilePath = GetSaveFilePath();

            FileStream file = null;
            if (!File.Exists(saveFilePath))
            {
                file = File.Create(saveFilePath);
                Debug.Log("Successfully created save file!");
            }
            else
            {
                file = File.Open(saveFilePath, FileMode.Open);
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();

            try
            {
                GameState gameState = file.Length > 0 ? binaryFormatter.Deserialize(file) as GameState : new GameState();
                UpdateWorldFromGameState(gameState);
                Debug.Log("Successfully loaded game!");
                
            }
            catch (SerializationException e)
            {
                // Catching the exception so that the game does not crash if the serialization fails
                Debug.LogException(e);
                Debug.Log("Failed to load game!");
            }
            finally
            {
                file.Close();
            }
        }

        IEnumerator SaveCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(_saveIntervalMinutes * 60f);
                SaveGame();
            }
        }

        public void SaveGame()
        {
            if (_saveGame)
            {
                string saveFilePath = GetSaveFilePath();

                FileStream file = null;
                if (!File.Exists(saveFilePath))
                {
                    file = File.Create(saveFilePath);
                    Debug.Log("Successfully created save file!");
                }
                else
                {
                    file = File.Open(saveFilePath, FileMode.Open);
                }

                BinaryFormatter binaryFormatter = new BinaryFormatter();

                try
                {
                    binaryFormatter.Serialize(file, GetGameState());
                    Debug.Log("Successfully saved game!");
                }
                catch (SerializationException e)
                {
                    // Catching the exception so that the game does not crash if the serialization fails
                    Debug.LogException(e);
                    Debug.Log("Failed to save game!");
                }
                finally
                {
                    file.Close();
                }
            }
        }

        public void DeleteSaveFile()
        {
            string saveFilePath = GetSaveFilePath();
            if (File.Exists(saveFilePath))
            {
                File.Delete(saveFilePath);
                Debug.Log("Successfully deleted save file!");
            }
        }


        private void UpdateWorldFromGameState(GameState gameState)
        {
            _azureTimeController.SetTimeline(gameState.timeOfDay);

            foreach (Activity activity in _activities)
            {
                if (string.IsNullOrEmpty(activity.GUID))
                {
                    Debug.Log("Activity: " + activity.gameObject.name + " does not have a GUID!");
                    Debug.Log("Parent " + activity.transform.root.name);
                    continue;
                }

                foreach (ActivityState activityState in gameState.activitiesStates)
                {
                    if (activityState.GUID == activity.GUID)
                    {
                        activity.LoadFromSaveInformation(activityState.saveInformation);
                        if (activityState.completed) activity.CompleteActivity();
                        break;
                    }
                }
            }

            foreach (Activity activity in _activities)
            {
                if (activity.IsCompleted())
                {
                    activity.PostComplete();
                }
            }

            m_playerMovementController.transform.position = _playerStart.position;
        }

        private GameState GetGameState()
        {
            List<ActivityState> activityStates = new List<ActivityState>();

            foreach (Activity activity in _activities)
            {

                if (string.IsNullOrEmpty(activity.GUID))
                {
                    Debug.Log("Activity: " + activity.gameObject.name + " does not have a GUID!" + activity.GUID);
                    Debug.Log("Parent " + activity.transform.root.name);
                    continue;
                }

                activityStates.Add(new ActivityState(
                    activity.GUID,
                    activity.IsCompleted(),
                    activity.GetSaveInformation()
                ));
            }

            return new GameState(
                false,
                _azureTimeController.GetTimeline(),
                m_playerMovementController.transform.position,
                activityStates
            );
        }

    }

    [Serializable]
    public class ActivityState
    {
        public string GUID;
        public bool completed;
        public ISerializable[] saveInformation;

        public ActivityState(string GUID, bool completed, ISerializable[] saveInformation)
        {
            this.GUID = GUID;
            this.completed = completed;
            this.saveInformation = saveInformation;
        }
    }


    [Serializable]
    public class SVector3 : ISerializable
    {
        public float x;
        public float y;
        public float z;

        public SVector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        protected SVector3(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            x = info.GetSingle("x");
            y = info.GetSingle("y");
            z = info.GetSingle("z");
        }

        public static implicit operator Vector3(SVector3 vector)
        {
            return new Vector3(vector.x, vector.y, vector.z);
        }

        public static implicit operator SVector3(Vector3 vector)
        {
            return new SVector3(vector.x, vector.y, vector.z);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("x", x);
            info.AddValue("y", y);
            info.AddValue("z", z);
        }
    }

    [Serializable]
    public class SFloat : ISerializable
    {
        public float v;

        public SFloat(float v)
        {
            this.v = v;
        }

        protected SFloat(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            v = info.GetSingle("v");
        }

        public static implicit operator float(SFloat sfloat)
        {
            return sfloat.v;
        }

        public static implicit operator SFloat(float v)
        {
            return new SFloat(v);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("v", v);
        }
    }

    [Serializable]
    public class SInt : ISerializable
    {
        public int i;

        public SInt(int i)
        {
            this.i = i;
        }

        protected SInt(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            i = info.GetInt32("i");
        }

        public static implicit operator int(SInt sInt)
        {
            return sInt.i;
        }

        public static implicit operator SInt(int i)
        {
            return new SInt(i);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("i", i);
        }
    }

    [Serializable]
    public class SString : ISerializable
    {
        public string s;

        public SString(string s)
        {
            this.s = s;
        }

        protected SString(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            s = info.GetString("s");
        }

        public static implicit operator string(SString sString)
        {
            return sString.s;
        }

        public static implicit operator SString(string s)
        {
            return new SString(s);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("s", s);
        }
    }

    [Serializable]
    public class SBool : ISerializable
    {
        public bool b;

        public SBool(bool b)
        {
            this.b = b;
        }

        protected SBool(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            b = info.GetBoolean("b");
        }

        public static implicit operator bool(SBool sBool)
        {
            return sBool.b;
        }

        public static implicit operator SBool(bool b)
        {
            return new SBool(b);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("b", b);
        }
    }

    [Serializable]
    public class GameState
    {
        public bool completedTutorial = false;
        public float timeOfDay = 15f;
        public SVector3 playerPosition; // TODO Add default player position
        public List<ActivityState> activitiesStates = new List<ActivityState>();

        public GameState() {}

        public GameState(bool completedTutorial, float timeOfDay, SVector3 playerPosition, List<ActivityState> activitiesStates)
        {
            this.completedTutorial = completedTutorial;
            this.timeOfDay = timeOfDay;
            this.playerPosition = playerPosition;
            this.activitiesStates = activitiesStates;
        }

    }
}