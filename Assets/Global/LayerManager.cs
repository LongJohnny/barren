﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Assets.Common.Scripts;
using Assets.Landmarks.Scripts;
using Assets.Mechanics;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Assets.Global
{
    public class LayerManager : Activity
    {
        public static LayerManager Instance;

        [SerializeField] private List<LayerItemList> _layerItemLists;
        private List<LayerItem>[] _sortedLayers = new List<LayerItem>[LayerItem.MAX_LAYERS];
        private LayerItem _currentTargetLayerItem;

        private void Start()
        {
            if (Instance != null)
            {
                throw new DuplicateSingletonException("Cannot have more than one LayerManager in the scene!");
            }

            Instance = this;

            if (_sortedLayers[0] == null)
            {
                for (int i = 0; i < LayerItem.MAX_LAYERS; ++i)
                {
                    _sortedLayers[i] = new List<LayerItem>();
                }

                foreach (LayerItemList layerGroup in _layerItemLists)
                {
                    layerGroup.Init();

                    if (layerGroup.startActive)
                    {
                        _sortedLayers[layerGroup.First.layerNumber].Add(layerGroup.First);
                    }
                }
            }
        }

        public bool ActivateLayer(LayerItem layerItem)
        {
            if (!_sortedLayers[layerItem.layerNumber].Contains(layerItem))
            {
                Debug.Log("Layer has been activated!");
                _sortedLayers[layerItem.layerNumber].Add(layerItem);
                _currentTargetLayerItem = layerItem;
                LandmarksManager.Instance.PointToTransform(layerItem.layerEvent.LandmarkTarget);
                return true;
            } 
            else
            {
                Debug.Log("Failed to activate layer!");
                return false;
            }
        }

        public void CompleteLayer(LayerItem layerItem)
        {
            if (_sortedLayers[layerItem.layerNumber].Contains(layerItem))
            {
                Debug.Log("Layer item has been removed from sorted layers!");
                _sortedLayers[layerItem.layerNumber].Remove(layerItem);
                LandmarksManager.Instance.StopPointingToTransform(layerItem.layerEvent.LandmarkTarget);
            }


            if (_currentTargetLayerItem == layerItem && layerItem.HasNext)
            {
                ActivateLayer(layerItem.next);
                Debug.Log("Advancing to next layer item!");
            }
            else
            {
                Debug.Log("Selecting random next layer item!");
                LayerItem nextItem;
                while ((nextItem = SelectRandomNextLayer()) != null)
                {
                    if (nextItem.layerEvent.IsComplete)
                    {
                        _sortedLayers[nextItem.layerNumber].Remove(nextItem);
                        Debug.Log("Removed already completed item in layer" + nextItem.layerNumber);
                    }
                    else
                    {
                        Debug.Log("Found item in Layer " + nextItem.layerNumber);
                        _currentTargetLayerItem = nextItem;
                        LandmarksManager.Instance.PointToTransform(nextItem.layerEvent.LandmarkTarget);
                        break;
                    }
                }

                if (nextItem == null)
                {
                    Debug.Log("Could not find random layer item!");
                }
            }
        }

        private LayerItem SelectRandomNextLayer()
        {
            int startIdx = 0;
            if (LandmarksManager.Instance.LandmarksAreWorking())
            {
                startIdx = 1;
            }

            List<LayerItem> layerList;
            for (int i = startIdx; i < _sortedLayers.Length; i++)
            {
                layerList = _sortedLayers[i];
                if (layerList.Count > 0)
                {
                    int idx = Random.Range(0, layerList.Count);

                    return layerList[idx];
                }
            }

            return _sortedLayers[0][0];
        }

        public override ISerializable[] GetSaveInformation()
        {
            List<ISerializable> list = new List<ISerializable>();

            list.Add(_currentTargetLayerItem != null
                ? new SString(_currentTargetLayerItem.layerEvent.GUID)
                : new SString(""));


            foreach (List<LayerItem> layer in _sortedLayers)
            {
                foreach (LayerItem layerItem in layer)
                {
                    list.Add(new SString(layerItem.layerEvent.GUID));
                }
            }

            return list.ToArray();
;        }

        public override void LoadFromSaveInformation(ISerializable[] saveInformation)
        {
            string currGUID = saveInformation[0] as SString;

            if (currGUID.Length > 0)
            {
                _currentTargetLayerItem = FindLayerItemFromGUID(currGUID);
            }

            LayerItem currItem;
            for (int i = 1; i < saveInformation.Length; i++)
            {
                currItem = FindLayerItemFromGUID(saveInformation[i] as SString);
                _sortedLayers[currItem.layerNumber].Add(currItem);
            }
        }

        private LayerItem FindLayerItemFromGUID(string guid)
        {
            foreach (LayerItemList layerGroup in _layerItemLists)
            {
                foreach (LayerItem layerItem in layerGroup.layerItems)
                {
                    if (layerItem.layerEvent.GUID == guid)
                    {
                        return layerItem;
                    }
                }
            }

            return null;
        }

        public override bool IsCompleted()
        {
            return true;
        }

        public override void PostComplete()
        {
            if (_currentTargetLayerItem != null)
            {
                LandmarksManager.Instance.PointToTransformInstantly(_currentTargetLayerItem.layerEvent.LandmarkTarget);
            }
        }
    }
}