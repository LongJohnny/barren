﻿using Assets.Mechanics;
using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace Assets.Global
{
    public class GUIDCreator : MonoBehaviour
    {

#if UNITY_EDITOR
        public void AttributeGUIDsToActivities()
        {
            var activities = Resources.FindObjectsOfTypeAll<Activity>();
            if (activities == null || activities.Length == 0)
            {
                throw new MissingReferenceException("Can't attribute GUIDs without activities in the current scene!");
            }

            foreach (var activity in activities)
            {
                activity.GUID = Guid.NewGuid().ToString();
                EditorUtility.SetDirty(activity);

                if (PrefabUtility.IsPartOfPrefabInstance(activity.gameObject))
                {
                    GameObject prefabRoot = PrefabUtility.GetNearestPrefabInstanceRoot(activity.gameObject);
                    //Debug.Log("Found prefab: " + prefabRoot.name);
                    PrefabUtility.RecordPrefabInstancePropertyModifications(prefabRoot);
                }
            }

        }

        public void CheckForMissingGUIDs()
        {
            var activities = Resources.FindObjectsOfTypeAll<Activity>();
            //var activities = FindObjectsOfType()<Activity>();
            if (activities == null || activities.Length == 0)
            {
                throw new MissingReferenceException("Can't attribute GUIDs without activities in the current scene!");
            }

            foreach (var activity in activities)
            {
                if (String.IsNullOrEmpty(activity.GUID))
                {
                    Debug.Log("Found empty GUID in object " + activity.name);
                    Debug.Log("Parent " + activity.transform.root.name);
                }
            }

            Debug.Log("Searched " + activities.Length + " activities!");
        }
#endif
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(GUIDCreator))]
    public class GUIDCreatorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GUIDCreator guidCreator = target as GUIDCreator;
            if (guidCreator != null && GUILayout.Button("Attribute GUIDs to Activities"))
            {
                guidCreator.AttributeGUIDsToActivities();

                if (GUI.changed)
                {
                    EditorUtility.SetDirty(guidCreator);
                    EditorSceneManager.MarkSceneDirty(guidCreator.gameObject.scene);
                }
            }

            if (guidCreator != null && GUILayout.Button("Check for missing GUIDs"))
            {
                guidCreator.CheckForMissingGUIDs();
            }
        }
    }
#endif
}