using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTests : MonoBehaviour
{
    public Transform pivetPoint;
    public float rotationSpeed = 10;
    public Vector3 rotationAxis = Vector3.up;
    public Vector3 worldUp = Vector3.up;
    public float slerpFactor = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = LookAt((pivetPoint.position - transform.position).normalized);
        transform.RotateAround(pivetPoint.position, rotationAxis, rotationSpeed * Time.deltaTime);
    }


    Quaternion LookAt(Vector3 direction)
    {

        float dot = Vector3.Dot(direction.normalized, Vector3.forward);

        if (Mathf.Abs(dot + 1) < 0.000001f)
        {
            return new Quaternion(Vector3.up.x, Vector3.up.y, Vector3.up.z, Mathf.PI);
        } 
        else if (Mathf.Abs(dot - 1) < 0.000001f)
        {
            return Quaternion.identity;
        }

        float rotationAngle = Mathf.Acos(dot);
        Vector3 rotationAxis = Vector3.Cross(Vector3.forward, direction.normalized);
        rotationAxis = Vector3.Normalize(rotationAxis);
        return Quaternion.AngleAxis((rotationAngle * 180)/Mathf.PI, rotationAxis.normalized);
    }


}
