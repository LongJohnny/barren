using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScriptureMenuManager : MonoBehaviour {
    private void OnEnable () {
        List<int> scriptures = ScriptureSingleton.Instance.GetOwnedScriptures ();
        foreach (Transform t in transform) {
            t.gameObject.SetActive (false);
        }
        foreach (int i in scriptures) {
            transform.GetChild (i - 1).gameObject.SetActive (true);
        }

    }

    public void GetScriptures () {
        List<Texture> textures = new List<Texture> ();
        foreach (RawImage i in GetComponentsInChildren<RawImage> (true)) {
            textures.Add (i.texture);
        };
        ScriptureSingleton.Instance.SetImageList (textures);
    }


}