using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMenuController : MonoBehaviour {
    
    public Button firstSelectedButton;

    private void OnEnable () {
        if (firstSelectedButton != null)
            firstSelectedButton.Select ();
        else {
            // TODO Change this really bad solution
            firstSelectedButton = transform.parent.GetComponentInChildren<Button>();
            firstSelectedButton.Select ();
        }
    }
}