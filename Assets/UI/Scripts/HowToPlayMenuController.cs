using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UI {
    public class HowToPlayMenuController : MonoBehaviour {

        public Button tutorialVideosButton;

        public void OnBecomeActive() {
            tutorialVideosButton.Select();
        }
        
    }
}