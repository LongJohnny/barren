using System.IO.Compression;
using Assets.Global;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.UI.Scripts
{
    public class MainMenuController : MonoBehaviour {

        public GameObject continueButton;
        public GameObject mainMenu;
        public GameObject controlsMenu;
        private Controls _controls;

        private void Start () {
            _controls = new Controls ();
            _controls.UI.Pause.Enable ();
            _controls.UI.Back.Enable ();

            continueButton.SetActive(GameManager.IsThereSaveFile());

        }

        void Update () {

            if (_controls.UI.Pause.triggered || _controls.UI.Back.triggered) {
                if (controlsMenu != null && controlsMenu.activeInHierarchy) {
                    controlsMenu.SetActive (false);
                    mainMenu.SetActive (true);
                }
            }
        }


        public void NewGame()
        {
            GameManager.CreateOrOverwriteSaveFile();
            SceneManager.LoadScene("TutorialScene");
        }

        public void ContinueGame () {
            GameManager.SetLoadGame();
            SceneManager.LoadScene ("MainScene");
        }

        public void ExitGame () {
            Application.Quit ();
        }

    }
}