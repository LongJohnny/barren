﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.UI.Scripts
{
    public class MessagesController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _messageText;
        [SerializeField] private Image _background;
        private Coroutine _messageCoroutine;
        [SerializeField] private float _fadeDuration = 2f;
        [SerializeField] private float _maxBackgroundAlpha = 0.2f;
        private float _currFade = 0f;

        private void Start()
        {
            if (_messageText == null)
            {
                throw new MissingReferenceException("Messages controller requires a _message text!");
            }

            _messageText.alpha = 0f;
            Color color = _background.color;
            color.a = 0f;
            _background.color = color;
        }

        public void ShowMessage(String message)
        {
            if (_messageCoroutine != null)
            {
                StopCoroutine(_messageCoroutine);
            }

            _messageCoroutine = StartCoroutine(AnimateMessage(true, message));
        }

        public void ShowMessage(GameObject messageGameObject)
        {

        }

        public void StopShowingMessage()
        {
            if (_messageCoroutine != null)
            {
                StopCoroutine(_messageCoroutine);
            }

            _messageCoroutine = StartCoroutine(AnimateMessage(false, ""));
        }

        IEnumerator AnimateMessage(bool fadeIn, String message)
        {
            float fadeDir = fadeIn ? 1f : -1f;
            float fadeStart = fadeIn ? 0f : _fadeDuration;
            float fadeEnd = fadeIn ? _fadeDuration : 0f;

            Color color = _background.color;
            
            if (fadeIn) // If this is a fade in and there is an already displayed message then fade it out first
            {
                do
                {
                    _currFade -= Time.deltaTime;
                    _currFade = Mathf.Clamp(_currFade, 0f, _fadeDuration);
                    _messageText.alpha = _currFade / _fadeDuration;
                    color.a = (_currFade / _fadeDuration) * _maxBackgroundAlpha;
                    _background.color = color;

                    yield return new WaitForEndOfFrame();

                } while (_currFade > float.Epsilon);

                _messageText.text = message; // Only when the fade out is over do we update the message
            }

            do
            {
                //Debug.Log(_currFade);

                _currFade += Time.deltaTime * fadeDir;
                _currFade = Mathf.Clamp(_currFade, 0f, _fadeDuration);
                _messageText.alpha = _currFade / _fadeDuration;
                color.a = (_currFade / _fadeDuration) * _maxBackgroundAlpha;
                _background.color = color;

                yield return new WaitForEndOfFrame();

            } while (Math.Abs(_currFade - fadeEnd) > float.Epsilon);
        }
    }
}