using UnityEngine;

[RequireComponent (typeof (Collider))]
public class GetScriptureOnTrigger : MonoBehaviour {
    public int scriptureId = 1;

    private void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "Player") {
            ScriptureSingleton.Instance.UnlockScripture (scriptureId);
        }
    }

    private void OnCollisionEnter (Collision other) {
        if (other.gameObject.tag == "Player") {
            ScriptureSingleton.Instance.UnlockScripture (scriptureId);
        }
    }
}