using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptureSingleton : MonoBehaviour {

    public static ScriptureSingleton Instance;

    [HideInInspector]
    public List<Texture> textures;

    //Scriptures contain an id, and are stored in a list of ids or a 
    //single integer whose bits represent which scriptures the player owns
    private List<int> scripturesList;

    private void Awake () {
        if (Instance == null) Instance = this;
        else {
            Debug.LogWarning ("Multiple scripture singleton instances found");
            Destroy (this);
        }

        scripturesList = LoadOwnedScriptures ();
        foreach (ScriptureMenuManager smm in Resources.FindObjectsOfTypeAll (typeof (ScriptureMenuManager))) {
            smm.GetComponent<ScriptureMenuManager> ().GetScriptures ();
        }
    }

    public Texture UnlockScripture (int id) {
        if (!scripturesList.Contains (id)) {
            scripturesList.Add (id);
        }
        SaveOwnedScriptures ();
        return textures[id - 1];
    }

    public List<int> GetOwnedScriptures () {
        return scripturesList;
    }

    private void OnDestroy () {
        SaveOwnedScriptures ();
    }

    private List<int> LoadOwnedScriptures () {
        List<int> aux = new List<int> ();
        if (!PlayerPrefs.HasKey ("Scriptures")) return aux;
        int ownedScriptures = PlayerPrefs.GetInt ("Scriptures");
        Debug.Log ("Loading: " + ownedScriptures.ToString ());
        int i = 1;
        while (ownedScriptures != 0) {
            if (ownedScriptures % 2 == 1) {
                aux.Add (i);
            }
            i += 1;
            ownedScriptures = ownedScriptures >> 1;
        }
        return aux;
    }

    private void SaveOwnedScriptures () {
        if (scripturesList == null) return;
        int aux = 0;
        foreach (int i in scripturesList) {
            aux += (int) Mathf.Pow (2, i - 1);
        }
        Debug.Log ("Saving: " + aux);
        PlayerPrefs.SetInt ("Scriptures", aux);
        PlayerPrefs.Save ();
    }

    public void SetImageList (List<Texture> newImages) {
        textures = newImages;
    }
}