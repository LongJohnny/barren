using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class CreditsAnimator : MonoBehaviour
{   
    [SerializeField]
    private GameObject[] _parts;
    private GameObject _currPart;
    private RawImage[] _currImages;
    private TextMeshProUGUI[] _currTexts;
    private int _currPartIdx = 0;
    private float _alphaLerpFactor = 0.0f;
    private float _alphaLerpFactorSign = 1.0f;

    [SerializeField]
    private float _visibleDuration;
    private float _currentVisibleDuration;

    [SerializeField]
    private float  _alphaLerpFactorChangeSpeed = 0.5f;

    [SerializeField]
    private GameEvent _creditsOverEvent;

    // Start is called before the first frame update
    void Start()
    {
        GetCurrentTexts(); 
        _currentVisibleDuration = _visibleDuration; 
    }

    void Update() {
        UpdateCredits();
    }

    private void UpdateCredits() {

        UpdateLerpFactor();
        UpdateAlpha();

        if (_alphaLerpFactor == 1.0f) {
            if (_currentVisibleDuration <= 0.0f) {
                _currentVisibleDuration = _visibleDuration;
                _alphaLerpFactorSign *= -1.0f;
            } else {
                _currentVisibleDuration -= Time.deltaTime;
            }
            
        } else if (_alphaLerpFactor == 0.0f && _alphaLerpFactorSign == -1.0f) {
            _alphaLerpFactorSign *= -1.0f;
            ++_currPartIdx;
            if (_currPartIdx < _parts.Length)
                GetCurrentTexts();
            else 
                SceneManager.LoadScene(0);
        }

        Debug.Log(_alphaLerpFactor);
    }

    private void GetCurrentTexts() {
        if (_currPart != null)
            _currPart.SetActive(false);

        _currPart = _parts[_currPartIdx];
        _currPart.SetActive(true);
        _currTexts = _currPart.GetComponentsInChildren<TextMeshProUGUI>();
        _currImages = _currPart.GetComponentsInChildren<RawImage>();
        UpdateAlpha();
    }

    private void UpdateAlpha() {
        foreach (TextMeshProUGUI text in _currTexts)
        {
            text.alpha = Mathf.Lerp(0.0f, 1.0f, _alphaLerpFactor);
        }

        Color currColor;
        foreach(RawImage img in _currImages) {
            currColor = img.color;
            currColor.a = Mathf.Lerp(0.0f, 1.0f, _alphaLerpFactor);
            img.color = currColor;
        }
    }

    private void UpdateLerpFactor() {
        _alphaLerpFactor += _alphaLerpFactorSign * Time.deltaTime * _alphaLerpFactorChangeSpeed;
        _alphaLerpFactor = Mathf.Clamp(_alphaLerpFactor,0.0f, 1.0f);
    }

}
