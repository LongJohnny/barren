﻿using UnityEngine.SceneManagement;
using UnityEngine;

namespace UI {
    public class PauseMenuController : MonoBehaviour {

        private bool _isPaused = false;
        public GameObject pauseMenu;
        public GameObject controlsMenu;

        private Controls _controls;

        private void Start() {
            _controls = new Controls();
            _controls.UI.Pause.Enable();
            _controls.UI.Back.Enable();
        }

        private void Update() {

            if (_controls.UI.Pause.triggered || _controls.UI.Back.triggered) {
                if (controlsMenu != null && controlsMenu.activeInHierarchy) {
                    controlsMenu.SetActive(false);
                    pauseMenu.SetActive(true);
                } else {
                    if (_controls.UI.Pause.triggered)
                        TogglePause();
                }
            }
        }

        public void TogglePause() {
            _isPaused = !_isPaused;
            Cursor.visible = _isPaused;
            pauseMenu.SetActive(_isPaused);
            Time.timeScale = _isPaused ? 0 : 1;
        }

        public void ExitGame() {
            Application.Quit();
        }

        public void EnterMenu() {
            Time.timeScale = 1;
            SceneManager.LoadScene("MainMenu");
        }

        private void OnDestroy() {
            Time.timeScale = 1;
        }

        private void OnDisable() {
            Time.timeScale = 1;
        }
    }
}