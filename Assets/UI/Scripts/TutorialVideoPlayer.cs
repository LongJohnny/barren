using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Video;
using TMPro;
using UnityEngine.UI;

public class TutorialVideoPlayer : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _playButton;
    private VideoPlayer _videoPlayer;
    private RawImage _rawImage;

    [SerializeField]
    private TMP_Dropdown _videoSelectDropdown;
    
    [SerializeField]
    private VideoClip[] _tutorialVideos;
    [SerializeField]
    private RenderTexture _videoTexture;

    void Awake() {
        _rawImage = GetComponentInChildren<RawImage>();
        _videoPlayer = GetComponentInChildren<VideoPlayer>();
        _videoPlayer.clip = _tutorialVideos[0];
    }

    private void OnEnable() {
        _playButton.text = "Play";
        //_videoPlayer.clip = _tutorialVideos[0];
        _videoSelectDropdown.value = 0;
        _videoSelectDropdown.RefreshShownValue();
        //UpdateThumbnail();
    }

    public void OnVideoChange(int idx) {
        _playButton.text = "Play";
        _videoPlayer.clip = _tutorialVideos[idx];
        //UpdateThumbnail();
    }

    private void Update() {
        if (!_videoPlayer.isPlaying && _videoPlayer.frame > 0) {
            _playButton.text = "Play";
            _videoPlayer.Stop();
        }
    }

    public void OnClickPlayStop() {
        if (_videoPlayer.isPlaying) {
            _playButton.text = "Play";
            _videoPlayer.Stop();
        } else {
            _playButton.text = "Stop";
            _rawImage.texture = _videoTexture;
            _videoPlayer.Play();
        }
    }

    private void UpdateThumbnail() {

        //_videoPlayer.time = 0.0;
        _videoPlayer.frame = 20;
        
        _rawImage.texture = _videoPlayer.texture;
        _videoPlayer.Play();
        _videoPlayer.Stop();     
    }

    
}
