﻿using System.Collections;
using UnityEngine;

namespace Assets.Player
{
    public class DriftParticleController : MonoBehaviour
    {

        [SerializeField] private ParticleSystem _driftParticles;


        private void Start()
        {
            if (_driftParticles != null)
            {
                _driftParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }


        public void PlayDriftParticles(Vector3 velocity, Vector3 driftDirection)
        {
            if (_driftParticles != null)
            {
                if (!_driftParticles.isPlaying)
                {
                    _driftParticles.Play();
                }

                _driftParticles.transform.position = transform.position;

                Vector3 planeNormal = Vector3.Cross(velocity, driftDirection);
                planeNormal *= Mathf.Sign(planeNormal.y);

                _driftParticles.transform.rotation = Quaternion.LookRotation(-driftDirection.normalized, planeNormal);

            }

        }


        

        public void StopDriftParticles()
        {
            if (_driftParticles != null)
            {
                if (_driftParticles.isEmitting)
                    _driftParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }

        private void Update()
        {

        }

    }
}