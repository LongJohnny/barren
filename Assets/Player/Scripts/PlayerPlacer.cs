﻿using System.Collections;
using Assets.Common.Scripts;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

using UnityEngine;

namespace Assets.Player.Scripts
{
    public class PlayerPlacer : MonoBehaviour
    {
#if UNITY_EDITOR
        public bool PlacePlayer()
        {
            var view = SceneView.currentDrawingSceneView;

            if (view != null)
            {
                if (Physics.Raycast(view.camera.transform.position, view.camera.transform.forward,
                        out RaycastHit hit, 100f))
                {
                    
                    transform.position = hit.point + Vector3.up * 2f;
                    return true;
                }
            }

            return false;
        }
#endif

    }

#if UNITY_EDITOR
    [CustomEditor(typeof(PlayerPlacer))]
    public class ObjectSpawnerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            PlayerPlacer playerPlacer = target as PlayerPlacer;
            if (playerPlacer != null && GUILayout.Button("Place player in camera direction"))
            {
                if (playerPlacer.PlacePlayer() && GUI.changed)
                {
                    EditorUtility.SetDirty(playerPlacer);
                    EditorSceneManager.MarkSceneDirty(playerPlacer.gameObject.scene);
                }
            }

        }
    }
#endif
}