﻿using Assets.Mechanics.StatueCollectables;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Assets.Global;
using Assets.Mechanics;
using UnityEngine;

namespace Assets.Player.Scripts
{
    public class OrbitCollectiblesController : Activity
    {
        [SerializeField] OrbitCollectible[] _orbitCollectibles = new OrbitCollectible[5];

        public void Collect(int number)
        {
            _orbitCollectibles[number].gameObject.SetActive(true);
        }

        public bool PlayerHasAllCollectibles()
        {
            foreach (OrbitCollectible orbitCollectible in _orbitCollectibles)
            {
                if (!orbitCollectible.isActiveAndEnabled) return false;
            }

            return true;
        }

        public OrbitCollectible[] GetOrbitCollectibles()
        {
            return _orbitCollectibles;
        }
        public override ISerializable[] GetSaveInformation()
        {
            List<ISerializable> list = new List<ISerializable>();
            for (int i = 0; i < _orbitCollectibles.Length; i++)
            {
                if (_orbitCollectibles[i].isActiveAndEnabled)
                {
                    list.Add(new SInt(i));
                }
            }

            return list.ToArray();
        }

        public override void LoadFromSaveInformation(ISerializable[] saveInformation)
        {
            for (int i = 0; i < saveInformation.Length; i++)
            {
                _orbitCollectibles[saveInformation[i] as SInt].gameObject.SetActive(true);
            }
        }
    }
}