﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Player Material")]
public class PlayerMaterial : ScriptableObject {

    public PhysicMaterial physicsMaterial;
    public float gravityMultiplier = 1;
    public float mass = 50;
    public Material mat;
    public Mesh mesh;
    public float speed;
    public float midAirSpeed;
    public float timeToSpeed;
    public float timeToSpeedMidAir;
    public float jumpForce;
    public bool freezeRotation;
    public Material trailMat;
}
