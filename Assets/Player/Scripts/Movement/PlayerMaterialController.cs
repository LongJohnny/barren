﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent (typeof (Collider), typeof (Rigidbody), typeof (MeshRenderer))]
public class PlayerMaterialController : MonoBehaviour {

    [SerializeField] private AudioClip[] clips_array;

    public float dialvolume;
    private int randomSound;

    private List<AudioSource> _audios = new List<AudioSource> ();

    [SerializeField] private List<PlayerMaterial> _materials;
    private List<PlayerMaterial> _currentMaterials = new List<PlayerMaterial> ();
    public PlayerMaterial currentMaterial { get; private set; }

    private Rigidbody _rb;
    private MeshRenderer _mr;
    private MeshFilter _mf;
    private Collider _col;

    private Vector3 _initialGravity;
    private Vector3 _currentGravity;

    private bool _leftPressed = false, _rightPressed = false;

    private InputAction _changeToMaterialA, _changeToMaterialB;

    private bool _isPaused = false;

    private TrailRenderer _playerTrail;


    void Awake () {
        Controls _controls = new Controls ();
        _changeToMaterialA = _controls.Materials.ChangeToMaterialA;
        _changeToMaterialB = _controls.Materials.ChangeToMaterialB;
        _changeToMaterialA.Enable ();
        _changeToMaterialB.Enable ();
    }


    // Start is called before the first frame update
    void Start () {
        _rb = GetComponent<Rigidbody> ();
        _mr = GetComponent<MeshRenderer> ();
        _mf = GetComponent<MeshFilter> ();
        _col = GetComponent<Collider> ();
        _playerTrail = GetComponentInChildren<TrailRenderer>();
        PushMaterial (_materials[0]);

        _initialGravity = Physics.gravity;
        _currentGravity = _initialGravity;

        //Creating an audioSource for each clip in clips_array
        foreach (AudioClip c in clips_array) {
            AudioSource asource = gameObject.AddComponent<AudioSource>();
            asource.playOnAwake = false;
            asource.clip = c;
            asource.volume = dialvolume;
            _audios.Add (asource);
        }
    }

    // Update is called once per frame
    void Update () {
        if (!_isPaused)
        {
            if (!_leftPressed && _changeToMaterialA.triggered)
            {
                _leftPressed = true;
                PushMaterial(_materials[1]);
                randomSound = Random.Range(0, 14);
                _audios[randomSound].Play();            


            }
            else if (_leftPressed && _changeToMaterialA.triggered)
            {
                _leftPressed = false;
                PopMaterial(_materials[1]);

            }

            if (!_rightPressed && _changeToMaterialB.triggered)
            {
                _rightPressed = true;
                PushMaterial(_materials[2]);
                randomSound = Random.Range(0, 14);
                _audios[randomSound].Play();


            }
            else if (_rightPressed && _changeToMaterialB.triggered)
            {
                _rightPressed = false;
                PopMaterial(_materials[2]);

            }
        }
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        // Solves the change material issue by reseting to the default material
        // when the window is out of focus

        _isPaused = !hasFocus;

        if (_isPaused)
        {
            while (_currentMaterials.Count > 1)
                PopMaterial(_currentMaterials[1]);
            _leftPressed = false;
            _rightPressed = false;
            _changeToMaterialA.Disable();
            _changeToMaterialB.Disable();
        } 
        else
        {
            _changeToMaterialA.Enable();
            _changeToMaterialB.Enable();
        }
    }

    private void OnEnable()
    {
        _changeToMaterialA.Enable();
        _changeToMaterialB.Enable();
    }


    private void OnDisable()
    {
        ClearToDefault();
        _changeToMaterialA.Disable();
        _changeToMaterialB.Disable();
    }

    public void ClearToDefault()
    {
        while (_currentMaterials.Count > 1)
            PopMaterial(_currentMaterials[1]);
        _leftPressed = false;
        _rightPressed = false;
        _changeToMaterialA.Dispose();
        _changeToMaterialB.Dispose();
    }

    private void ApplyMaterial (PlayerMaterial playerMaterial) {         
        if (currentMaterial != null)
            _currentGravity = _currentGravity / currentMaterial.gravityMultiplier * playerMaterial.gravityMultiplier;
        currentMaterial = playerMaterial;
        _rb.mass = playerMaterial.mass;
        _col.material = playerMaterial.physicsMaterial;
        _mr.material = playerMaterial.mat;
        _mf.sharedMesh = playerMaterial.mesh;
        _playerTrail.material = playerMaterial.trailMat;
        
        
        if (playerMaterial.freezeRotation) {
            _rb.freezeRotation = true;
        } else {
            _rb.freezeRotation = false;
        }
    }


    private void FixedUpdate()
    {
        _rb.AddForce(_rb.mass * _currentGravity);
    }

    private void PushMaterial (PlayerMaterial playerMaterial) {
        _currentMaterials.Add (playerMaterial);
        ApplyMaterial (playerMaterial);
    }

    private void PopMaterial (PlayerMaterial playerMaterial) {
        _currentMaterials.Remove (playerMaterial); // Remove the material from the current materials list
        ApplyMaterial (_currentMaterials[_currentMaterials.Count - 1]); // Apply the one on top of the list
    }

    public PlayerMaterial GetCurrentMaterial () {
        return _currentMaterials[_currentMaterials.Count - 1];
    }
}