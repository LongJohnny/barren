using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Cameras.Scripts;
using Assets.Player;
using Assets.Player.Scripts;
using CameraShake;
using UnityEngine;
using Unity.Mathematics;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Rigidbody), typeof(PlayerMaterialController), typeof(DustParticleController)), 
 RequireComponent(typeof(DriftParticleController))]
public class PlayerMovementController : MonoBehaviour
{

    #region FIELDS AND PROPERTIES
    public const byte JUMP = 1;
    public const byte MOVE = 2;
    public const byte DASH = 4;
    public const byte DRIFT = 8;

    private byte _behaviourFlags = 0;

    // Parameters
    [Header("Movement parameters")]
    [SerializeField] private float _terminalVelocity = 50;
    [SerializeField] private TrailRenderer _trailRenderer;
    [SerializeField] private TrailRenderer _skidmark;
    [SerializeField] private float _dashSpeed;
    [SerializeField][Range(0f, 5f)] private float _dragOnStop = 2f;
    [SerializeField] private Vector3Event _onVelocityChange;
    [SerializeField] private float _groundDistThreshold = 0.8f;
    [SerializeField] private float _driftSpeedThreshold = 30f;

    // Audio fields
    private List<AudioSource> _audios = new List<AudioSource>();
    [Header("Movement sound parameters")]
    [SerializeField] private AudioClip[] _clipsArray;
    [SerializeField] private float _volume;
    [SerializeField] private AudioSource _driftSound;
    private int randomSound;

    // This fields only serve to display the values
    [Header("Movement state (Readonly)")]
    public float Speed;
    public float AngularSpeed;

    // Public getters
    public float TerminalVelocity { get => _terminalVelocity; }
    public bool OnGround { get => _onGround; }
    public bool ForceJump = false;
    public bool ForceDash = false;

    [Header("Debug options")][SerializeField]private bool _drawJoyStickLines = false;

    // Internal State
    private float _oldHorizontal, _oldVertical;
    private float _horizontalInput, _verticalInput;
    private float _driftForce;
    private bool _tryJump, _onGround, _isDash, _tryDrift;
    private float _sqrterminalVelocity;
    private float _driftDir;

    // Required Components
    private PlayerMaterialController _playerMaterialController;
    private DustParticleController _dustParticleController;
    private DriftParticleController _driftParticleController;

    private Rigidbody _rb;
    private Camera _camera;
    private Controls _controls;

    #endregion

    #region UNITY_CALLBACKS
    private void Awake()
    {
        _controls = new Controls();
        _controls.Movement.Jump.Enable();
        _controls.Movement.MoveX.Enable();
        _controls.Movement.MoveY.Enable();
        _controls.Movement.Dash.Enable();
        _controls.Movement.DriftPress.Enable();
        _controls.Movement.DriftRelease.Enable();
        _controls.Movement.PrintPosition.Enable();
    }

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _playerMaterialController = GetComponent<PlayerMaterialController>();
        _dustParticleController = GetComponent<DustParticleController>();
        _driftParticleController = GetComponent<DriftParticleController>();
        _sqrterminalVelocity = _terminalVelocity * _terminalVelocity;
        _camera = Camera.main;
        _driftForce = 0;

        //Creating an audioSource for each clip in clips_array
        foreach (AudioClip c in _clipsArray)
        {
            AudioSource asource = gameObject.AddComponent<AudioSource>();
            //asource.playOnAwake = true;
            asource.loop = true;
            asource.clip = c;
            asource.volume = 0;
            //asource.spatialBlend = 1f;
            _audios.Add(asource);
        }

        if (_audios.Count > 0)
        {
            _audios[0].Play();
            _audios[1].Play();
        }

        _oldPosition = transform.position;

    }

    private Vector3 _oldPosition;
    private void Update()
    {
        _oldHorizontal = _horizontalInput;
        _oldVertical = _verticalInput;

        if ((_behaviourFlags & MOVE) != MOVE)
        {
            _horizontalInput = _controls.Movement.MoveX.ReadValue<float>();
            _verticalInput = _controls.Movement.MoveY.ReadValue<float>();
        }

        if ((_behaviourFlags & JUMP) != JUMP && (_controls.Movement.Jump.triggered))
        {
            _tryJump = true;
            Invoke("StopTryJump", 0.1f);
        }

        if (((_behaviourFlags & DRIFT) != DRIFT  && _controls.Movement.DriftPress.triggered) && _onGround) //&& Mathf.Abs(_horizontalInput) > 0.1f
        {
            _tryDrift = true;
            _driftDir = Mathf.Sign(_horizontalInput);
            _driftSound.Play();
        }
        else if (_controls.Movement.DriftRelease.triggered)
        {
            _tryDrift = false;
            _driftDir = 0f;
            _driftForce = 0;
            _driftParticleController.StopDriftParticles();
        }

        if (_tryDrift)
        {
            float horizontalInput = _verticalInput < 0f ? Mathf.Sign(_horizontalInput) : _horizontalInput;
            float rotationIncrease = _driftForce / 10f;
            //rotationIncrease = Mathf.Sin(rotationIncrease * Mathf.PI * .5f);
            CameraController.Instance.EnqueueRotation(new Vector2( horizontalInput * 7f * rotationIncrease /* * Mathf.Min(1f, Mathf.Pow(Speed / _driftSpeedThreshold, 2f))*/, 0f));
        }

        if ((_behaviourFlags & DASH) != DASH && _controls.Movement.Dash.triggered || ForceDash)
        {
            _isDash = true;
            Invoke("StopIsDash", 0.1f);
        }

        UpdateSound();

#if UNITY_EDITOR
        if (_controls.Movement.PrintPosition.triggered)
        {
            Debug.Log(transform.position);
        }
#endif

        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit) &&
            hit.transform.gameObject.name == "InfiniteWorld")
        {
            transform.position = _oldPosition + Vector3.up*0.0001f;
            Debug.Log("Pushed from ground");
        }

        _oldPosition = transform.position;
    }

    private void FixedUpdate()
    {
        // Update speed and angular speed to view in editor
        Speed = _rb.velocity.magnitude;
        AngularSpeed = _rb.angularVelocity.magnitude;

        _rb.drag = 0.0f;
        if (_horizontalInput != 0 || _verticalInput != 0)
        {
            float dampenFactor = JoystickDampen(Mathf.Abs(_horizontalInput) + Mathf.Abs(_verticalInput));

            // The movement direction is always in the camera forward
            Vector3 movementDirection = _horizontalInput * _camera.transform.right + _verticalInput * Vector3.ProjectOnPlane(_camera.transform.forward, Vector3.up).normalized;
            movementDirection = movementDirection.normalized * dampenFactor;
            movementDirection.y = 0.0f;

            // If on the ground move with speed otherwise with midAirSpeed 
            if (_onGround)
            {
                _rb.AddForceToReachVelocity(movementDirection * _playerMaterialController.currentMaterial.speed, _playerMaterialController.currentMaterial.timeToSpeed);

                if (_playerMaterialController.currentMaterial.name == "Rubber")
                {
                    _rb.drag = Mathf.Lerp(1f, 0.3f, Mathf.Min(1f, Speed / _driftSpeedThreshold));
                }

                if (_tryDrift   &&  Speed > _driftSpeedThreshold    &&  Mathf.Abs(_horizontalInput) > 0.1f) //drifting
                {

                    float horizontalInput = _verticalInput < 0f ? Mathf.Sign(_horizontalInput) : _horizontalInput;

                    Vector3 perfectDriftDirection = (Vector3.Cross(Vector3.up, _rb.velocity)).normalized;

                    Vector3 driftDirection = perfectDriftDirection * (horizontalInput + _driftDir) / 2f;

                    driftDirection = (driftDirection + _rb.velocity.normalized * 3f).normalized;

                    if (_driftForce == 0f)
                    {

                        // Using a coroutine to only play the particles when the player drifts for more than 0.05 seconds
                        // otherwise the particles are played with a bounce
                        StartCoroutine(PlayDriftParticles(_driftDir * perfectDriftDirection, 0.05f));
                    }

                    _driftForce += 10f * Mathf.Abs(_horizontalInput) * Time.fixedDeltaTime;
                    _driftForce = Mathf.Min(_driftForce, 10f);

                    _rb.AddForceToReachVelocity(driftDirection * Speed * 1.5f,
                        (_playerMaterialController.currentMaterial.timeToSpeed / _driftForce) * .7f);

                } else
                {
                    _driftForce = 0f;
                        
                        //Mathf.Lerp(_driftForce, 0, 10f * Time.fixedDeltaTime);
                }
            }
            else
            {
                _driftForce = 0f;
                

                //Mathf.Lerp(_driftForce, 0, 10f * Time.fixedDeltaTime);
                _rb.AddForceToReachVelocity(movementDirection * _playerMaterialController.currentMaterial.midAirSpeed, _playerMaterialController.currentMaterial.timeToSpeedMidAir);
            }
        }
        else if (ForceDash)
        {
            //Debug.Log("DASH NO INPUT");
            ForceDash = false;
            Vector3 dashDirWhileStopped = (Vector3.ProjectOnPlane(_camera.transform.forward, Vector3.up).normalized + Vector3.up*0.25f).normalized;
            _rb.AddForce(dashDirWhileStopped.normalized * _dashSpeed, ForceMode.Impulse);
        }
        else if ((_horizontalInput == 0 && _verticalInput == 0) && _onGround && _playerMaterialController.currentMaterial.name == "Glass")
        {//if glass material is on, and player is on ground and not trying to move, then sphere drags (slows down)
            _rb.drag = _dragOnStop;
        }

        if (_tryJump && _onGround || ForceJump)
        {
            ForceJump = false;
            _onGround = false;
            _tryJump = false;
            _rb.AddForce(Vector3.up * _playerMaterialController.currentMaterial.jumpForce, ForceMode.Impulse);
            randomSound = UnityEngine.Random.Range(2, 16);
            _audios[randomSound].volume = _volume;
            _audios[randomSound].loop = false;
            _audios[randomSound].Play();
        }

        //Clamp to terminal velocity
        if (_rb.velocity.sqrMagnitude > _sqrterminalVelocity)
            _rb.velocity = _rb.velocity.normalized * _terminalVelocity;

        if ((_behaviourFlags & MOVE) != MOVE)
        {
            bool onTerrain = true;
            if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, _groundDistThreshold))
            {
                _onGround = true;
                if (!hit.collider.name.Contains("Terrain"))
                {
                    onTerrain = false;
                }
            }
            else
            {
                _onGround = false;
                if (_audios.Count > 0)
                {
                    _audios[0].volume -= 0.001f * Speed;
                }
            }

            // Play dust particles when on the ground or trail when on the air; skidmarks on drift on ground
            _skidmark.transform.position = transform.position - new Vector3(0, 0.49f, 0); //not 0.5 because it glitches with the ground
            if (_onGround)
            {
                if (onTerrain) _dustParticleController.PlayDustParticles(_rb.velocity, hit.normal, hit.point);
                _trailRenderer.emitting = false;
                _skidmark.emitting = _tryDrift;
            }
            else
            {
                _dustParticleController.StopDustParticles();
                _trailRenderer.emitting = true;
                _skidmark.emitting = false;
                _driftParticleController.StopDriftParticles();
            }
        }

        _onVelocityChange.Raise(_rb.velocity);

    }

    private void OnCollisionStay(Collision other)
    {
        // Only set on ground when the angle between the collision surface normal
        // and up is below 90 degrees
        // TODO Instead of below 90, tweak the value until we are satisfied
        // because 89 degrees is almost a wall
        if (Vector3.Dot(other.contacts[0].normal, Vector3.up) > 0.2f)
        {
            _onGround = true;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        _onGround = false;
        //randomSound = UnityEngine.Random.Range(2, 16);
        //_audios[randomSound].volume = _volume;
        //_audios[randomSound].loop = false;
        //_audios[randomSound].Play();
    }

    private List<Vector3> _lines = new List<Vector3>();
    private void OnDrawGizmos()
    {

#if UNITY_EDITOR
        // Joystick Debug   Shows input from left joystick on xy axis
        if (_drawJoyStickLines && _camera != null)
        {
            Vector3 origin = new Vector3(.2f, .2f, .12f);
            Vector3 end = origin + new Vector3(_horizontalInput, _verticalInput * _camera.aspect) * 0.1f;
            Vector3 start = origin + new Vector3(_oldHorizontal, _oldVertical * _camera.aspect) * 0.1f;

            _lines.Add(end);
            _lines.Add(start);

            for (int i = 0; i < _lines.Count - 1; i += 2)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(_camera.ViewportToWorldPoint(_lines[i]), _camera.ViewportToWorldPoint(_lines[i + 1]));
            }
        }
#endif

    }

    #endregion

    #region METHODS

    private void StopTryJump()
    {
        _tryJump = false;
    }

    private void StopIsDash()
    {
        _isDash = false;
    }

    public void SetBehaviourFlags(byte behaviourFlags)
    {
        _behaviourFlags = behaviourFlags;

        if ((_behaviourFlags & MOVE) == MOVE)
        {
            _horizontalInput = 0f;
            _verticalInput = 0f;
        }
    }

    private void OnDisable()
    {
        _horizontalInput = 0f;
        _verticalInput = 0f;
        _tryJump = false;
        _isDash = false;
    }

    private float JoystickDampen(float val)
    {
        return Mathf.Min(Mathf.Pow(Mathf.Abs(val), math.E), 1.0f);
    }

    private void UpdateSound()
    {
        if (_audios.Count > 0)
        {
            if (_onGround)
                _audios[0].volume = Speed/_terminalVelocity;
            if (Speed > 75)
                _audios[1].volume = (Speed - 100f)/_terminalVelocity;
            else if (Speed <= 75)
                _audios[1].volume -= 0.001f;
        }
    }

    private IEnumerator PlayDriftParticles(Vector3 driftDirection, float delay)
    {
        yield return new WaitForSeconds(delay);

        if (_tryDrift && Speed > _driftSpeedThreshold)
        {
            _driftParticleController.PlayDriftParticles(_rb.velocity, driftDirection);
        }

        yield return null;
    }

    public void SwapCamera(Camera newCamera)
    {
        _camera = newCamera;
    }

    public void Dash()
    {
        Vector3 dashDirWhileStopped = (Vector3.ProjectOnPlane(_camera.transform.forward, Vector3.up).normalized + Vector3.up * 0.25f).normalized;
        _rb.AddForce(dashDirWhileStopped.normalized * _dashSpeed, ForceMode.Impulse);
    }

    public void UpdateTerminalVelocity(float value)
    {
        _terminalVelocity = value;
        _sqrterminalVelocity = _terminalVelocity * _terminalVelocity;
    }

    #endregion
}
