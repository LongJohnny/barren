using UnityEngine;
using System.Collections;
using System;
using Assets.Player.Scripts.Inventory;
using Assets.Player.Scripts;

[RequireComponent(typeof(PlayerMovementController), typeof(PlayerMaterialController), typeof(MeshRenderer))]
public class PlayerMainController : MonoBehaviour {

    public static PlayerMainController Instance = null;

    private PlayerMovementController _playerMovementController;
    private PlayerMaterialController _playerMaterialController;
    private PullForceController _pullForceController;
    private OrbitCollectiblesController _collectiblesController;
    
    [SerializeField]
    private ParticleSystem _dustParticles;

    [SerializeField]
    private ParticleSystem _teleportParticles;
    [SerializeField]
    private Material _teleportMaterial;
    [SerializeField]
    private MeshRenderer _meshRenderer;
    [SerializeField]
    private Rigidbody _rb;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            throw new Exception("Cannot have more than one instance of PlayerMainController");
        }
    }

    private void Start() {
        _playerMovementController = GetComponent<PlayerMovementController>();
        _playerMaterialController = GetComponent<PlayerMaterialController>();
        _pullForceController = GetComponent<PullForceController>();
        _collectiblesController = GetComponent<OrbitCollectiblesController>();
    }

    public Vector3 PlayerPosition
    {
        get => transform.position;
        set
        {
            transform.position = value;
        }
    }
    public Quaternion PlayerRotation => transform.rotation;
    public Vector3 PlayerVelocity => _rb.velocity;
    public float TerminalVelocity => _playerMovementController.TerminalVelocity;


    public void Jump()
    {
        _playerMovementController.ForceJump = true;
    }

    public void Dash()
    {
        //_playerMovementController.ForceDash = true;
        _playerMovementController.Dash();
    }

    public void ApplyForce(Vector3 force, ForceMode mode)
    {
        _rb.AddForce(force, mode);
    }

    public void ClearPlayerPhysics()
    {
        _rb.velocity = Vector3.zero;
        _rb.angularVelocity = Vector3.zero;
    }

    public void SwapPlayerMovementCamera(Camera newCamera)
    {
        _playerMovementController.SwapCamera(newCamera);
    }

    public void CollectCollectable(int number)
    {
        _collectiblesController.Collect(number);
    }

    public void DisablePlayer() {
        if (_playerMovementController.enabled && _playerMaterialController.enabled && _pullForceController.enabled) {
            _playerMovementController.enabled = false;
            _playerMaterialController.enabled = false;
            //_playerMaterialController.Clear();
            _pullForceController.enabled = false;
            _rb.isKinematic = true;
            _rb.detectCollisions = true;
            
            _dustParticles.Stop();
        }
    }

    public void EnablePlayer()
    {
        if (!_playerMovementController.enabled && !_playerMaterialController.enabled && !_pullForceController.enabled)
        {
            _playerMovementController.enabled = true;
            _playerMaterialController.enabled = true;
            _pullForceController.enabled = true;
            _rb.isKinematic = false;
            _rb.detectCollisions = true;
        }
    }

    public void DisablePlayerNotPhysics()
    {
        if (_playerMovementController.enabled && _playerMaterialController.enabled && _pullForceController.enabled)
        {
            _playerMovementController.enabled = false;
            _playerMaterialController.enabled = false;
            _pullForceController.enabled = false;
            _dustParticles.Stop();
        }
    }

    public void EnablePlayerNotPhysics()
    {
        if (!_playerMovementController.enabled && !_playerMaterialController.enabled && !_pullForceController.enabled)
        {
            _playerMovementController.enabled = true;
            _playerMaterialController.enabled = true;
            _pullForceController.enabled = true;
        }
    }


    public void DisablePlayerNotJump()
    {
        if (_playerMaterialController.enabled && _pullForceController.enabled)
        {
            _playerMovementController.SetBehaviourFlags(PlayerMovementController.DASH | PlayerMovementController.MOVE);
            _playerMaterialController.enabled = false;
            _pullForceController.enabled = false;
            _dustParticles.Stop();
        }
    }

    public void EnablePlayerNotJump()
    {
        if (!_playerMaterialController.enabled && !_pullForceController.enabled)
        {
            _playerMovementController.SetBehaviourFlags(0);
            _playerMaterialController.enabled = true;
            _pullForceController.enabled = true;
        }
    }

    public void TeleportPlayer(Action onAnimationEnd) {
        DisablePlayer();
        PlayTeleportAnimation(onAnimationEnd);
    }

    public void PlayTeleportAnimation(Action onAnimationEnd) {
        _teleportParticles.transform.position = transform.position;
        _rb.useGravity = false;
        _rb.velocity = Vector3.zero;
        _teleportParticles.Play();
        StartCoroutine(ShrinkAnimation(onAnimationEnd));
        _meshRenderer.material = _teleportMaterial;
    }

    public void ClearMovementConstraints()
    {
        _rb.constraints = RigidbodyConstraints.None;
        //TerminalVelocity = 100;
    }
    public void EnableMovementConstraints(Vector3 constraints)
    {
        if ((constraints.x != 0f && constraints.x !=1f ) || (constraints.y != 0f && constraints.y != 1f) || (constraints.z != 0f && constraints.z != 1f))
            throw new MissingFieldException("Constraints out of range, only 0s or 1s!");

        if (constraints.x == 1f)
        {
            _rb.constraints = RigidbodyConstraints.FreezePositionX;
        }
        if (constraints.y == 1f)
        {
            _rb.constraints = RigidbodyConstraints.FreezePositionY;
        }
        if (constraints.z == 1f)
        {
            _rb.constraints = RigidbodyConstraints.FreezePositionZ;
        }
    }

    public void ChangeTerminalVelocity(float velocity)
    {
        _playerMovementController.UpdateTerminalVelocity(velocity);
    }


    float scaleEpsilon = 0.05f;
    private IEnumerator ShrinkAnimation(Action onAnimationEnd) {
        while(transform.localScale.magnitude > scaleEpsilon) {
            transform.localScale = transform.localScale - transform.localScale*0.15f;
            yield return new WaitForSeconds(0.1f);
        }
        onAnimationEnd();
    }
}
