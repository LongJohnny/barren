using Assets.Overworld.Scripts;
using UnityEngine;

namespace Assets.Player.Scripts
{
    public class DustParticleController : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem _dustParticles;

        [SerializeField]
        [Tooltip("Position relative to the main character")]
        private Vector3 _position;

        [SerializeField]
        [Tooltip("Threshold from which the particles start emitting ")]
        private float _speedThreshold = 20.0f;

        [SerializeField]
        private float _maxEmissionRate = 500.0f;

        [SerializeField]
        [Tooltip("Controls the emission rate over time, higher values result in larger emission over time")]
        private float _rateOverTimeCoefficient = 12.0f;

        [SerializeField]
        private TerrainGenerator _terrainGenerator;

        private ParticleSystem.MainModule _particlesMainModule;

        void Start() {
            if (_dustParticles != null) {
                _dustParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);

                _particlesMainModule = _dustParticles.main;
            }
        }

        public void PlayDustParticles(Vector3 velocity, Vector3 planeNormal, Vector3 hitPoint) {

            if (_dustParticles!= null) {

                UpdateDustColor();
                float speed = velocity.magnitude;

                // Only play particles if the character's speed is above the threshold
                if (speed > _speedThreshold) {
                
                    _dustParticles.transform.position = transform.position - planeNormal*0.4f;
                    var main = _dustParticles.main;
                    var emission = _dustParticles.emission;

                    // The emission increases as the character's speed increases
                    emission.rateOverTime = Mathf.Clamp((speed - _speedThreshold) * _rateOverTimeCoefficient, 
                        0.0f, _maxEmissionRate);

                    // Particles always face the opposite direction of movement
                    _dustParticles.transform.rotation = Quaternion.LookRotation(-velocity.normalized, planeNormal);

                    if (!_dustParticles.isEmitting)
                        _dustParticles.Play();

                } else {
                    StopDustParticles();
                }
            }
        }

        private void UpdateDustColor()
        {
            if (_terrainGenerator != null)
            {
                Vector3 playerPos = transform.position;
                playerPos = _terrainGenerator.GetTerrain0().InverseTransformVector(playerPos);

                playerPos.y = 0;
                playerPos /= 30000f;

                Color color = _terrainGenerator.GetBlendMap(playerPos.x, playerPos.z);

                Vector3 rgb = new Vector3(color.r, color.g, color.b).normalized;
                //                                              PURPLE                                 ORANGE                                      TURQUOIS
                _particlesMainModule.startColor = rgb.x * new Color(0.6f, 0, 0.6f, 1) + rgb.y * new Color(0.08f, 0.7f, 0.7f, 1) + rgb.z * new Color(0.7f, 0.35f, 0.1f, 1);
            }
        }

        public void StopDustParticles() {
            if (_dustParticles!= null) {
                if (_dustParticles.isEmitting)
                    _dustParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }
    }
}
