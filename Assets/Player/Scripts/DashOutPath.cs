﻿using System.Collections;
using UnityEngine;

namespace Assets.Player.Scripts
{
    public class DashOutPath : MonoBehaviour
    {
        private LineRenderer _lineRenderer;

        private void Start()
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }

        public void UpdatePath(Vector3 direction)
        {
            Vector3 initialPosition = transform.position;
            Vector3 nextPosition = initialPosition + (direction.normalized + Vector3.down * 0.05f)*100f;

            Vector3[] positions = {initialPosition, nextPosition};

            _lineRenderer.SetPositions(positions);
        }
    }
}