﻿using Assets.Mechanics.CarryingEnergy.Scripts;
using UnityEngine;

namespace Assets.Player.Scripts.Inventory
{
    public class EnergyInventory : MonoBehaviour
    {
        [SerializeField] private Energy _energy;
        [SerializeField] private EnergyConfigurations _energyConfigurations;
        private EnergyDestination[] _energyDestinations;
        private EnergySource[] _energySources;

        // Use this for initialization
        private void Start()
        {
            _energyDestinations = FindObjectsOfType<EnergyDestination>();
            _energySources = FindObjectsOfType<EnergySource>();
        }

        // Update is called once per frame
        private void Update()
        {

            if (_energy != null && _energy.RanOut())
            {
                _energy = null;
            }

            if (_energy != null && !_energy.IsConsumed())
            {
                foreach (EnergyDestination energyDestination in _energyDestinations)
                {
                    if ((energyDestination.transform.position - transform.position).magnitude < _energyConfigurations.energyPlacementRadius
                        && energyDestination.PlaceEnergy(_energy))
                    {
                        _energy = null;
                        break;
                    }
                }

                if (_energy != null)
                {
                    foreach (EnergySource energy in _energySources)
                    {
                        if (energy != null && energy != _energy &&
                            (energy.transform.position - transform.position).magnitude <
                            _energyConfigurations.energyGrabRadius)
                        {
                            _energy.ResetToMax();
                            break;
                        }
                    }
                }
            } 
            else
            {
                Energy energy;
                foreach (EnergySource energySource in _energySources)
                {
                    if (energySource != null && (energySource.transform.position - transform.position).magnitude < _energyConfigurations.energyGrabRadius
                                             && (energy = energySource.GrabEnergy()) != null
                       )
                    {
                        _energy = energy;
                        break;
                    }
                }
            }
        }
    }
}