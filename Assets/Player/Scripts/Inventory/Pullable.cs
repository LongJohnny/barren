﻿using System.Collections;
using UnityEngine;

namespace Assets.Player.Scripts.Inventory
{
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    public class Pullable : MonoBehaviour
    {   
        private PullForceController _playerInventory;
        private Rigidbody _rigidbody;

        public bool CanBePickedUp = true;

        private void Start()
        {
            _playerInventory = FindObjectOfType<PullForceController>();
            if (_playerInventory == null) throw new MissingReferenceException("There is no player inventory in the scene!");
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            if (CanBePickedUp && _playerInventory.PickingUpObjects)
                MoveTowardsInventory();
        }

        private void MoveTowardsInventory()
        {
            _rigidbody.AddForce(_playerInventory.PullForce(_rigidbody));
        }
    }
}