﻿using System.Collections;
using Assets.Common.Scripts;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Player.Scripts.Inventory
{
   [RequireComponent(typeof(Rigidbody))]
    public class PullForceController : MonoBehaviour
    {
        [SerializeField] private float _gravitationalConstant = 10.0f;
        [SerializeField] private float _radius = 50.0f;
        [SerializeField] private ParticleSystem _pullParticles;
        private Rigidbody _rigidbody;
        public bool PickingUpObjects => _pickingUpObjects;

        private Controls _controls;
        private InputAction _pickUpObject;
        private InputAction _releaseObject;
        private bool _pickingUpObjects = false;


        private void Awake() {
            _controls = new Controls();
            _pickUpObject = _controls.Interaction.PickUpObject;
            _releaseObject = _controls.Interaction.ReleaseObject;
            _pickUpObject.Enable();
            _releaseObject.Enable();
        }

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            if (_pickUpObject.triggered && !_pickingUpObjects)
            {
                _pickingUpObjects = true;
                _pullParticles.Play();
            }
            else if (_releaseObject.triggered && _pickingUpObjects)
            {
                _pickingUpObjects = false;
                _pullParticles.Stop();
            }
        }

        /// <summary>
        /// Calculates a Pull force similar to the gravitational force
        /// </summary>
        public Vector3 PullForce(Rigidbody pickable)
        {
            Vector3 direction = transform.transform.position - pickable.position;

            float distance = direction.magnitude;

            if (distance > _radius) return Vector3.zero;

            float force = _gravitationalConstant * ((_rigidbody.mass * pickable.mass) / distance);

            float x = Mathf.Min(_radius, distance) / _radius;

            //Debug.Log(x);

            x = Mathf.Clamp(Mathf.Pow(x * 3f, 5f), 0f, 1f) * .5f;

            //Mathf.Clamp(Mathf.Pow(x * 2f, 5f), 0f, 1f);

            //Debug.Log(x);

            //Debug.Log(distance);

            return direction.normalized * force * x;
        }
    }
}