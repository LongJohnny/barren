﻿using System.Collections;
using UnityEngine;
using static UnityEngine.Mathf;
using static Den.Tools.Vector2D;

namespace Den.Tools
{
    public static class BarrenExtensions
    {

        public static float Fract(float x) => x - Floor(x);
        public static Vector2D Floor2(Vector2D p) => new Vector2D(Floor(p.x), Floor(p.z));
        public static Vector2D Fract2(Vector2D p) => new Vector2D(Fract(p.x), Fract(p.z));

        public static Vector2D Cos2(Vector2D p) => new Vector2D(Cos(p.x), Cos(p.z));
        public static Vector2D Sin2(Vector2D p) => new Vector2D(Sin(p.x), Sin(p.z));

        public static float Bilerp(float a, float b, float c, float d, float k)
        {
            return Lerp(Lerp(a, b, k), Lerp(c, d, k), k);
        }

        public static float Noise(Vector2D p) => PerlinNoise(p.x, p.z);
        public static float Ridge(Vector2D p) => 1f - Abs(PerlinNoise(p.x, p.z));

        public static float Smin(float a, float b, float k)
        {
            float x = Exp(-k * a);
            float y = Exp(-k * b);
            return (a * x + b * y) / (x + y);
        }

        public static float Smax2(float a, float b, float k)
        {
            return Smin(a, b, -k);
        }

        public static float Smax(float a, float b, float k)
        {
            float h = Clamp(0.5f + 0.5f * (b - a) / k, 0.0f, 1.0f);
            return Lerp(b, a, h) - k * h * (1.0f - h);
        }

        public static float Rand(Vector2D p)
        {
            return Fract(Sin(Dot(p, new Vector2D(12.9898f, 78.233f)) * 43758.5453123f));
        }

        public static float Rand(float x)
        {
            return Fract(Sin(x) * 43758.5453123f);
        }

        public static Vector2D Rand2(Vector2D p)
        {
            Vector2D a = new Vector2D(Dot(p, new Vector2D(127.1f, 311.7f)), Dot(p, new Vector2D(269.5f, 183.3f)));
            return new Vector2D(Rand(a.x), Rand(a.z));
        }

        public static float SmoothStep(float edgeA, float edgeB, float x)
        {
            float p = Clamp((x - edgeA) / (edgeB - edgeA), 0.0f, 1.0f);
            float v = p * p * (3.0f - 2.0f * p);
            return v;
        }

        public static float Voronoi(Vector2D p)
        {
            Vector2D i = Floor2(p);
            Vector2D f = Fract2(p);

            Vector2D nb, rp, b;
            float min = 0f;
            float sm = 0f;
            float dist = 1f;
            for (int _i = -1; _i <= 1; ++_i)
            {
                for (int j = -1; j <= 1; ++j)
                {
                    nb.x = _i;
                    nb.z = j;

                    rp = Rand2(Floor2(i + nb));

                    dist = Min(dist, (f - (nb + rp)).Magnitude);

                    //min = (f - (nb + rp)).magnitude; 

                    /*if (min < 0.1f)
                    {
                        dist = Min(dist, 0.1f);
                    } 
                    else if (min < 0.2f)
                    {
                        float k = (min - 0.1f) / 0.1f;

                        dist = Min(dist, Lerp(0.1f, min, k));
                    } 
                    else
                    {
                        dist = Min(dist, min);
                    }*/

                }
            }

            return dist;
        }

        public static float DomainWarping(Vector2D p)
        {

            Vector2D a = new Vector2D(
                Noise(p),
                Noise(p + new Vector2D(2f, 2f)));

            Vector2D b = new Vector2D(
                Noise(Cos2(a * .2f) + new Vector2D(1.7f, 9.2f)),
                Noise(a * 2f + new Vector2D(8.3f, 2.8f)) * 2f);

            // .5f to ensure this is between 0 and 1
            return Noise(p + b * 4f);
        }

        public static float DomainWarping2(Vector2D p)
        {

            float val = 0f;
            for (int i = 0; i < 4; ++i)
            {
                val = Noise(p + new Vector2D(val, val));
            }
            return val;
        }

        public static float WavesCircle(Vector2D p, float f)
        {

            float d = (p - new Vector2D(.5f, .5f)).Magnitude;

            return 1f - Abs(-Cos(d * f));
        }
    }
}