﻿using UnityEngine;
using Assets.Common.Scripts;
using System.Threading.Tasks;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Assets.Overworld.Scripts
{
    public class TerrainGenerator : MonoBehaviour
    {
        [SerializeField] private TerrainData[] _terrains;
        [SerializeField] private int _lowPassFilterPasses = 0;
        [SerializeField] private Texture2D _blendMap;

        public void GenerateTerrain()
        {
            int singleTerrainWidth = _terrains[0].heightmapResolution;
            int singleTerrainHeight = _terrains[0].heightmapResolution;

            int size = (int)Mathf.Sqrt(_terrains.Length);

            int blendMapWidth = singleTerrainWidth * size;
            int blendMapHeight = singleTerrainHeight * size;

            Vector3[,] blendMapProcessed = new Vector3[blendMapWidth, blendMapHeight];

            for (int y = 0; y < blendMapHeight; ++y)
            {
                for (int x = 0; x < blendMapWidth; ++x)
                {
                    Vector2 p = new Vector2(x / ((float)blendMapWidth), y / ((float)blendMapHeight));

                    Color pixel = _blendMap.GetPixelBilinear(p.x, p.y);

                    float sum = pixel.r + pixel.g + pixel.b;

                    blendMapProcessed[x, y] = new Vector3(pixel.r / sum, pixel.g / sum, pixel.b / sum);
                }
            }

            float[][,] heightMaps = new float[_terrains.Length][,];

            Parallel.For(0, size, yt =>
            {
                Parallel.For(0, size, xt => 
                {
                    int startY = yt * (singleTerrainHeight - 1);
                    int endY = startY + singleTerrainHeight;

                    int startX = xt * (singleTerrainWidth - 1);
                    int endX = startX + singleTerrainWidth;

                    float[,] heightMap = new float[singleTerrainWidth, singleTerrainHeight];

                    Parallel.For(startY, endY, y =>
                    {
                        Parallel.For(startX, endX, x =>
                        {
                            Vector2 p = new Vector2(x / ((float)blendMapWidth), y / ((float)blendMapHeight));

                            Vector3 pixel = blendMapProcessed[x, y];

                            float hm0 = MathAux.DomainWarping(p * size) * .5f;

                            float hm1 = MathAux.Voronoi(p * 100f * size) * .07f + 
                                        MathAux.Voronoi(p * 20f * size) * .25f;

                            float hm2 = MathAux.WavesCircle(p * size, 12f) * .25f +
                                        MathAux.DomainWarping(p * 20f * size) * .06f;

                            float hm3 = MathAux.Noise(p * 66f * size) * .12f +
                                        MathAux.Noise(p * 44f * size) * .12f +
                                        MathAux.Noise(p * 22f * size) * .10f +
                                        MathAux.Noise(p * 11f * size) * .05f;

                            Vector3 hmaps = new Vector3(hm1 + hm0, hm2 + hm0, hm3 + hm0);

                            heightMap[y - startY, x - startX] = Vector3.Dot(hmaps, pixel);
                        });
                    });

                    if (_lowPassFilterPasses > 0)
                    {
                        for (int i = 0; i < _lowPassFilterPasses; ++i)
                        {
                            float[,] heightMap2 = new float[singleTerrainWidth, singleTerrainHeight];
                            Parallel.For(0, singleTerrainHeight, y =>
                            {
                                Parallel.For(0, singleTerrainWidth, x =>
                                {
                                    Vector2 p = new Vector2(x / ((float)singleTerrainWidth), y / ((float)singleTerrainHeight));

                                    float average = 0.0f;
                                    int quocient = 0;
                                    for (int i = -1; i <= 1; ++i)
                                    {
                                        for (int j = -1; j <= 1; ++j)
                                        {
                                            ++quocient;

                                            int x1 = x + j;
                                            int y1 = y + i;

                                            if (x1 < 0 || x1 >= singleTerrainWidth)
                                            {
                                                x1 = x;
                                            }

                                            if (y1 < 0 || y1 >= singleTerrainHeight)
                                            {
                                                y1 = y;
                                            }

                                            average += heightMap[x1, y1];
                                        }

                                    }

                                    heightMap2[x, y] = average / quocient;


                                });
                            });
                            heightMap = heightMap2;
                        }
                    }

                    heightMaps[xt + yt * size] = heightMap;
                });
            });

            for (int i = 0; i < size*size; i++)
            {
                _terrains[i].SetHeights(0, 0, heightMaps[i]);
            }


        }
        public void GenerateTerrain2()
        {
            int singleTerrainWidth = _terrains[0].heightmapResolution;
            int singleTerrainHeight = _terrains[0].heightmapResolution;

            int size = (int)Mathf.Sqrt(_terrains.Length);

            int blendMapWidth = singleTerrainWidth * size;
            int blendMapHeight = singleTerrainHeight * size;

            Vector3[,] blendMapProcessed = new Vector3[blendMapWidth, blendMapHeight];

            /*for (int y = 0; y < blendMapHeight; ++y)
            {
                for (int x = 0; x < blendMapWidth; ++x)
                {
                    Vector2 p = new Vector2(x / ((float)blendMapWidth), y / ((float)blendMapHeight));

                    Color pixel = blendMap.GetPixelBilinear(p.x, p.y);

                    float sum = pixel.r + pixel.g + pixel.b;

                    blendMapProcessed[x, y] = new Vector3(pixel.r / sum, pixel.g / sum, pixel.b / sum);
                }
            }*/

            float[][,] heightMaps = new float[9][,];

            Parallel.For(0, 1, yt =>
            {
                Parallel.For(0, 1, xt =>
                {
                    int startY = yt * (singleTerrainHeight - 1);
                    int endY = startY + singleTerrainHeight;

                    int startX = xt * (singleTerrainWidth - 1);
                    int endX = startX + singleTerrainWidth;

                    float[,] heightMap = new float[singleTerrainWidth, singleTerrainHeight];

                    Parallel.For(startY, 12288, y =>
                    {
                        Parallel.For(startX, 12288, x =>
                        {
                            Vector2 p = new Vector2(x / ((float)blendMapWidth), y / ((float)blendMapHeight));

                            Vector3 pixel = blendMapProcessed[x, y];

                            //float hm0 = MathAux.DomainWarping(p * size) * .8f;

                            float hm1 = MathAux.Voronoi(p * 100f * size);
                            //+ MathAux.Voronoi(p * 20f * size) * .25f;

                            /*float hm2 = MathAux.WavesCircle(p * size, 12f) * .3f +
                                        MathAux.DomainWarping(p * 25f * size) * .07f;*/

                            /*float hm3 = MathAux.Noise(p * 66f * size) * .12f +
                                        MathAux.Noise(p * 44f * size) * .12f +
                                        MathAux.Noise(p * 22f * size) * .10f +
                                        MathAux.Noise(p * 11f * size) * .05f;*/

                            //Vector3 hmaps = new Vector3(hm1 + hm0, hm2 + hm0, hm3 + hm0);

                            heightMap[(int)(p.y * singleTerrainWidth), (int)(p.x * singleTerrainWidth)] = hm1;
                            //heightMap[y - startY, x - startX] = Vector3.Dot(hmaps, pixel);
                            //heightMap[x, y] = hm1;
                        });
                    });

                    if (_lowPassFilterPasses > 0)
                    {
                        for (int i = 0; i < _lowPassFilterPasses; ++i)
                        {
                            float[,] heightMap2 = new float[singleTerrainWidth, singleTerrainHeight];
                            Parallel.For(0, singleTerrainHeight, y =>
                            {
                                Parallel.For(0, singleTerrainWidth, x =>
                                {
                                    Vector2 p = new Vector2(x / ((float)singleTerrainWidth), y / ((float)singleTerrainHeight));

                                    float average = 0.0f;
                                    int quocient = 0;
                                    for (int i = -1; i <= 1; ++i)
                                    {
                                        for (int j = -1; j <= 1; ++j)
                                        {
                                            ++quocient;

                                            int x1 = x + j;
                                            int y1 = y + i;

                                            if (x1 < 0 || x1 >= singleTerrainWidth)
                                            {
                                                x1 = x;
                                            }

                                            if (y1 < 0 || y1 >= singleTerrainHeight)
                                            {
                                                y1 = y;
                                            }

                                            average += heightMap[x1, y1];
                                        }

                                    }

                                    heightMap2[x, y] = average / quocient;


                                });
                            });
                            heightMap = heightMap2;
                        }
                    }

                    heightMaps[xt + yt * size] = heightMap;
                });
            });

            for (int i = 0; i < size * size; i++)
            {
                _terrains[i].SetHeights(0, 0, heightMaps[i]);
            }


        }
        public Transform GetTerrain0()
        {
            return transform.GetChild(0);
        }
        public Color GetBlendMap(float uvX, float uvY)
        {
            return _blendMap.GetPixelBilinear(uvX, uvY);
        }
        public TerrainData[] GetTerrains()
        {
            return _terrains;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(TerrainGenerator))]
    public class TerrainGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            TerrainGenerator terrainGenerator = target as TerrainGenerator;
            if (terrainGenerator != null && GUILayout.Button("Generate terrain"))
            {
                terrainGenerator.GenerateTerrain();
            }

        }
    }
#endif

}