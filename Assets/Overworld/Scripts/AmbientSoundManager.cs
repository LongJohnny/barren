﻿using System.Collections;
using UnityEngine;

namespace Assets.Overworld.Scripts
{
    public class AmbientSoundManager : MonoBehaviour
    {
        [SerializeField] private AudioSource _ambientAudioSource;
        [SerializeField] private AudioClip[] _ambientSounds;

        private void Start()
        {
            ChangeAmbientSound(0);
        }


        public void ChangeAmbientSound(int ambient)
        {
            _ambientAudioSource.Stop();
            _ambientAudioSource.clip = _ambientSounds[ambient];
            _ambientAudioSource.Play();
        }
    }
}