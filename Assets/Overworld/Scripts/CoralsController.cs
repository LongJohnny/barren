﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Overworld.Scripts
{
    public class CoralsController : MonoBehaviour
    {
        private Transform _movementController;
        [SerializeField] private float _viewDistance = 100f;

        private void Start()
        {
            _movementController = FindObjectOfType<PlayerMovementController>().transform;
            if (_movementController == null)
            {
                throw new MissingReferenceException("CoralsController requires the PlayerMovementController to be in scene to work!");
            }
        }

        // Update is called once per frame
        void Update()
        {
            foreach (Transform child in transform)
            {
                if ((child.position - _movementController.position).magnitude > _viewDistance) {
                    child.gameObject.SetActive(false);
                } 
                else
                {
                    child.gameObject.SetActive(true);
                }
            }
        }
    }
}