﻿using System;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Overworld.Scripts
{
    public class CoralPopulator : MonoBehaviour
    {
        [SerializeField] private GameObject[] _coralPrefabs;
        [SerializeField] private uint _coralDensity;
        [SerializeField] private TerrainGenerator _terrainGenerator;

        public void PopulateWithCorals()
        {
            TerrainData[] terrains = _terrainGenerator.GetTerrains();

            int singleTerrainWidth = terrains[0].heightmapResolution;
            int singleTerrainHeight = terrains[0].heightmapResolution;

            int size = (int)Mathf.Sqrt(terrains.Length);
            float sizef = Mathf.Sqrt(terrains.Length);

            for (int yt = 0; yt < size; ++yt)
            for (int xt = 0; xt < size; ++xt)
            for (float y = 0.0f; y < 1f; y += 0.001f)
            for (float x = 0.0f; x < 1f; x += 0.001f)
            {
                Color color = _terrainGenerator.GetBlendMap(((float)xt + x)/sizef, ((float) yt + y)/sizef);

                if (color.r > 0.7f)
                {
                    int terrainIdx = xt + yt * size;

                    float steepness = terrains[terrainIdx].GetSteepness(x, y);
                    if (steepness > 0.1 || steepness < -0.1)
                    {
                        if (Random.Range(0f, 1f) > 0.999f)
                        {
                            float height = terrains[terrainIdx].GetInterpolatedHeight(x, y);

                            var coral = Instantiate(_coralPrefabs[0], transform);
                            coral.transform.position =
                                new Vector3(((float)xt + x) * 10000f, height, ((float)yt + y) * 10000f);
                            coral.transform.localScale *= Random.Range(0.5f, 0.8f);

                            Vector3 normal = terrains[terrainIdx].GetInterpolatedNormal(x, y);

                            Vector3 axis = Vector3.Cross(coral.transform.up, normal);
                            float angle = Mathf.Asin(axis.magnitude);

                            coral.transform.rotation *= Quaternion.AngleAxis(Mathf.Rad2Deg * angle, axis);
                        }
                    }
                }
            }
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(CoralPopulator))]
    public class CoralPopulatorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            CoralPopulator coralPopulator = target as CoralPopulator;
            if (coralPopulator != null && GUILayout.Button("Populate with corals"))
            {
                coralPopulator.PopulateWithCorals();
            }

        }
    }
#endif

}