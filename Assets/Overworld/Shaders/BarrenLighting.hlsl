#ifndef CUSTOM_LIGHTING_INCLUDED
#define CUSTOM_LIGHTING_INCLUDED

// This is a neat trick to work around a bug in the shader graph when
// enabling shadow keywords. Created by @cyanilux
// https://github.com/Cyanilux/URP_ShaderGraphCustomLighting
#ifndef SHADERGRAPH_PREVIEW
	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
	#if (SHADERPASS != SHADERPASS_FORWARD)
		#undef REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR
	#endif
#endif

float white_noise(float2 st) {
    return frac(sin(dot(st,
        float2(12.9898, 78.233))) *
        43758.5453123);
}

struct SurfData {

    float3 albedo;

    // Normal in world space
    float3 normal;

    // Position in world space
    float3 position;

    // Texture coordinates
    float2 uv;

    // From the fragment to the camera
    float3 viewDirection;

    // Specifies the intensity at which surface is affected by fog
    float fogIntensity;

    float3 random;

    float3 random2;

    float noise;

    float3 lightPosition;

    float3 blendMap;
};

float3 ApplyFog(float3 position, float3 color, float fogIntensity)
{
#ifdef SHADERGRAPH_PREVIEW
    return color;
#else
    float fogFactor = saturate(ComputeFogFactor(TransformWorldToHClip(position).z) + (1. - fogIntensity));
    return color * fogFactor;
#endif
}

float4 GetShadowUVCoords(float3 position)
{
#ifdef SHADERGRAPH_PREVIEW
    // In preview, there's no shadows or bakedGI
    return 0;
#else
    // Calculate the main light shadow coords
    // There are two types depending on if cascades are enabled
    float4 positionCS = TransformWorldToHClip(position);
#if SHADOWS_SCREEN
    return  ComputeScreenPos(positionCS);
#else
    return TransformWorldToShadowCoord(position);
#endif
#endif
}

#define NIGHT_LIGHT 0.3

float3 PerlinLighting(SurfData sd, Light l)
{

    //l.direction = float3(0.0, -1.0, 0.0);

    sd.albedo = lerp(float3(0.27, 0.07, 0.5), float3(1., 0.1, 0.1), smoothstep(150, 250, sd.position.y));
    sd.albedo = lerp(sd.albedo, float3(1., .9, .6) * 2., smoothstep(270, 390, sd.position.y));

    float3 normal = sd.normal;
    normal.xz += (sd.random.xz * 2. - 1.) * 0.5;

    //float wnoise = pow(sd.random.x, 7.);
    //wnoise = pow(wnoise, .1); // Biasing to have more values of one

    //float3 normal = sd.normal;
    //normal.xz -= wnoise*.4;
    normal.y *= 0.3;
    float dif = 4. * max(0.0, dot(normal, l.direction));

    float3 hlf = normalize(sd.viewDirection + l.direction);
    float spec = pow(max(0.0, (dot(hlf, normal))), 8.0);

    // Adapted fresnel term
    float fre = pow(1. - saturate(dot(sd.viewDirection, normal)), 5.0);

    //float ndif = -min(0.0, dot(sd.normal, l.direction));
    float glitter = max(0, dot(sd.random2 * 2. - 1., float3(0.0, 1.0, 0.0)));
    glitter = step(0.99999, glitter) * 5.0;

    //float3 color = float3(1., 0.1, 0.1) * (dif * .9 + fre * 7. + spec * 5. + .3) * l.color * .1;

    float3 color = sd.albedo * l.color * dif;
    color += sd.albedo * l.color * spec * 5.;
    color += sd.albedo * l.color * fre;
    color += sd.albedo * l.color * glitter * 20.;
    color += sd.albedo * l.color * .1;

    return color * .6 * (l.distanceAttenuation * l.shadowAttenuation);
}

float3 VoronoiLighting(SurfData sd, Light l)
{
    //l.direction = normalize(sd.lightPosition - sd.position);

    sd.albedo = lerp(float3(76. / 255., 39. / 255., 66. / 255.) * .5, float3(255. / 255., 255. / 255., 255. / 255.) * 1.5, smoothstep(220, 390, sd.position.y));
    //sd.albedo = lerp(sd.albedo, float3(240. / 255., 241. / 255., 242. / 255.) * 2., smoothstep(270, 390, sd.position.y));

    sd.normal.y *= 2.;
    float3 normal = normalize(lerp(sd.normal, float3(0.0, 1.0, 0.0), .2));
    //normal.xz += (sd.random.xz * 2. - 1.) * 0.5;

    //float wnoise = pow(sd.random.x, 7.);
    //wnoise = pow(wnoise, .1); // Biasing to have more values of one

    //float3 normal = sd.normal;
    //normal.xz -= wnoise*.4;
    //normal.y *= 0.3;
    //normal.xz *= 0.3;
    float dif = max(0.0, dot(normal, l.direction));

    float3 hlf = normalize(sd.viewDirection + l.direction);
    float spec = pow(max(0.0, (dot(hlf, normal))), 2.0);

    // Adapted fresnel term
    float fre = pow(1. - saturate(dot(sd.viewDirection, normal)), 8.0);

    //float3 color = float3(1., 0.1, 0.1) * (dif * .9 + fre * 7. + spec * 5. + .3) * l.color * .1;

    float3 color = sd.albedo * l.color * dif;
    color += sd.albedo * l.color * spec * .4;
    color += float3(240. / 255., 241. / 255., 242. / 255.) * fre * dif * 2.5;
    //color += sd.albedo * l.color * glitter * 20.;
    color += sd.albedo * l.color * .2;

    return color * .6 * (l.distanceAttenuation * l.shadowAttenuation);
}

float3 DomainWarpingLighting(SurfData sd, Light l)
{
    //l.direction = normalize(sd.lightPosition - sd.position);

    float y = 0.0;
    sd.albedo = float3(0.0, 0.5, 1.0);
    while(y < 600)
    {
        float py = y;
        y += 7. + frac(sin(y * 10202.) * 40341.);
        sd.albedo = lerp(sd.albedo, 
            float3(
                frac(y * 123.) * 2.,
				frac(y * .1), frac(y * .1)), 
				smoothstep(py + (sd.random2.y * 2. - 1.) * 4, y + (sd.random2.y * 2. - 1.) * 4., sd.position.y));
    }


    float3 normal = sd.normal;
    //normal.xz += (sd.random.xz * 2. - 1.) * 0.5;

    //float wnoise = pow(sd.random.x, 7.);
    //wnoise = pow(wnoise, .1); // Biasing to have more values of one

    //float3 normal = sd.normal;
    //normal.xz -= wnoise*.4;
    normal.y *= 0.6;
    //normal.xz *= 0.3;
    float dif = max(0.0, dot(normal, l.direction));

    float3 hlf = normalize(sd.viewDirection + l.direction);
    float spec = pow(max(0.0, (dot(hlf, normal))), 2.0);

    // Adapted fresnel term
    float fre = pow(1. - saturate(dot(sd.viewDirection, normal)), 8.0);

    //float3 color = float3(1., 0.1, 0.1) * (dif * .9 + fre * 7. + spec * 5. + .3) * l.color * .1;

    float3 color = sd.albedo * l.color * dif;
    //color += sd.albedo * l.color * spec * .4;
    //color += float3(240. / 255., 241. / 255., 242. / 255.) * fre * dif * 2.5;
    //color += sd.albedo * l.color * glitter * 20.;
    color += sd.albedo * l.color * .2;

    return color * (l.distanceAttenuation * l.shadowAttenuation);
}

float3 LightingFromBlendMap(SurfData sd, Light l) {

    float3 blend = sd.blendMap;

    return blend.r * VoronoiLighting(sd, l) + blend.g * DomainWarpingLighting(sd, l) + blend.b * PerlinLighting(sd, l);
}

float3 Lighting(SurfData sd) {

#ifdef SHADERGRAPH_PREVIEW

    // In the shadergraph preview window there are no lights therefore we have to simulate one
    float3 dir = float3(0.0, 0.5, 0.5);

    float dif = saturate(dot(dir, sd.normal));

    return sd.albedo * dif;

#else

    Light mainLight = GetMainLight(GetShadowUVCoords(sd.position), sd.position, 1);
    mainLight.shadowAttenuation += 0.5;
    mainLight.shadowAttenuation = clamp(mainLight.shadowAttenuation, 0.0, 1.0);

    float3 color = LightingFromBlendMap(sd, mainLight);

#ifdef _ADDITIONAL_LIGHTS
    // Shade additional cone and point lights. Functions in URP/ShaderLibrary/Lighting.hlsl
    uint numAdditionalLights = GetAdditionalLightsCount();
    for (uint lightI = 0; lightI < numAdditionalLights; lightI++) {
        Light light = GetAdditionalLight(lightI, sd.position, 1);
        color += LightingFromBlendMap(sd, light);
    }
#endif

    return ApplyFog(sd.position, color, 0.7);

#endif

}

void BarrenLighting_float(float3 Albedo, float3 Normal, float3 Position, float2  UV, float3 ViewDirection, float FogIntensity, float3 Random, float3 Random2, float Noise, float3 LightPosition, float3 BlendMap,
    out float3 Color) {

    SurfData sd;
    sd.albedo = Albedo;
    sd.normal = Normal;
    sd.position = Position;
    sd.uv = UV;
    sd.viewDirection = normalize(ViewDirection); // THIS CRAP DOES NOT COME NORMALIZED
    sd.fogIntensity = FogIntensity;
    sd.random = Random;
    sd.random2 = Random2;
    sd.noise = Noise;
    sd.lightPosition = LightPosition;
    sd.blendMap = BlendMap;

    Color = Lighting(sd);
}

#endif