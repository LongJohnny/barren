﻿using System;
using Assets.Common.Scripts;
using Assets.Mechanics;
using CameraShake;
using UnityEngine;

namespace Assets.Landmarks.Scripts
{
    public class LandmarkController : Activity
    {
        [SerializeField] private Transform _rotatingPart;
        [SerializeField] private float _rotationSpeed = 100.0f;
        private float _currRotation = 0.0f;
        private float _targetRotation = 0.0f;
        private float _rotationDir = 1.0f;
        private float _currTargetRotationDiff = 0.0f;

        [SerializeField] private Transform _place;
        [SerializeField] private bool _working = true;
        [SerializeField] private AudioSource _startWorkingSound;
        public bool Rotating => _rotating;
        private bool _rotating = false;

        private void Start()
        {
            if (_rotatingPart == null)
            {
                _rotatingPart = this.transform;
            }
        }

        public float GetRotation()
        {
            return _currRotation;
        }

        public void SetRotation(float rotation)
        {   
            _currRotation = rotation;
            _targetRotation = rotation;
            _rotatingPart.rotation = Quaternion.Euler(Vector3.up * rotation);
        }

        public void StartWorking()
        {
            _working = true;
            if (_place != null)
                PointToPlace(_place);
            _startWorkingSound.Play();
        }

        public void StopPointingToPlace(Transform place)
        {
            if (_place == place) _place = null;
        }

        public void PointToPlace(Transform place)
        {
            if (_working)
            {
                _place = place;

                Vector3 currentOrientation = Vector3.ProjectOnPlane(-_rotatingPart.forward, Vector3.up).normalized;
                Vector3 targetOrientation = Vector3.ProjectOnPlane(place.position - _rotatingPart.position, Vector3.up).normalized;

                float rotation = Mathf.Acos(Vector3.Dot(currentOrientation, targetOrientation)) * Mathf.Rad2Deg;
                float rotationDir = Mathf.Sign(Vector3.Cross(currentOrientation, targetOrientation).y);

                _rotationDir = rotationDir;
                _targetRotation = _currRotation + rotation * rotationDir;

                _currTargetRotationDiff = Mathf.Abs(_targetRotation - _currRotation);
            }
        }

        public void PointToPlaceInstantly(Transform place)
        {
            if (_working)
            {
                _place = place;

                Vector3 currentOrientation = Vector3.ProjectOnPlane(-_rotatingPart.forward, Vector3.up).normalized;
                Vector3 targetOrientation = Vector3.ProjectOnPlane(place.position - _rotatingPart.position, Vector3.up).normalized;

                float rotation = Mathf.Acos(Vector3.Dot(currentOrientation, targetOrientation)) * Mathf.Rad2Deg;
                float rotationDir = Mathf.Sign(Vector3.Cross(currentOrientation, targetOrientation).y);

                _rotationDir = rotationDir;
                _targetRotation = _currRotation + rotation * rotationDir;
                _currRotation = _targetRotation;

                _rotatingPart.rotation = Quaternion.Euler(Vector3.up * _currRotation);
            }
        }
   
        private void Update()
        { 
            if (Math.Abs(_currRotation - _targetRotation) > float.Epsilon)
            {
                _rotating = true;

                float rotationDiff = Mathf.Abs(_targetRotation - _currRotation);
                float s = 1f - rotationDiff / _currTargetRotationDiff;

                CameraShaker.Instance.ShakePresets.ShortShake3D(0.1f * (1f - s), 40f, 1);

                //Debug.Log(s);
                s *= .8f;
                s += 0.2f;
                //s = Mathf.Min(1f, s);
                //s = Mathf.Sin(s * 1.2f * Mathf.PI - Mathf.PI * .5f * .2f) * .5f - .5f;
                //s = MathAux.SmoothStep(-0.2f, 0.9f, s);
                //s = Mathf.Cos(2.5f * s - 1f);

                float rotation = _rotationSpeed * s * _rotationDir * Time.deltaTime;
                _currRotation += rotation;

                float k = _rotationDir * .5f + .5f;
                _currRotation = k * 
                    Mathf.Min(_currRotation, _targetRotation) + (1f - k) * 
                    Mathf.Max(_currRotation, _targetRotation);

                _rotatingPart.rotation = Quaternion.Euler(Vector3.up * _currRotation);
            
            }
            else
            {
                _rotating = false;
            }

        }

        public bool IsWorking() => _working;

        public override bool IsCompleted()
        {
            return _working;
        }

        public override void CompleteActivity()
        {
            _working = true;
        }
    }
}