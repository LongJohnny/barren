﻿using Assets.Common.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Landmarks.Scripts
{
    public class LandmarksManager : MonoBehaviour
    {
        public static LandmarksManager Instance;

        private LandmarkController[] _landmarkControllers;

        private RemovableStack<Transform> _pointingTargets = new RemovableStack<Transform>();

        private Transform _currPointingTarget = null;

        private void Start()
        {
            if (Instance != null)
            {
                throw new DuplicateSingletonException("Cannot have more than one LandMarkManager in the scene!");
            }

            Instance = this;

            _landmarkControllers = FindObjectsOfType<LandmarkController>();

            if (_landmarkControllers.Length < 2) throw new MissingReferenceException("There are less than 2 landmarks!");

            if (_landmarkControllers.Length > 2) throw new MissingReferenceException("There are more than 2 landmarks!");
        }

        private void Update()
        {
            if (_landmarkControllers[0].Rotating || _landmarkControllers[1].Rotating) return;

            if (_pointingTargets.Count == 0) return;

            _currPointingTarget = _pointingTargets.Pop();

            foreach (LandmarkController landmarkController in _landmarkControllers)
            {
                landmarkController.PointToPlace(_currPointingTarget);
            }
        }

        public bool LandmarksAreWorking() => _landmarkControllers[0].IsWorking() && _landmarkControllers[1].IsWorking();


        public void PointToTransform(Transform place)
        {
            if (!_pointingTargets.Contains(place))
            {
                _pointingTargets.Push(place);
            }
        }

        public void PointToTransformInstantly(Transform place)
        {
            foreach (LandmarkController landmarkController in _landmarkControllers)
            {
                landmarkController.PointToPlaceInstantly(place);
            }
        }

        public void StopPointingToTransform(Transform place)
        {
            if (_pointingTargets.Contains(place))
            {
                _pointingTargets.Remove(place);
            }

            if (place == _currPointingTarget)
            {
                _currPointingTarget = null;
                foreach (LandmarkController landmarkController in _landmarkControllers)
                {
                    landmarkController.StopPointingToPlace(_currPointingTarget);
                }
            }
        }

    }
}