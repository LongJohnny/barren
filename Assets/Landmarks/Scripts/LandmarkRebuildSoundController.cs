﻿using System.Collections;
using UnityEngine;

namespace Assets.Landmarks.Scripts
{
    public class LandmarkRebuildSoundController : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _audioClips;

        public void OnRotate(float rotation)
        {
            if (rotation > 0f)
            {
                _audioSource.clip = _audioClips[0];
            } 
            else if (rotation < 0f)
            {
                _audioSource.clip = _audioClips[1];
            }
        }

    }
}