using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AzureSky;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[ExecuteInEditMode]
public class SunMovementController : MonoBehaviour
{

    [SerializeField] private Transform _centerWorldTransform;

    [SerializeField] private Transform _azureSun;

    [SerializeField] private Light _light;

    [SerializeField] private float _distanceToCenter;

    [SerializeField] private AzureTimeController _azureTimeController;

    [SerializeField] private float _triggerEndGameRadius = 500f;

    [SerializeField] private Image _image;

    private bool _stuckInTime = false;
    private float _stuckTimeline;

    private bool _triggeredEndGame = false;

    private void Start()
    {
        _distanceToCenter = (_centerWorldTransform.position - transform.position).magnitude;
    }

    private void Update()
    {
        // Rotates the sun around the center of the world from the orientation
        // of the sun from the Azure[Sky] Dynamic Skybox asset
        Vector3 forwardSun = _azureSun.forward * _distanceToCenter;
        transform.position = _centerWorldTransform.position - forwardSun;
        _light.transform.LookAt(_centerWorldTransform);

        if (_stuckInTime)
        {
            _azureTimeController.SetTimeline(_stuckTimeline);
        }

        if (PlayerMainController.Instance != null && (PlayerMainController.Instance.PlayerPosition - transform.position).magnitude < _triggerEndGameRadius)
        {
            TriggerEndGame();
        }
    }

    public void SetTime(float normalizedTime)
    {
        if (!_stuckInTime)
        {
            float newTimeline = _azureTimeController.GetTimeline() + normalizedTime * 2f;

            Debug.Log(newTimeline);

            if ((Mathf.Abs(newTimeline - 19f)) <= 0.1f)
            {
                StopTime();
            }
            else
            {

                if (newTimeline < 0f)
                {
                    newTimeline = 23.9f;
                }

                if (newTimeline > 24f)
                {
                    newTimeline = 0f;
                }
            }

            _azureTimeController.SetTimeline(newTimeline);
        }
    }

    public void SetTimeFast(float normalizedTime)
    {
        float newTimeline = _azureTimeController.GetTimeline() + normalizedTime * 40f;

        if (newTimeline < 0f)
        {
            newTimeline = 23.9f;
        }

        if (newTimeline > 24f)
        {
            newTimeline = 0f;
        }

        _azureTimeController.SetTimeline(newTimeline);
    }

    public void StopTime()
    {
        _stuckTimeline = _azureTimeController.GetTimeline();
        _stuckInTime = true;
    }

    private void TriggerEndGame()
    {
        if (!_triggeredEndGame)
        {
            _triggeredEndGame = true;
            StartCoroutine(FadeToWhite());
        }
    }

    IEnumerator FadeToWhite()
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Credits", LoadSceneMode.Additive);
        asyncOperation.allowSceneActivation = false;

        float k = 0f;
        do
        {
            k += Time.deltaTime * 0.1f;
            _image.color = new Color(1f, 1f, 1f, k);

            yield return new WaitForEndOfFrame();

        } while (k < 1f);

        asyncOperation.allowSceneActivation = true;
    }
}

